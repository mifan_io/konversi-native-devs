<?php
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Report extends REST_Controller {

  function __construct(){
    parent::__construct();

    $this->load->model('Menu_model');
    $this->load->model('Report_model');
    $this->load->library('session');
    $this->load->helper('url_helper');
    $this->load->helper('url');

    if (empty($_SESSION['username'])) {
      redirect(base_url());
    }
  }

  function fetch_class(){
    return $this->router->fetch_class();
  }

  function fetch_method(){
    return $this->router->fetch_method();
  }

  function dates($date){
    $date = date("Y-m-d",strtotime($date));
    return $date;
  }

  function module(){
    return ['Benefit Claim' => 'Summary', 'Member Group' => 'Summary', '20 Highest Claim' => 'Claim Member', 'Common Diseases' => 'Claim Member', '20 Highest Visit' => 'Provider', 'Claim Member' => 'Cases Claim', 'Detail Claim' => 'Detail Claim Member', 'Analysis Sheet' => 'Summary', 'Reject Letter' => 'Letter Format', 'Delayed Letter' => 'Letter Format', 'Coordination of Benefit' => 'Letter Format'];
  }

  function logo(){
    return ['assignment', 'assignment_ind', 'assignment_late', 'assignment_returned', 'card_membership', 'done', 'done_all', 'description', 'error_outline', 'restore_page', 'perm_data_setting'];
  }

  function status(){
    return ['IN' => '<span class="text-success">Active</span>', 'SU' => '<span class="text-rose">Not Active</span>', 'MA' => '<span class="text-primary">Mature</span>', 'PA' => '<span class="text-warning">Prop. Approve</span>', 'TR' => '<span class="text-danger">Terminate</span>'];
  }

  function policy($where = null, $limit = null){
    $data = $this->Report_model->procedure('mod_policy("' . $limit . '", "' . $where . '")');
    $status = $this->status();

    foreach ($data as $key => &$val) {
      $val['pol_start_date'] = get_indonesian_simple_date($val['pol_start_date']);
      $val['pol_end_date'] = get_indonesian_simple_date($val['pol_end_date']);
      $val['pol_member'] = number_format($val['pol_member'], 0, ',', '.');
      $val['pol_status'] = $status[$val['pol_status']];
    }

    return $data;
  }

  function provider_post(){
    $data = $this->Report_model->provider($_POST['param']);

    $response = (empty($data)) ? 204 : 200 ;
    json_output($response, $data);
  }

  function benefit_post(){
    $data = $this->Report_model->benefit($_POST['param']);
    $response = (empty($data)) ? 204 : 200 ;
    json_output($response, $data);
  }

  function data_post(){
    extract($_POST);

    $param2 = (empty($param2)) ? date('Y-m') . '-1' : $this->dates($param2);
    $param3 = (empty($param3)) ? date('Y-m') . '-' . cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y')) : $this->dates($param3);
    $param4 = (empty($param4)) ? '' : $param4;
    $param5 = (empty($param5)) ? '' : $param5;

    $data['claim'] = $this->Report_model->claim($param, $param2, $param3);
    $data['claims'] = $this->Report_model->claims($param, $param2, $param3);
    $data['group'] = $this->Report_model->group($param, $param2, $param3, $param4);
    $data['highest'] = $this->Report_model->highest($param, $param2, $param3, $param4);
    $data['deseases'] = $this->Report_model->deseases($param, $param2, $param3, $param4);
    $data['visit'] = $this->Report_model->visit($param, $param2, $param3, $param4, $param5);

    $response = (empty($data)) ? 204 : 200 ;
    json_output($response, $data);
  }

  function index_get(){
    $header['supmenu'] = 'Report';
    $header['submenu'] = 'Claim';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu('admin');

    $data['data'] = $this->policy("WHERE pol_status = 'IN'");
    $data['logo'] = $this->logo();
    $data['tab'] = ['Benefit Claim', 'Member Group', 'Highest Claim', 'Common Diseases', '20 Highest Visit'];

    $this->load->view('header', $header);
    $this->load->view('report/index', $data);
    $this->load->view('footer');
  }

  function production_get(){
    $header['supmenu'] = 'Report';
    $header['submenu'] = 'Production';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu('admin');

    $this->load->view('header', $header);
    $this->load->view('content');
    $this->load->view('footer');
  }

  function remunerasi_get(){
    $header['supmenu'] = 'Report';
    $header['submenu'] = 'Remunerasi';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu('admin');

    $this->load->view('header', $header);
    $this->load->view('content');
    $this->load->view('footer');
  }

  function deposit_get(){
    $header['supmenu'] = 'Report';
    $header['submenu'] = 'Deposit Fund';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu('admin');

    $this->load->view('header', $header);
    $this->load->view('content');
    $this->load->view('footer');
  }


}