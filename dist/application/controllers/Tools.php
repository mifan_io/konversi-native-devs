<?php
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Tools extends REST_Controller {

  function __construct(){
    parent::__construct();

    $this->load->model('Menu_model');
    $this->load->model('Tools_model');
    $this->load->library('session');
    $this->load->helper('url_helper');
    $this->load->helper('url');

    if (empty($_SESSION['username'])) {
      redirect(base_url());
    }
  }

  function fetch_class(){
    return $this->router->fetch_class();
  }

  function fetch_method(){
    return $this->router->fetch_method();
  }

  function index_get(){

    $header['supmenu'] = 'Tools';
    $header['submenu'] = 'Claim Reimburs Wika';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu('admin');

    $this->load->view('header', $header);
    $this->load->view('content');
    $this->load->view('footer');
  }

  function indorama_get(){
    $header['supmenu'] = 'Tools';
    $header['submenu'] = 'Claim Reimburs Indorama';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu('admin');

    $this->load->view('header', $header);
    $this->load->view('content');
    $this->load->view('footer');
  }

  function ssa_get(){
    $header['supmenu'] = 'Tools';
    $header['submenu'] = 'Claim Reimburs ASO SSA';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu('admin');

    $this->load->view('header', $header);
    $this->load->view('content');
    $this->load->view('footer');
  }

  function managecare_get(){
    $header['supmenu'] = 'Tools';
    $header['submenu'] = 'Dashboard Manage care';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu('admin');

    $data['label'] = ['OP' =>'success', 'RE' =>'danger', 'FU' =>'primary', 'CF' =>'primary', 'DS' =>'warning', 'CL' =>'success'];
    $data['status'] = ['OP' => 'OPEN', 'RE' => 'REJECT', 'FU' => 'FOLLOW UP', 'CF' => 'CONFIRMATION', 'DS' => 'DISCARD', 'CL' => 'CLOSE'];
    $data['data'] = $this->Tools_model->managecare();

    $this->load->view('header', $header);
    $this->load->view('tools/managecare', $data);
    $this->load->view('footer');
  }

  function provider_get(){
    $header['supmenu'] = 'Tools';
    $header['submenu'] = 'Dashboard Provider System';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu('admin');

    $data['head'] = ['primary', 'rose', 'success', 'info'];
    $data['data'] = $this->Tools_model->provider();

    $this->load->view('header', $header);
    $this->load->view('tools/provider', $data);
    $this->load->view('footer');
  }


}