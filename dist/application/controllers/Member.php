<?php
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Member extends REST_Controller {

  function __construct(){
    parent::__construct();

    $this->load->model('Menu_model');
    $this->load->model('Member_model');
    $this->load->library('session');
    $this->load->helper('url_helper');
    $this->load->helper('url');

    if (empty($_SESSION['username'])) {
      redirect(base_url());
    }
  }

  function fetch_class(){
    return $this->router->fetch_class();
  }

  function fetch_method(){
    return $this->router->fetch_method();
  }

  function status(){
    return ['IN' => '<span class="text-success">Active</span>', 'SU' => '<span class="text-rose">Not Active</span>', 'MA' => '<span class="text-primary">Mature</span>', 'PA' => '<span class="text-warning">Prop. Approve</span>', 'TR' => '<span class="text-danger">Terminate</span>'];
  }

  function dates($date){
    $date = date("Y-m-d",strtotime($date));
    return $date;
  }

  function policy_data($where = null, $limit = null){
    $data = $this->Member_model->procedure('mod_policy("' . $limit . '", "' . $where . '")');
    $status = $this->status();

    foreach ($data as $key => &$val) {
      $val['pol_start_date'] = get_indonesian_simple_date($val['pol_start_date']);
      $val['pol_end_date'] = get_indonesian_simple_date($val['pol_end_date']);
      $val['pol_member'] = number_format($val['pol_member'], 0, ',', '.');
      $val['pol_status'] = $status[$val['pol_status']];
    }

    return $data;
  }

  function index_get(){
    $header['supmenu'] = 'Member';
    $header['submenu'] = 'New Member';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu('admin');

    $data['data'] = $this->policy_data("WHERE pol_status = 'IN'");
    $data['type'] = $this->Member_model->code('mco_cat_id = "10" AND mco_status = "1"');
    $data['code'] = $this->Member_model->code('mco_cat_id = "9" AND mco_status = "1"');
    $data['mari'] = $this->Member_model->code('mco_cat_id = "19" AND mco_status = "1"');
    $data['gender'] = $this->Member_model->code('mco_cat_id = "1" AND mco_status = "1"');

    $this->load->view('header', $header);
    $this->load->view('member/form', $data);
    $this->load->view('footer');
  }

  function suspend_get(){
    $header['supmenu'] = 'Member';
    $header['submenu'] = 'Member Suspend';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu('admin');

    $this->load->view('header', $header);
    $this->load->view('content');
    $this->load->view('footer');
  }

  function reactive_get(){
    $header['supmenu'] = 'Member';
    $header['submenu'] = 'Member Re Active';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu('admin');

    $this->load->view('header', $header);
    $this->load->view('content');
    $this->load->view('footer');
  }

  function upload_get(){
    $header['supmenu'] = 'Member';
    $header['submenu'] = 'Member Upload';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu('admin');

    $this->load->view('header', $header);
    $this->load->view('content');
    $this->load->view('footer');
  }

  function voucher_get(){
    $header['supmenu'] = 'Member';
    $header['submenu'] = 'Voucher Membership';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu('admin');

    $this->load->view('header', $header);
    $this->load->view('content');
    $this->load->view('footer');
  }

  function report_get(){
    $header['supmenu'] = 'Member';
    $header['submenu'] = 'Member Preview';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu('admin');

    $this->load->view('header', $header);
    $this->load->view('content');
    $this->load->view('footer');
  }

}