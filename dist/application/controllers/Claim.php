<?php
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Claim extends REST_Controller {

  function __construct(){
    parent::__construct();

    $this->load->model('Menu_model');
    $this->load->model('Claim_model');
    $this->load->library('session');
    $this->load->helper('url_helper');
    $this->load->helper('url');

    if (empty($_SESSION['username'])) {
      redirect(base_url());
    }
  }

  function fetch_class(){
    return $this->router->fetch_class();
  }

  function fetch_method(){
    return $this->router->fetch_method();
  }

  function index_get(){

    $header['supmenu'] = 'Claim';
    $header['submenu'] = 'Claim';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu('admin');

    $this->load->view('header', $header);
    $this->load->view('content');
    $this->load->view('footer');
  }

  function register_get(){
    $header['supmenu'] = 'Claim';
    $header['submenu'] = 'Register | Batch';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu('admin');

    $this->load->view('header', $header);
    $this->load->view('content');
    $this->load->view('footer');
  }

  function supervisor_get(){
    $header['supmenu'] = 'Claim';
    $header['submenu'] = 'Approval Claim';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu('admin');

    $this->load->view('header', $header);
    $this->load->view('content');
    $this->load->view('footer');
  }

  function voucher_get(){
    $header['supmenu'] = 'Claim';
    $header['submenu'] = 'Voucher Claim';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu('admin');

    $this->load->view('header', $header);
    $this->load->view('content');
    $this->load->view('footer');
  }

  function preview_get(){
    $header['supmenu'] = 'Claim';
    $header['submenu'] = 'Preview Claim';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu('admin');

    $this->load->view('header', $header);
    $this->load->view('content');
    $this->load->view('footer');
  }

  function productivity_get(){
    $header['supmenu'] = 'Claim';
    $header['submenu'] = 'Productivity Analyst';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu('admin');

    $this->load->view('header', $header);
    $this->load->view('content');
    $this->load->view('footer');
  }


}