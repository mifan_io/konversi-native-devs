<?php
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Policy extends REST_Controller {

  function __construct(){
    parent::__construct();

    $this->load->model('Menu_model');
    $this->load->model('Bank_model');
    $this->load->model('Policy_model');
    $this->load->library('session');
    $this->load->helper('url_helper');
    $this->load->helper('url');

    if (empty($_SESSION['username'])) {
      redirect(base_url());
    }
  }

  function fetch_class(){
    return $this->router->fetch_class();
  }

  function fetch_method(){
    return $this->router->fetch_method();
  }

  function status(){
    return ['IN' => '<span class="text-success">Active</span>', 'SU' => '<span class="text-rose">Not Active</span>', 'MA' => '<span class="text-primary">Mature</span>', 'PA' => '<span class="text-warning">Prop. Approve</span>', 'TR' => '<span class="text-danger">Terminate</span>'];
  }

  function dates($date){
    $date = date("Y-m-d",strtotime($date));
    return $date;
  }

  function policy_code(){
    $opt = $this->Policy_model->read('mst_code', 'WHERE mco_status = 1 AND mco_cat_id IN (2, 5, 6, 7)', 'mco_mastercode, mco_description, mco_cat_id');

    $i = 0;
    $data['dua'] = '';//2
    $data['lima'] = '';//5
    $data['enam'] = '';//6
    $data['tujuh'] = '';//7

    foreach ($opt as $key => $val) {

      if ($val['mco_cat_id'] == 2){
        $data['dua'] .= '<option value="' . $val['mco_mastercode'] . '">' . $val['mco_description'] . '</option>';
      }

      if ($val['mco_cat_id'] == 5){
        $data['lima'] .= '<option value="' . $val['mco_mastercode'] . '">' . $val['mco_description'] . '</option>';
      }

      if ($val['mco_cat_id'] == 6){
        $data['enam'] .= '<option value="' . $val['mco_mastercode'] . '">' . $val['mco_description'] . '</option>';
      }

      if ($val['mco_cat_id'] == 7){
        $data['tujuh'] .= '<div class="form-check form-check-inline">
                            <label class="form-check-label">
                              <input class="form-check-input" name="polbt_benefit_type[' . $i . '][polbt_benefit_type]" type="checkbox" value="' . $val['mco_mastercode'] . '"> ' . $val['mco_description'] . '
                              <span class="form-check-sign">
                                <span class="check"></span>
                              </span>
                            </label>
                          </div>';
        $i++;
      }
    }

    return $data;
  }

  function policy_data($where = null, $limit = null){
    $data = $this->Policy_model->procedure('mod_policy("' . $limit . '", "' . $where . '")');
    $status = $this->status();

    foreach ($data as $key => &$val) {
      $val['pol_start_date'] = get_indonesian_simple_date($val['pol_start_date']);
      $val['pol_end_date'] = get_indonesian_simple_date($val['pol_end_date']);
      $val['pol_member'] = number_format($val['pol_member'], 0, ',', '.');
      $val['pol_status'] = $status[$val['pol_status']];
    }

    return $data;
  }

  function index_get(){

    $header['supmenu'] = 'Policy';
    $header['submenu'] = 'Policy';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu($_SESSION['username']);

    $data['data'] = $this->policy_data();

    $this->load->view('header', $header);
    $this->load->view('policy/index', $data);
    $this->load->view('footer');
  }

  function form_post($id = null){
    $_POST['pol_pol_no'] = $this->Policy_model->no($_POST['pol_start_date'], $_POST['pol_insurance_code']);
    $_POST['pol_dedicated_account_normal'] = $_POST['pol_dedicated_account'];
    $_POST['pol_start_date'] = $this->dates($_POST['pol_start_date']);
    $_POST['pol_end_date'] = $this->dates($_POST['pol_end_date']);
    $_POST['pol_dedicated_account_status_realisasi'] = 'SAFE';
    $_POST['pol_dedicated_account_status_proyeksi'] = 'SAFE';
    $_POST['pol_blank_card_behind'] = 'ssa-card-back.png';
    $_POST['pol_blank_card_file'] = 'ssa-blank-card.png';
    $_POST['pol_channel_dist'] = 'CHANNEL DISTRIBUTION';
    $_POST['pol_cp_dept'] = 'NOT CP Departemen';
    $_POST['pol_user'] = $_SESSION['username'];
    $_POST['pol_manual_claim_status'] = 'N';
    $_POST['pol_policy_loading'] = 1;
    $_POST['pol_stamp'] = cdatetime;
    $_POST['pol_status'] = 'IN';
    $_POST['pol_owlexa'] = 'N';

    $benefit = $_POST['polbt_benefit_type'];
    unset($_POST['polbt_benefit_type']);

    foreach ($benefit as $key => $val) {
      $benefit[$key]['polbt_pol_no'] = $_POST['pol_pol_no'];
      $benefit[$key]['polbt_pol_no'] = $_POST['pol_pol_no'];
      $benefit[$key]['polbt_user'] = $_SESSION['username'];
      $benefit[$key]['polbt_stamp'] = cdatetime;
    }

    $this->Policy_model->create_batch('policy_benefit_type', $benefit);
    $this->Policy_model->create('policy', $_POST);

    $_SESSION['users_input'] = true;
    $_SESSION['users_text'] = 'Policy Baru Berhasil Disimpan';
    $_SESSION['users_status'] = 'success';

    redirect(base_url() . 'policy');
  }

  function form_get($id = null){

    $header['supmenu'] = 'Policy';
    $header['submenu'] = 'Policy';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu('admin');

    $opt = $this->policy_code();

    $data['pol_payment_mode'] = $opt['dua'];//2
    $data['pol_product_code'] = $opt['enam'];//6
    $data['pol_services_type'] = $opt['lima'];//5
    $data['polbt_benefit_type'] = $opt['tujuh'];//7

    $data['holding'] = [1 => 'Head Office', 2 => 'Branch Office'];
    $data['payment'] = ['C' => 'Company', 'M' => 'Member'];
    $data['family'] = ['N' => 'NO', 'Y' => 'YES'];
    $data['agent'] = ['1' => 'Default'];
    $data['bank'] = ['BCA', 'Mandiri'];
    $data['currency'] = ['RP', 'USD'];
    $data['id'] = $id;

    $footer['js'] = 'demo.initMaterialWizard();
                     setTimeout(function () {
                       $(".card .card-wizard").addClass("active");
                     }, 600);';

    $this->load->view('header', $header);
    $this->load->view('policy/form', $data);
    $this->load->view('footer', $footer);
  }

  function plan_post(){
    $i = 0;
    $planning = null;
    extract($_POST);
    foreach ($plan as $key => $val) {
      if ($val != '0') {
        $planning[$i]['polbt_pol_no'] = $polbt_pol_no;
        $planning[$i]['polbt_user'] = $_SESSION['username'];
        $planning[$i]['polbt_benefit_type'] = $val;
        $planning[$i]['polbt_benefit_plan'] = $key;
        $planning[$i]['polbt_stamp'] = cdatetime;
        $i++;
      }
    }

    $this->Policy_model->create_batch($polbt_pol_no . '_policy_benefit_type', $planning);

    $_SESSION['users_input'] = true;
    $_SESSION['users_text'] = 'Policy Plan Baru Berhasil Disimpan';
    $_SESSION['users_status'] = 'success';

    redirect(base_url() . 'policy/plan');
  }

  function plan_list_post(){
    $plan = $this->Policy_model->plan($_POST['no'], $_POST['product']);
    $response = (empty($plan)) ? 204 : 200 ;
    json_output($response, $plan);
  }

  function plan_get(){
    $header['supmenu'] = 'Policy';
    $header['submenu'] = 'Policy Plan';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu('admin');

    $data['data'] = $this->policy_data("WHERE pol_status = 'IN'");

    $this->load->view('header', $header);
    $this->load->view('policy/plan', $data);
    $this->load->view('footer');
  }

  function code_post(){
    $benefit = $this->Policy_model->code($_POST['param']);
    $response = (empty($benefit)) ? 204 : 200 ;
    json_output($response, $benefit);
  }

  function benefit_type_post(){
    $benefit = $this->Policy_model->benefit_plan($_POST['param'],$_POST['param2']);
    $response = (empty($benefit)) ? 204 : 200 ;
    json_output($response, $benefit);
  }

  function benefit_list_post(){
    $benefit = $this->Policy_model->benefit($_POST['param']);
    $response = (empty($benefit)) ? 204 : 200 ;
    json_output($response, $benefit);
  }

  function benefit_data_post(){
    $data = $this->Policy_model->procedure('mod_produk_benefit_plan_select("' . $_POST['param'] . '", "' . $_POST['param2'] . '", "' . $_POST['param3'] . '")');
    $response = (empty($data)) ? 204 : 200 ;
    json_output($response, $data);
  }

  function benefit_post(){
    extract($_POST);
    foreach ($cek as $key => $val) {
      if ($val == 0) {
        unset($benefit[$key]);
      }
    }

    foreach ($benefit as $key => $val) {
      $benefit[$key]['prdplan_provider_category'] = $prdplan_provider_category;
      $benefit[$key]['prdplan_product_code'] = $prdplan_product_code;
      $benefit[$key]['prdplan_plan_code'] = $prdplan_plan_code;
      $benefit[$key]['prdplan_ben_type'] = $prdplan_ben_type;
      $benefit[$key]['prdplan_ben_code'] = $prdplan_ben_code;
      $benefit[$key]['prdplan_user'] = $_SESSION['username'];
      $benefit[$key]['prdplan_stamp'] = cdatetime;
      $benefit[$key]['prdplan_status'] = 1;
    }

    $this->Policy_model->create_batch($no . '_product_benefit_plan', $benefit);

    $_SESSION['users_input'] = true;
    $_SESSION['users_text'] = 'Policy Benefit Baru Berhasil Disimpan';
    $_SESSION['users_status'] = 'success';

    redirect(base_url() . 'policy/plan');
  }

  function benefit_get(){
    $header['supmenu'] = 'Policy';
    $header['submenu'] = 'Policy Benefit';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu('admin');

    $data['data'] = $this->policy_data("WHERE pol_status = 'IN'");
    $data['provider'] = ['P' => 'Provider' , 'N' => 'Non-Provider'];

    $this->load->view('header', $header);
    $this->load->view('policy/benefit', $data);
    $this->load->view('footer');
  }

  function class_data_post(){
    $data = $this->Policy_model->klass($_POST['param']);
    // print_r($data);die();
    $category = ['P' => 'Provider', 'N' => 'Non Provider'];
    if (!empty($data)) {
      foreach ($data as $key => &$val) {
        $val['prdclass_provider_category'] = $category[$val['prdclass_provider_category']];
      }
    }
    $response = (empty($data)) ? 204 : 200 ;
    json_output($response, $data);
  }

  function class_get(){
    $header['supmenu'] = 'Policy';
    $header['submenu'] = 'Policy Class Benefit';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu('admin');

    $data['data'] = $this->policy_data("WHERE pol_status = 'IN'");
    $data['provider'] = ['P' => 'Provider' , 'N' => 'Non-Provider'];

    $this->load->view('header', $header);
    $this->load->view('policy/class', $data);
    $this->load->view('footer');
  }

  function class_post(){
    extract($_POST);
    $where = null;
    $table = $no . '_product_class';
    unset($_POST['no']);

    foreach ($_POST as $key => $val) {
      if ($key != 'prdclass_plan_name') {
        $where[] = $key . ' = "' . $val . '"';
      }
    }

    $data = $this->Policy_model->one($table, 'WHERE ' . implode(' AND ', $where), 'prdclass_benefit_plan');

    $_POST['prdclass_stamp'] = cdatetime;
    $_POST['prdclass_user'] = $_SESSION['username'];

    $txt = (empty($data)) ? 'Berhasil Disimpan' : 'Sudah Ada' ;
    $sts = (empty($data)) ? 'success' : 'warning' ;

    if (empty($data)){
      $this->Policy_model->create($table, $_POST);
    }

    $_SESSION['users_input'] = true;
    $_SESSION['users_text'] = 'Data Policy Class ' . $txt;
    $_SESSION['users_status'] = $sts;

    redirect(base_url() . 'policy/class');
  }

  function provider_get(){
    $header['supmenu'] = 'Policy';
    $header['submenu'] = 'Policy Provider';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu('admin');

    $this->load->view('header', $header);
    $this->load->view('content');
    $this->load->view('footer');
  }

  function family_data_post(){
    $family = $this->Policy_model->family($_POST['param']);

    $data['plan'] = ($family['vals'] == 'Y') ? $this->Policy_model->family_plan($_POST['param']) : null;
    $data['benefit'] = ($family['vals'] == 'Y') ? $this->Policy_model->family_benefit($_POST['param']) : null;

    $response = (empty($data['plan']) && empty($data['benefit'])) ? 204 : 200 ;
    json_output($response, $data);
  }

  function family_post(){
    extract($_POST);
    $i = 0;
    $pla = null;
    $ben = null;
    $amount = [];
    $planning = [];

    foreach ($plan as $key => $val) {
      if ($val['benfac_benefit_amount'] > 0) {
      $pla[] =  '"' . $val['benfac_benefit_class'] . '"';

        $amount[$key]['prdplan_benefit_amount'] = $val['benfac_benefit_amount'];
        $amount[$key]['prdplan_user'] = $_SESSION['username'] ;
        $amount[$key]['prdplan_stamp'] = cdatetime ;
        $amount[$key]['prdplan_case_disability'] = 1 ;
        $amount[$key]['prdplan_frequently'] = 1 ;
        $amount[$key]['prdplan_category'] = 'T';
      } else {
        unset($plan[$key]);
      }
    }

    foreach ($benefit as $key => &$val) {
      if ($val['benfam_benefit_type'] != '0') {
        $val['benfam_status'] = 1;
        $val['benfam_pol_no'] = $no;
        $val['benfam_stamp'] = cdatetime;
        $val['benfam_user'] = $_SESSION['username'];


        foreach ($plan as $k => $vals) {
          if ($vals['benfac_benefit_amount'] > 0) {
            $planning[$i]['benfac_status'] = 1;
            $planning[$i]['benfac_pol_no'] = $no;
            $planning[$i]['benfac_stamp'] = cdatetime;
            $planning[$i]['benfac_user'] = $_SESSION['username'];
            $planning[$i]['benfac_benefit_type'] = $val['benfam_benefit_type'];
            $planning[$i]['benfac_benefit_class'] = $vals['benfac_benefit_class'];
            $planning[$i]['benfac_benefit_amount'] = $vals['benfac_benefit_amount'];
            $i++;
          }
        }
      } else {
        unset($benefit[$key]);
      }
      $ben[] = '"' . $val['benfam_benefit_type'] . '"';
    }

    $product = $this->Policy_model->read($no . '_product_class', 'WHERE prdclass_class_code IN (' . implode(', ', $pla) . ') AND prdclass_benefit_type IN (' . implode(', ', $ben) . ')', 'DISTINCT prdclass_benefit_plan');

    $this->Policy_model->delete('policy_benefit_family', 'benfam_pol_no = "' . $no . '"');
    $this->Policy_model->delete('policy_benefit_family_class', 'benfac_pol_no = "' . $no . '"');

    $this->Policy_model->create_batch('policy_benefit_family', $benefit);
    $this->Policy_model->create_batch('policy_benefit_family_class', $planning);

    foreach ($product as $key => $val) {
      foreach ($amount as $k => $vals) {
        $this->Policy_model->update($no . '_product_benefit_plan', 'prdplan_plan_code = "' . $val['prdclass_benefit_plan'] . '" AND prdplan_ben_code LIKE "%9999%"', $vals);
      }
    }

    $_SESSION['users_input'] = true;
    $_SESSION['users_text'] = 'Data Policy Family Berhasil Disimpan';
    $_SESSION['users_status'] = 'success';

    redirect(base_url() . 'policy/family');

  }

  function family_get(){
    $header['supmenu'] = 'Policy';
    $header['submenu'] = 'Family Benefit';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu('admin');

    $data['data'] = $this->policy_data("WHERE pol_status = 'IN'");

    $this->load->view('header', $header);
    $this->load->view('policy/family', $data);
    $this->load->view('footer');
  }

  function topup_data_post(){
    $data['topup'] = $this->Policy_model->topup($_POST['param']);
    $data['history'] = $this->Policy_model->history($_POST['param']);

    if (!empty($data['history'])) {
      foreach ($data['history'] as $key => &$val) {
        $val['pda_date_transfer'] = get_indonesian_simple_date($val['pda_date_transfer']);
        $val['pda_amount'] = number_format($val['pda_amount'], 0);
        $val['pda_amount_proyeksi'] = number_format($val['pda_amount_proyeksi'], 0);
        $val['pda_persen_proyeksi'] = number_format($val['pda_persen_proyeksi'], 2);
      }
    }

    $response = (empty($data['topup']) && empty($data['history'])) ? 204 : 200 ;
    json_output($response, $data);
  }

  function topup_post(){
    extract($_POST);
    $pda['pda_date_transfer'] = $this->dates($pda['pda_date_transfer']);
    $pda['pda_user'] = $_SESSION['username'];
    $pda['pda_stamp'] = cdatetime;
    $pda['pda_amount'] = $amount;
    $pda['pda_policy_no'] = $no;

    $pro_amo = $pol['pol_dedicated_account_proyeksi'] + $amount;
    $sel_amo = ($pro_amo > $pol['pol_dedicated_account_normal']) ? $pro_amo - $pol['pol_dedicated_account_normal'] : 0 ;

    $pol['pol_dedicated_account_balance'] = $pol['pol_dedicated_account_balance'] + $amount;
    $pol['pol_dedicated_account'] = $pol['pol_dedicated_account'] + $sel_amo;
    $pol['pol_dedicated_account_proyeksi'] = $pro_amo;
    $pol['pol_user'] = $_SESSION['username'];
    $pol['pol_stamp'] = cdatetime;

    unset($pol['pol_dedicated_account_normal']);

    $this->Policy_model->create('policy_deposite_fund_history', $pda);
    $this->Policy_model->update('policy', 'pol_pol_no = "' . $no . '"', $pol);

    $_SESSION['users_input'] = true;
    $_SESSION['users_text'] = 'Data Topup Berhasil Disimpan';
    $_SESSION['users_status'] = 'success';

    redirect(base_url() . 'policy/topup');
  }

  function topup_get(){
    $header['supmenu'] = 'Policy';
    $header['submenu'] = 'Topup Dedicated Fund';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu('admin');

    $data['bank'] = $this->Bank_model->bank();
    $data['data'] = $this->policy_data("WHERE pol_status = 'IN'");

    $this->load->view('header', $header);
    $this->load->view('policy/topup', $data);
    $this->load->view('footer');
  }

  function suspend_data_post(){
    $suspend = $this->Policy_model->status($_POST['param']);
    $response = (empty($suspend)) ? 204 : 200 ;
    json_output($response, $suspend);
  }

  function suspend_post(){
    $pol['pol_user'] = $_SESSION['username'];
    $pol['pol_suspend_date'] = cdatetime;
    $pol['pol_stamp'] = cdatetime;
    $pol['pol_status'] = 'SU';

    $pol['pol_pol_no'] = $_POST['no'];
    $this->Policy_model->update('policy', 'pol_pol_no = "' . $_POST['no'] . '"', $pol);

    $_SESSION['users_input'] = true;
    $_SESSION['users_text'] = 'Data Policy Berhasil Suspend';
    $_SESSION['users_status'] = 'success';

    redirect(base_url() . 'policy/suspend');
  }

  function suspend_get(){
    $header['supmenu'] = 'Policy';
    $header['submenu'] = 'Suspend Policy';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu('admin');

    $data['data'] = $this->policy_data("WHERE pol_status = 'IN'");

    $this->load->view('header', $header);
    $this->load->view('policy/suspend', $data);
    $this->load->view('footer');
  }

  function report_exclusion_post(){
    $report = $this->Policy_model->exclusion($_POST['param']);
    $response = (empty($report)) ? 204 : 200 ;
    json_output($response, $report);
  }

  function report_klass_post(){
    $report = $this->Policy_model->klas($_POST['param']);
    $response = (empty($report)) ? 204 : 200 ;
    json_output($response, $report);
  }

  function reporting_post(){
    $report = $this->Policy_model->report($_POST['param'], $_POST['param2'], $_POST['param3']);
    $response = (empty($report)) ? 204 : 200 ;
    json_output($response, $report);
  }

  function report_get(){
    $header['supmenu'] = 'Policy';
    $header['submenu'] = 'Policy Preview';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu('admin');

    $data['data'] = $this->policy_data("WHERE pol_status = 'IN'");

    $this->load->view('header', $header);
    $this->load->view('policy/report', $data);
    $this->load->view('footer');
  }

}