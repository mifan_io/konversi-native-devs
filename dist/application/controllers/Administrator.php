<?php
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Administrator extends REST_Controller {

  function __construct(){
    parent::__construct();

    $this->load->model('Menu_model');
    $this->load->model('Administrator_model');
    $this->load->library('session');
    $this->load->helper('url_helper');
    $this->load->helper('url');

    if (empty($_SESSION['username'])) {
      redirect(base_url());
    }
  }

  /*
cdate
cdatetime
ctime
systemcode

mlog_username
mlog_password
mlog_name
mlog_jabatan
mlog_function
kepesertaan claimanalysis provider finance administrator
mlog_email
mlog_login
mlog_system
A : Healthcare B : Customer Service C : Provider D : SHIS E : Dashboard F : Personal
mlog_photo
mlog_status
mlog_stamp_date
mlog_stamp_user
  */

  function fetch_class(){
    return $this->router->fetch_class();
  }

  function fetch_method(){
    return $this->router->fetch_method();
  }

  function system(){
    return ['A' => 'Health Care', 'B' => 'Call Center', 'C' => 'Provider', 'D' => 'SHIS (SSA Healthcare Information Sistem)', 'E' => 'Dashboard'];
  }
  function divisi(){
    return ['kepesertaan' => 'Membership', 'claimanalysis' => 'Claim Analysis', 'provider' => 'Provider Relation', 'finance' => 'Finance', 'administrator' => 'Administrator'];
  }

  function hashing($username, $password){
    $hash = 'adzhwa'.$username.'9113'.$password.'086666!@#$%^&*()ClUmSyArt';
    return md5(md5($hash));
  }

  function username_post(){
    $users = $this->Administrator_model->username($_POST['username'], $_POST['system']);
    $response = (empty($users)) ? 204 : 200 ;
    json_output($response, $users);
  }

  function admin_post(){
    $users = $this->Administrator_model->admin_system($_POST['param']);
    $response = (empty($users)) ? 204 : 200 ;
    json_output($response, $users);
  }

// ==================granting======================================
  function system_post(){
    $users = $this->Administrator_model->menu($_POST['param'], $_POST['param2']);
    $response = (empty($users)) ? 204 : 200 ;
    json_output($response, $users);
  }

  function granting_post(){
    extract($_POST);

    $i = 0;
    $create = [];

    $where = 'mua_username = "' . $username . '" AND mua_system = "' . $system . '"';
    $this->Administrator_model->delete('mst_system_access', $where);

    foreach ($menu as $key => $val) {
      if ($val != 0) {
        $create[$i]['mua_username'] = $username;
        $create[$i]['mua_stamp'] = cdatetime;
        $create[$i]['mua_system'] = $system;
        $create[$i]['mua_idmenu'] = $val;
        $i++;
      }
    }
    $this->Administrator_model->create_batch('mst_system_access', $create);

    $_SESSION['users_input'] = true ;
    $_SESSION['users_text'] = 'Granting '. $username .' Berhasil di Simpan';

    redirect(base_url() . 'administrator/granting');
  }

  function granting_get(){
    $header['supmenu'] = 'Administrator';
    $header['submenu'] = 'Granting User';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu($_SESSION['username']);

    $data['system'] = $this->system();
    $data['divisi'] = $this->divisi();

    $this->load->view('header', $header);
    $this->load->view('administrator/granting', $data);
    $this->load->view('footer');
  }
// ==================end granting======================================

// ==================change======================================
  function change_post(){
    extract($_POST);
    $user = $this->Administrator_model->username($_SESSION['username'], systemcode, $old_password);
    if (empty($user)) {

      $_SESSION['users_input'] = true;
      $_SESSION['users_text'] = 'Password Lama Tidak Sesuai';
      $_SESSION['users_status'] = 'danger';

      redirect(base_url() . 'administrator/change');
    } else {
      $data['mlog_password'] = $this->hashing($_SESSION['username'], $password);

      $where = 'mlog_username = "' . $_SESSION['username'] . '" AND mlog_system = "' . systemcode . '"';

      $this->Administrator_model->update('mst_login', $where, $data);

      $_SESSION['users_input'] = true;
      $_SESSION['users_text'] = 'Password Berhasil Dirubah';
      $_SESSION['users_status'] = 'success';

      redirect(base_url() . 'administrator/change');
    }
  }

  function change_get(){
    $header['supmenu'] = 'Administrator';
    $header['submenu'] = 'Change Password';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu($_SESSION['username']);

    $this->load->view('header', $header);
    $this->load->view('administrator/change');
    $this->load->view('footer');
  }
// ==================end change======================================

// ==================reset======================================
  function reset_post(){
    extract($_POST);

    $data['mlog_password'] = $this->hashing($username, $password);

    $where = 'mlog_username = "' . $username . '" AND mlog_system = "' . systemcode . '"';

    $this->Administrator_model->update('mst_login', $where, $data);

    $_SESSION['users_input'] = true;
    $_SESSION['users_text'] = 'Password ' . ucfirst($username) . ' Berhasil Dirubah';
    $_SESSION['users_status'] = 'success';

    redirect(base_url() . 'administrator/reset');
  }

  function reset_get(){
    $header['supmenu'] = 'Administrator';
    $header['submenu'] = 'Reset Password';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu($_SESSION['username']);

    $data['system'] = $this->system();

    $this->load->view('header', $header);
    $this->load->view('administrator/reset', $data);
    $this->load->view('footer');
  }
// ==================end reset======================================

// ==================users======================================
  function users_get($id = null){
    $header['supmenu'] = 'Administrator';
    $header['submenu'] = 'Create User';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu($_SESSION['username']);

    $data['system'] = $this->system();
    $data['divisi'] = $this->divisi();
    $data['users'] = $this->Administrator_model->users();

    $this->load->view('header', $header);
    $this->load->view('administrator/users', $data);
    $this->load->view('footer');
  }

  function iddle_post(){
    $data['mlog_status'] = '0';

    $ret = $this->Administrator_model->update('mst_login', 'mlog_username = "' . $_POST['username'] . '"', $data);

    $_SESSION['users_input'] = (empty($ret)) ? false : true ;
    $_SESSION['users_text'] = 'User '. $_POST['username'] .' Berhasil di Non-Aktifkan';

    redirect(base_url().'administrator/users');
  }

  function users_post(){
    $hash = 'adzhwa'.$_POST['username'].'9113'.$_POST['password'].'086666!@#$%^&*()ClUmSyArt';
    $mlog_password = md5(md5($hash));

    $data['mlog_login'] = "0";
    $data['mlog_status'] = "1";
    $data['mlog_stamp_date'] = cdatetime;
    $data['mlog_name'] = $_POST['name'];
    $data['mlog_email'] = $_POST['email'];
    $data['mlog_system'] = $_POST['system'];
    $data['mlog_password'] = $mlog_password;
    $data['mlog_jabatan'] = $_POST['jabatan'];
    $data['mlog_username'] = $_POST['username'];
    $data['mlog_function'] = $_POST['function'];
    $data['mlog_stamp_user'] = $_SESSION['username'];

    $ret = $this->Administrator_model->create('mst_login', $data);

    $_SESSION['users_input'] = true ;
    $_SESSION['users_text'] = 'User '. $_POST['username'] .' Aplikasi Berhasil di Simpan';

    redirect(base_url().'administrator/users');
  }
// ==================end users======================================

  function index_get(){
    $header['supmenu'] = 'Administrator';
    $header['submenu'] = 'Create New User';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu($_SESSION['username']);

    $this->load->view('header', $header);
    $this->load->view('content');
    $this->load->view('footer');
  }
}