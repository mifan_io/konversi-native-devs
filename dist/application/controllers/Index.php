<?php
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Index extends REST_Controller {

  function __construct(){
    parent::__construct();

    $this->load->model('Log_model');
    $this->load->model('Menu_model');
    $this->load->library('session');
    $this->load->helper('url_helper');
    $this->load->helper('url');
  }

  function fetch_class(){
    return $this->router->fetch_class();
  }

  function fetch_method(){
    return $this->router->fetch_method();
  }

  function index_get(){

    if (empty($_SESSION['username'])) {
      $this->load->view('login');
    } else {
      $header['supmenu'] = '';
      $header['submenu'] = '';
      $header['class'] = $this->fetch_class();
      $header['method'] = $this->fetch_method();
      $header['menu'] = $this->Menu_model->menu($_SESSION['username']);

      $this->load->view('header', $header);
      $this->load->view('content');
      $this->load->view('footer');
    }
  }

  function logout_post(){
    session_destroy();
    session_unset(); 
    redirect('');
  }

  function login_post(){
    $this->Log_model->login($_POST['username'], $_POST['password']);
    redirect('');
  }

  function menu_get(){
    echo "<pre>";
    $menu = $this->Menu_model->menu($_SESSION['username']);
    print_r($menu);
  }

}