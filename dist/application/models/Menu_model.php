<?php
// https://www.teletopiasms.no/np/frontpage/gateway/api-http-examples-php
// https://stackoverflow.com/questions/5647461/how-do-i-send-a-post-request-with-php
class Menu_model extends CI_Model {

  public function __construct() {
    parent::__construct();
    $this->load->database();
    $this->load->model('Dml_model');
  }

  function old_link(){
    return ['#', 'homee.php', 'mod-admin-change-password.php', 'mod-admin-create-user.php', 'mod-admin-granting.php', 'mod-admin-reset-password.php', 'mod-callcenter-report-guaranteeletter.php', 'mod-callcenter-report.php', 'mod-callcenter.php', 'mod-casemonitoring.php', 'mod-confirmation-print.php', 'mod-confirmation.php', 'mod-dedicated-fund.php', 'mod-eoc-inquery.php', 'mod-eoc-invoice.php', 'mod-finance-invoice-member.php', 'mod-finance-paid-manual.php', 'mod-finance-panel.php', 'mod-finance-receivepay-eoc.php', 'mod-finance-receivepay-membership.php', 'mod-guarantee-letter.php', 'mod-hospital-monitoring-input-panel.php', 'mod-hospital-monitoring-report-panel.php', 'mod-mattel-reporting.php', 'mod-mattel.php', 'mod-member-new-member.php', 'mod-member-reactive.php', 'mod-member-report.php', 'mod-member-suspend.php', 'mod-member-upload.php', 'mod-member-voucher.php', 'mod-membership.php', 'mod-policy-benefit-family.php', 'mod-policy-benefit.php', 'mod-policy-card.php', 'mod-policy-class.php', 'mod-policy-plan.php', 'mod-policy-provider.php', 'mod-policy-report.php', 'mod-policy-suspend.php', 'mod-policy-topup-da.php', 'mod-policy.php', 'mod-provider-dashboard.php', 'mod-provider-group.php', 'mod-provider-policy.php', 'mod-provider.php', 'mod-registration-print.php', 'mod-registration.php', 'mod-report-claim-analysis.php', 'mod-report-deposit-fund.php', 'mod-report-membership.php', 'mod-report-production.php', 'mod-report-remunerasi.php', 'mod-tools-claim-indorama.php', 'mod-tools-claim-ssa.php', 'mod-tools-claim.php', 'mod-tools-dashboard-provider.php', 'mod-tools-managecare.php', 'mod-transaction-claim.php', 'mod-transaction-preview-claim.php', 'mod-transaction-prod-analysis.php', 'mod-transaction-register.php', 'mod-transaction-supervisor-panel.php', 'mod-transaction-voucher-panel.php', 'mod-void.php'];
  }

  function link(){
    return ['#', 'logout', 'administrator/change', 'administrator/users', 'administrator/granting', 'administrator/reset', 'callcenter/guaranteeletter', 'callcenter/report', 'callcenter', 'casemonitoring', 'confirmation/print', 'confirmation', 'dedicated/fund', 'eoc/inquery', 'eoc/invoice', 'finance/invoice', 'finance/manual', 'finance/panel', 'finance/eoc', 'finance/membership', 'guarantee/letter', 'hospital/input', 'hospital/report', 'mattel/reporting', 'mattel', 'member/new', 'member/reactive', 'member/report', 'member/suspend', 'member/upload', 'member/voucher', 'membership', 'policy/family', 'policy/benefit', 'policy/card', 'policy/class', 'policy/plan', 'policy/provider', 'policy/report', 'policy/suspend', 'policy/topup', 'policy', 'provider/dashboard', 'provider/group', 'provider/policy', 'provider', 'registration/print', 'registration', 'report/analysis', 'report/deposit', 'report/membership', 'report/production', 'report/remunerasi', 'tools/indorama', 'tools/ssa', 'tools/claim', 'tools/provider', 'tools/managecare', 'transaction/claim', 'transaction/preview', 'transaction/analysis', 'transaction/register', 'transaction/supervisor', 'transaction/voucher', 'void'];
  }

  function menu($username){
    $_id = null;

    $menu = $this->Dml_model->read('mst_system_access', 'JOIN mst_submenu ON mst_system_access.mua_idmenu = mst_submenu.mss_id
    JOIN mst_menu ON mst_submenu.mss_menu_category = mst_menu.msm_id AND mst_system_access.mua_system = mst_menu.msm_system
    WHERE mst_system_access.mua_username = "' . $username . '" AND mst_system_access.mua_system = "A" AND mst_menu.msm_status = "1"
    ORDER BY mst_menu.msm_id ASC', 'DISTINCT mst_menu.msm_id, mst_menu.msm_menu_category, mst_menu.msm_class_fa');

    foreach ($menu as $key => $val) {
      $_id[] = $val['msm_id'];
    }

    $id_ = implode(',', $_id);

    $submenu = $this->Dml_model->read('mst_system_access', 'JOIN mst_submenu ON mst_system_access.mua_idmenu = mst_submenu.mss_id
    JOIN mst_menu ON mst_system_access.mua_system = mst_menu.msm_system AND mst_submenu.mss_menu_category = mst_menu.msm_id
    WHERE mst_system_access.mua_username = "' . $username . '" AND mst_system_access.mua_system = "A" AND
    mst_submenu.mss_menu_category IN (' . $id_ . ') AND mst_submenu.mss_status = "1"
    ORDER BY mst_submenu.mss_id', 'DISTINCT mst_submenu.mss_id, mst_menu.msm_menu_category, mst_submenu.mss_submenu_desc,
    mst_submenu.mss_title, mst_submenu.mss_url, mst_submenu.mss_menu_category super_id');

    foreach ($submenu as &$val) {
      $val['mss_url'] = str_replace($this->old_link(), $this->link(), $val['mss_url']);
    }

    foreach ($menu as &$arr1) {
      $offer_id = $arr1['msm_id'];

      $match = array_filter($submenu, function($v) use ($offer_id){
        return $v['super_id'] == $offer_id;
      });

      // $match = array_pop($match);
      $arr1['submenu'] = array_values($match);
    }

    return $menu;
    // return $this->Dml_model->read('mst_submenu', 'ORDER BY mss_url ASC');
  }
}