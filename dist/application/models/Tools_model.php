<?php

class Tools_model extends CI_Model {

  public function __construct() {
    parent::__construct();
    $this->load->database();
    $this->load->model('Dml_model');
  }

  function provider(){
    return $this->read('mst_provider', 'WHERE mprov_login_system = "1"', 'mprov_prov_code, mprov_name, mprov_last_login_date');
  }

  function managecare(){
    return $this->procedure('report_callcenter_dashboard_manage_care()');
  }

  function procedure($procedure){
    return $this->Dml_model->procedure($procedure);
  }

  function one($table, $condition = null, $fields = "*"){
    return $this->Dml_model->one($table, $condition, $fields);
  }

  function create_batch($table, $data){
    return $this->Dml_model->create_batch($table, $data);
  }

  function create($table, $data = null){
    return $this->Dml_model->create($table, $data);
  }

  function read($table, $condition = null, $fields = "*"){
    return $this->Dml_model->read($table, $condition, $fields);
  }

  function update($table, $where, $data = null){
    return $this->Dml_model->update($table, $where, $data);
  }

  function delete($table, $where){
    return $this->Dml_model->delete($table, $where);
  }

  function upload($data, $url = null, $rename = null){
    return $this->Dml_model->upload($data, $url, $rename);
  }

}