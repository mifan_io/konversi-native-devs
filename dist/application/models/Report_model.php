<?php
// https://www.teletopiasms.no/np/frontpage/gateway/api-http-examples-php
// https://stackoverflow.com/questions/5647461/how-do-i-send-a-post-request-with-php
class Report_model extends CI_Model {

  public function __construct() {
    parent::__construct();
    $this->load->database();
    $this->load->model('Dml_model');
  }

  function benefit($no){
    $table = $no . '_policy_benefit_type';
    $cols = 'DISTINCT ' . $table . '.polbt_benefit_type vals, mst_code.mco_description label';
    $join = 'JOIN mst_code ON ' . $table . '.polbt_benefit_type = mst_code.mco_mastercode';
    $where = 'WHERE mst_code.mco_status = "1" AND mst_code.mco_cat_id = "7"';
    return $this->read($table, $join . ' ' . $where, $cols);
  }

  function provider($no){
    $table = $no . '_claim';
    return $this->read($table, '', 'DISTINCT clm_provider_code vals, clm_provider_name label');
  }

  function claims($no, $start, $end){
    $table = $no . '_claim';
    $cols = 'SUM(clm_proposed_amount) proposed, SUM(clm_approved_amount) approved, SUM(clm_excess_amount) excess, COUNT(clm_claim_code) code';
    $where = 'WHERE clm_claim_date BETWEEN "' . $start . '" AND "' . $end . '"';

    return $this->one($table, $where, $cols);
  }

  function claim($no, $start, $end){
    $table = $no . '_policy_benefit_type';
    $cols = 'DISTINCT ' . $table . '.polbt_benefit_type vals, mst_code.mco_description label';
    $join = 'JOIN mst_code ON ' . $table . '.polbt_benefit_type = mst_code.mco_mastercode';
    $where = 'WHERE mst_code.mco_cat_id = "7" AND mst_code.mco_status = "1"';

    $benefit = $this->read($table, $join . ' ' . $where, $cols);

    $vals = null;
    foreach ($benefit as $key => $val) {
      $vals[] = $val['vals'];
    }

    $table = $no . '_claim';
    $cols = 'SUM(clm_proposed_amount) proposed, SUM(clm_approved_amount) approved, SUM(clm_excess_amount) excess, COUNT(clm_claim_code) code, clm_benefit_type parent';
    $where = 'WHERE clm_benefit_type IN ("' . implode('", "', $vals) . '") AND clm_claim_date BETWEEN "' . $start . '" AND "' . $end . '"';

    $claim = $this->read($no . '_claim', $where , $cols);

    foreach ($benefit as &$arr1) {
      $offer_id = $arr1['vals'];

      $match = array_filter($claim, function($v) use ($offer_id){
        return $v['parent'] == $offer_id;
      });

      $match = array_pop($match);
      $arr1['subval'] = $match;
    }
    return $benefit;
  }

  function group($no, $start, $end, $benefit = ''){
    $table = $no . '_claim';
    $jtable = $no . '_mst_member';
    $cols = 'CONCAT(mmbr_enrol_code, mmbr_sex) person, SUM(clm_proposed_amount) proposed, SUM(clm_approved_amount) approved, SUM(clm_excess_amount) excess, COUNT(clm_claim_code) code';
    $join = 'JOIN ' . $jtable . ' ON clm_member_no = mmbr_member_no';
    $where = 'WHERE mmbr_enrol_code IN ("EMP", "SPO", "CHL") AND mmbr_sex IN ("F", "M") AND clm_claim_date BETWEEN "' . $start . '" AND "' . $end . '"';
    $group = 'GROUP BY CONCAT(mmbr_enrol_code, mmbr_sex)';
    $member = ['EMPM' => 'Karyawan', 'EMPF' => 'Karyawati', 'SPOM' => 'Istri', 'SPOF' => 'Suami', 'CHLM' => 'Putra', 'CHLF' => 'Putri'];

    $benefit = (empty($benefit)) ? '' : 'AND clm_benefit_type = "' . $benefit . '"' ;

    $data = $this->read($table, $join . ' ' . $where . ' ' . $benefit . ' ' . $group, $cols);
    foreach ($data as $key => &$val) {
      $val['person'] = $member[$val['person']];
    }

    return $data;
  }

  function highest($no, $start, $end, $benefit = ''){
    $table = $no . '_claim';
    $jtable = $no . '_mst_member';
    $cols = 'mmbr_employee_id, clm_member_no, mmbr_name, mmbr_enrol_code, SUM(clm_proposed_amount) as propose, SUM(clm_approved_amount) as approve';
    $join = 'JOIN ' . $jtable . ' ON clm_member_no = mmbr_member_no';
    $where = 'WHERE clm_claim_date BETWEEN "' . $start . '" AND "' . $end . '"';
    $group = 'GROUP BY mmbr_employee_id, clm_member_no, mmbr_name, mmbr_enrol_code';
    $order = 'ORDER BY propose DESC';

    $insql = '(SELECT member.mmbr_name FROM ' . $jtable . ' AS member WHERE member.mmbr_pol_no = "' . $no . '" AND member.mmbr_enrol_code = "EMP" AND member.mmbr_employee_id = ' . $jtable . '.mmbr_employee_id) as parent';

    $benefit = (empty($benefit)) ? '' : 'AND clm_benefit_type = "' . $benefit . '"' ;

    return $this->read($table, $join . ' ' . $where . ' ' . $benefit . ' ' . $group . ' ' . $order, $cols . ', ' . $insql);
  }

  function deseases($no, $start, $end, $benefit = ''){
    $table = $no . '_claim';
    $cols = 'clm_icd1, micd_description, COUNT(clm_claim_code) code, SUM(clm_proposed_amount) as propose, SUM(clm_approved_amount) as approve';
    $join = 'JOIN mst_icd ON clm_icd1 = micd_code';
    $where = 'WHERE clm_claim_date BETWEEN "' . $start . '" AND "' . $end . '"';
    $group = 'GROUP BY clm_icd1, micd_description ORDER BY propose DESC';

    $benefit = (empty($benefit)) ? '' : 'AND clm_benefit_type = "' . $benefit . '"' ;

    return $this->read($table, $join . ' ' . $where . ' ' . $benefit . ' ' . $group, $cols);
  }


  function visit($no, $start, $end, $benefit = '', $provider = ''){
    $table = $no . '_claim';
    $cols = 'clm_provider_code, clm_provider_name, COUNT(clm_claim_code) code, SUM(clm_proposed_amount) as propose, Sum(clm_approved_amount) as approve';
    $where = 'WHERE clm_claim_code <> "" AND clm_claim_date BETWEEN "' . $start . '" AND "' . $end . '"';
    $order = 'GROUP BY clm_provider_code, clm_provider_name ORDER BY propose DESC ';

    $benefit = (empty($benefit)) ? '' : 'AND clm_benefit_type = "' . $benefit . '"' ;
    $provider = (empty($provider)) ? '' : 'AND clm_provider_code = "' . $provider . '"' ;

    return $this->read($table, $where . ' ' . $benefit . ' ' . $provider . ' ' . $order, $cols);
  }

  function procedure($procedure){
    return $this->Dml_model->procedure($procedure);
  }

  function one($table, $condition = null, $fields = "*"){
    return $this->Dml_model->one($table, $condition, $fields);
  }

  function create_batch($table, $data){
    return $this->Dml_model->create_batch($table, $data);
  }

  function create($table, $data = null){
    return $this->Dml_model->create($table, $data);
  }

  function read($table, $condition = null, $fields = "*"){
    return $this->Dml_model->read($table, $condition, $fields);
  }

  function update($table, $where, $data = null){
    return $this->Dml_model->update($table, $where, $data);
  }

  function delete($table, $where){
    return $this->Dml_model->delete($table, $where);
  }

  function upload($data, $url = null, $rename = null){
    return $this->Dml_model->upload($data, $url, $rename);
  }

}