<?php
// https://www.teletopiasms.no/np/frontpage/gateway/api-http-examples-php
// https://stackoverflow.com/questions/5647461/how-do-i-send-a-post-request-with-php
class Log_model extends CI_Model {

  public function __construct() {
    parent::__construct();
    $this->load->database();
    $this->load->model('Dml_model');
  }

  function login($username, $password, $remember = false){
    $data = [];
    $hash = 'adzhwa'.$username.'9113'.$password.'086666!@#$%^&*()ClUmSyArt';
    $mlog_password = md5(md5($hash));

    $user = $this->Dml_model->one('mst_login', 'WHERE mlog_username = "' . $username . '" AND mlog_system = "' . systemcode . '"', 'mlog_name, mlog_function, mlog_username');

    if (!empty($user)) {

      $user = $this->Dml_model->one('mst_login', 'WHERE mlog_username = "' . $username . '"AND mlog_password = "' . $mlog_password . '" AND mlog_system = "' . systemcode . '"', 'mlog_name, mlog_function, mlog_username, mlog_email');

      if (!empty($user)) {
        $_SESSION['username'] = $user['mlog_username'];
        $_SESSION['name'] = $user['mlog_name'];
        $_SESSION['division'] = $user['mlog_function'];
        $_SESSION['email'] = $user['mlog_email'];

        $update['mlog_login'] = '1';
        $update['mlog_stamp_date'] = cdatetime;

        $trans_login['tlog_id'] = $username;
        $trans_login['tlog_date'] = cdatetime;
        $trans_login['tlog_system'] = systemcode;
        $trans_login['tlog_comp_ip'] = $this->get_client_ip();

        if($remember == true){//if the Remember me is checked, it will create a cookie.
          setcookie('username', $username, time()+31536000); 
          setcookie('password', $password, time()+31536000);
        }

        $this->Dml_model->create('trans_login', $trans_login);
        $this->Dml_model->update('mst_login', 'mlog_username = "' . $username . '" AND mlog_system = "' . systemcode . '"', $update);

        if ($user) {
          $data['user'] = $user;
        } else {
          $data['error'] = 'Your Account Has Been Disabled By Your Administrator';
        }
      } else {
        $data['error'] = 'Invalid Access Password';
      }
    } else {
      $data['error'] = 'Invalid Access Username';
    }
  }

  function get_client_ip() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
      $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
      $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
      $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
      $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
      $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
      $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
      $ipaddress = 'UNKNOWN';
    return $ipaddress;
  }
}