<?php
// https://www.teletopiasms.no/np/frontpage/gateway/api-http-examples-php
// https://stackoverflow.com/questions/5647461/how-do-i-send-a-post-request-with-php
class Policy_model extends CI_Model {

  public function __construct() {
    parent::__construct();
    $this->load->database();
    $this->load->model('Dml_model');
  }

  function klas($no){
    return $this->procedure('report_policy_benefit("' . $no . '_product_class", "' . $no . '_product_benefit_plan")');
  }

  function exclusion($no){
    return $this->procedure('report_policy_exclution("' . $no . '")');  
  }

  function report($plan, $type, $id){
    return $this->procedure('report_policy_benefit_detail("' . $plan . '", "' . $type . '", "' . $id . '")');  
  }

  function status($no){
    $data['ac'] = $this->one($no . '_mst_member', 'WHERE mmbr_pol_no = "' . $no . '" AND mmbr_status = "AC"', 'COUNT(mmbr_member_no) AS kount');
    $data['su'] = $this->one($no . '_mst_member', 'WHERE mmbr_pol_no = "' . $no . '" AND mmbr_status = "SU"', 'COUNT(mmbr_member_no) AS kount');
    return $data;
  }

  function history($no){
    $cols = 'history.pda_amount, history.pda_persen_proyeksi,
             history.pda_bank_code, history.pda_date_transfer,
             history.pda_keterangan, history.pda_amount_proyeksi,
             mst_bank.msbank_bank_name, history.pda_status_proyeksi';
    $join = 'JOIN mst_bank ON history.pda_bank_code = mst_bank.msbank_id';
    $where = 'WHERE history.pda_policy_no = "' . $no . '"';
    $order = 'ORDER BY history.pda_date_transfer ASC';

    return $this->read('policy_deposite_fund_history AS history', $join . ' ' . $where . ' ' . $order, $cols);
  }

  function topup($no){
    $cols = 'pol_dedicated_account_normal, pol_dedicated_account_status_proyeksi,
             pol_dedicated_account_balance, pol_dedicated_account_proyeksi,
             pol_dedicated_account, pol_dedicated_account_status_realisasi';
    $insql = '(SELECT pda_date_transfer FROM policy_deposite_fund_history WHERE pda_policy_no = "' . $no . '" ORDER BY pda_date_transfer DESC LIMIT 1) AS history';

    return $this->one('policy', 'WHERE pol_pol_no = "' . $no . '"', $cols . ', ' . $insql);
  }

  function bank(){
    // mod-finance-paybills-claim-provider
    // mod-finance-paybills-claim-reimbursment
    return $this->Dml_model->read('mst_bank', 'WHERE msbank_status = 1 ORDER BY msbank_bank_name ASC', 'msbank_id, msbank_bank_name');
  }

  function family_benefit($no){
    $table = $no . '_product_benefit_plan';
    $cols = 'DISTINCT ' . $table . '.prdplan_ben_type, mst_code.mco_description';
    $join = 'JOIN mst_code ON ' . $table . '.prdplan_ben_type = mst_code.mco_mastercode';
    $where = 'WHERE mst_code.mco_cat_id = "7"';

    $insql = '(SELECT benfam_benefit_type
               FROM policy_benefit_family
               WHERE benfam_pol_no = "' . $no . '"
               AND benfam_status = "1"
               AND benfam_benefit_type = ' . $table . '.prdplan_ben_type)
               AS cek';

    return $this->read($table, $join . ' ' . $where, $cols . ', ' . $insql);
  }

  function family_plan($table){
    $table .= '_product_class';

    return $this->read($table, '', 'DISTINCT ' . $table . '.prdclass_plan_name, ' . $table . '.prdclass_class_code', $table);
  }

  function klass($table, $type = null, $provider = null){
    $table .= '_product_class';
    $cols = 'prdclass_provider_category, prdclass_product_code,
             mst_code.mco_description, prdclass_benefit_type,
             prdclass_benefit_plan, prdclass_class_code, prdclass_plan_name';
    $join = 'JOIN mst_code ON ' . $table . '.prdclass_benefit_type = mst_code.mco_mastercode';
    $where = 'WHERE mst_code.mco_status = "1" AND mst_code.mco_cat_id = "7" /*AND prdclass_benefit_type = "' . $type . '" AND prdclass_provider_category = "' . $provider . '"*/';
    $order = 'ORDER BY prdclass_class_code, prdclass_provider_category, prdclass_benefit_type, prdclass_benefit_plan';
    return $this->read($table, $join. ' ' . $where . ' ' . $order, $cols);
  }

  function code($no){
    return $this->one('policy', 'WHERE pol_pol_no = "' . $no . '"', 'pol_product_code AS vals, pol_product_code AS label');
  }

  function family($no){
    return $this->one('policy', 'WHERE pol_pol_no = "' . $no . '"', 'pol_family_benefit AS vals, pol_family_benefit AS label');
  }

  function benefit_plan($no, $type){
    return $this->read($no . '_policy_benefit_type', 'WHERE polbt_pol_no = "' . $no . '" AND polbt_benefit_type = "' . $type . '"', 'polbt_benefit_plan AS vals, polbt_benefit_plan AS label');
  }

  function benefit($no){
    $cols = 'policy_benefit_type.polbt_benefit_type AS vals, CONCAT(policy_benefit_type.polbt_benefit_type, " | ", mst_code.mco_description) AS label ';
    $join = 'JOIN mst_code ON policy_benefit_type.polbt_benefit_type = mst_code.mco_mastercode';
    $where = 'WHERE policy_benefit_type.polbt_pol_no  = "' . $no . '" AND mst_code.mco_cat_id = "7" AND mst_code.mco_status = "1"';
    $order = 'ORDER BY mco_code_id ASC';

    return $this->read('policy_benefit_type', $join . ' ' . $where . ' ' . $order, $cols);
  }

  function plan($no, $product){
    $cols = 'policy_benefit_type.polbt_benefit_type, mst_code.mco_description';
    $join = 'JOIN mst_code ON policy_benefit_type.polbt_benefit_type = mst_code.mco_mastercode';
    $where = 'WHERE mst_code.mco_cat_id = "7"
              AND mst_code.mco_status = "1"
              AND policy_benefit_type.polbt_pol_no = "' . $no . '"';

    $benefit = $this->read('policy_benefit_type', $join .' '. $where, $cols);

    $type = null;
    foreach ($benefit as $key => $val){
      $type[] = $val['polbt_benefit_type'];
    }
    $type = '"' . implode('", "', $type) . '"';

    $cols = 'DISTINCT mst_code.mco_mastercode, mst_code.mco_description, product_benefit_plan.prdplan_plan_code';
    $join = 'JOIN product_benefit_plan ON mst_code.mco_mastercode = product_benefit_plan.prdplan_ben_type';
    $where = 'WHERE mst_code.mco_cat_id = "7"
              AND mst_code.mco_status = "1"
              AND product_benefit_plan.prdplan_product_code = "' . $product . '"
              AND mst_code.mco_mastercode IN (' . $type . ')';
    $order = 'ORDER BY product_benefit_plan.prdplan_plan_code ASC';

    $code = $this->read('mst_code', $join .' '. $where .' '. $order, $cols);

    foreach ($benefit as &$arr1) {
      $offer_id = $arr1['polbt_benefit_type'];  

      $match = array_filter($code, function($v) use ($offer_id){
        return $v['mco_mastercode'] == $offer_id;
      });

      // $match = array_pop($match);
      $arr1['submenu'] = array_values($match);
    }

    return $benefit;
  }

  function no($startdate, $insurance){
    $cols = 'mnum_last_number_year, mnum_last_number_month,
            mnum_parameter, mnum_last_number_lenght,
            mnum_template, mnum_last_number_running';
    $code = $this->one('mst_code_number', 'WHERE mnum_code = 1', $cols);

    $digit = $code['mnum_last_number_lenght'];
    $last = $code['mnum_last_number_running'];
    $month = $code['mnum_last_number_month'];
    $year = $code['mnum_last_number_year'];
    $parameter = $code['mnum_parameter'];
    $template = $code['mnum_template'];

    $lenght = strlen($last);
    $years = date("Y",strtotime($startdate));
    $months = date("m",strtotime($startdate));

    $zero = null;
    $defsystem = null;
    $digit = $digit - $lenght;

    for ($i = 1; $i <= $digit; $i++) {
      $zero .= '0';
    }

    $month = ($month < 10) ? '0' . $month : $month ;
    $insurance = (empty($insurance)) ? '000' : $insurance ;

    $generate = ['C' => $template.$zero.$last,
                 'S' => $years.$months.$insurance.$zero.$last,
                 'M' => $template."-".$defsystem.".".$zero.$last.".".$month.".".$year];

    return $generate[$parameter];
  }

  function procedure($procedure){
    return $this->Dml_model->procedure($procedure);
  }

  function one($table, $condition = null, $fields = "*"){
    return $this->Dml_model->one($table, $condition, $fields);
  }

  function create_batch($table, $data){
    return $this->Dml_model->create_batch($table, $data);
  }

  function create($table, $data = null){
    return $this->Dml_model->create($table, $data);
  }

  function read($table, $condition = null, $fields = "*"){
    return $this->Dml_model->read($table, $condition, $fields);
  }

  function update($table, $where, $data = null){
    return $this->Dml_model->update($table, $where, $data);
  }

  function delete($table, $where){
    return $this->Dml_model->delete($table, $where);
  }

  function upload($data, $url = null, $rename = null){
    return $this->Dml_model->upload($data, $url, $rename);
  }

}