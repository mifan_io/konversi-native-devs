<?php
// https://www.teletopiasms.no/np/frontpage/gateway/api-http-examples-php
// https://stackoverflow.com/questions/5647461/how-do-i-send-a-post-request-with-php
class Dml_model extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	function custom($query){
		$this->db->query($query)->result_array();
	}

	function field($table){
		return $this->db->query("DESCRIBE ".$table)->result_array();
	}

	function procedure($procedure){
		return $this->db->query("CALL $procedure")->result_array();
	}

	function create_batch($table, $data){
		return $this->db->insert_batch($table, $data);
	}

	function create($table, $data = array()){
		if(empty($data)){
			$Field = $this->field($table);
			foreach($Field as $Fields){
				if($Fields['Type'] != 'timestamp'){
					$data[$Fields['Field']] = $_REQUEST[$Fields['Field']];
				}
			}
		}
		$this->db->insert($table, $data);
		return $this->db->insert_id();
	}

	function read($table, $condition = null, $fields = "*"){
		return $this->db->query("SELECT $fields FROM $table $condition")->result_array();
	}

	function update($table,$where,$data = null){
		if(empty($data)){
			$Field = $this->field($table);
			foreach($Field as $Fields){
				if($Fields['Type'] != 'timestamp'){
					$data[$Fields['Field']] = $_POST[$Fields['Field']];
				}
			}
		}
		return $this->db->update($table, $data, $where);
	}

	function delete($table, $where){
		return $this->db->delete($table, $where); 
	}

	function creade($create, $read, $condition, $data){
		$key = implode(', ', array_keys($data));
		$val = implode(', ', $data);
		return $this->db->query("INSERT $create ($key) SELECT $val FROM $table $condition")->result_array();
	}

	function one($table, $condition = null, $fields = "*"){
		return $this->db->query("SELECT $fields FROM $table $condition")->row_array();
	}

	function dates($data){
		$date = date("Y-m-d",strtotime($data));
		return $date;
	}

	function upload($data, $url = null, $rename = null){
		$files = $_FILES[$data];
		$name = $files['name'];
		move_uploaded_file($files['tmp_name'], "$url/".$name);
		if (!is_null($rename)) {
			$tipe = explode(".", $name);
			$rename = $rename.".".end($tipe);
			rename("$url/".$name, "$url/".$rename);
			$files['name'] = $rename;
			$name = $rename;
		}
		chmod("$url/".$name, 0777);
		return $files;
	}

	function uploads($data, $url = null, $rename = null){
		$files = $data;
		$name = $files['name'];
		move_uploaded_file($files['tmp_name'], "$url/".$name);
		if (!is_null($rename)) {
			$tipe = explode(".", $name);
			$rename = $rename.".".end($tipe);
			rename("$url/".$name, "$url/".$rename);
			$files['name'] = $rename;
			$name = $rename;
		}
		chmod("$url/".$name, 0777);
		return $files;
	}

	function http_put_data(\stdClass &$a_data) {
		$input = file_get_contents('php://input');
		preg_match('/boundary=(.*)$/', $_SERVER['CONTENT_TYPE'], $matches);
		if ($matches) {
			$boundary = $matches[1];
			$a_blocks = preg_split("/-+$boundary/", $input);
			array_pop($a_blocks);
		} else {
			parse_str($input, $a_blocks);
		}

		foreach ($a_blocks as $id => $block) {
			if (empty($block))
				continue;

			if (strpos($block, 'application/octet-stream') !== FALSE) {
				preg_match("/name=\"([^\"]*)\".*stream[\n|\r]+([^\n\r].*)?$/s", $block, $matches);
			}
			else {
				preg_match('/name=\"([^\"]*)\"[\n|\r]+([^\n\r].*)?\r$/s', $block, $matches);
				// preg_match('/filename="([^"\\]*(?:\\.[^"\\]*)*)"/i', $block, $matches);
			}
			if ($matches) {
				$a_data->{$matches[1]} = $matches[2];
			} else {
				$a_data->{$id} = $block;
			}
		}
	}

}