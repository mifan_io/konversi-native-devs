<?php
// https://www.teletopiasms.no/np/frontpage/gateway/api-http-examples-php
// https://stackoverflow.com/questions/5647461/how-do-i-send-a-post-request-with-php
class Eoc_model extends CI_Model {

  public function __construct() {
    parent::__construct();
    $this->load->database();
    $this->load->model('Dml_model');
  }

  function benefit($table){
    // inquiry, master
    return $this->Dml_model->read($table, 'INNER JOIN mst_code ON $table_bentype.polbt_benefit_type = mst_code.mco_mastercode WHERE mst_code.mco_cat_id = 7 AND mst_code.mco_status = 1', 'DISTINCT (' . $table . '.polbt_benefit_type) polbt_benefit_type, mst_code.mco_description');
  }

  function code($username, $code){
    // invoice, master
    return $this->Dml_model->one('temp_invoice_eoc', 'WHERE tmeoc_username = "' . $username . '" AND tmeoc_claim_code = "' . $code . '"', 'tmeoc_claim_code');
  }

  function excess($username){
    // invoice
    return $this->Dml_model->one('temp_invoice_eoc', 'WHERE tmeoc_username = "' . $username . '"', 'COUNT(tmeoc_claim_code) AS count, SUM(tmeoc_claim_excess) AS sum');
  }

  function detail($username){
    // invoice
    $cols = 'tmeoc_claim_code, tmeoc_claim_date, tmeoc_member_no,
             tmeoc_benefit_type, tmeoc_claim_proposed,
             tmeoc_claim_approved, tmeoc_claim_excess,
             tmeoc_member_name, tmeoc_provider_code,
             tmeoc_icd_code, tmeoc_icd_description,
             tmeoc_provider_name, tmeoc_claim_type,
             tmeoc_claim_note, tmeoc_claim_source,
             tmeoc_serv_in, tmeoc_serv_out';
    return $this->Dml_model->read('temp_invoice_eoc', 'WHERE tmeoc_username = "' . $username . '"', $cols);
  }

  function procedure($procedure){
    return $this->Dml_model->procedure($procedure);
  }

  function create($table, $data = null){
    return $this->Dml_model->create($table, $data);
  }

  function update($table, $where, $data = null){
    return $this->Dml_model->update($table, $where, $data);
  }

  function delete($table, $where){
    return $this->Dml_model->delete($table, $where);
  }

  function upload($data, $url = null, $rename = null){
    return $this->Dml_model->upload($data, $url, $rename);
  }

}