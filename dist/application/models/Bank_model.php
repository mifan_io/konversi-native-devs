<?php
// https://www.teletopiasms.no/np/frontpage/gateway/api-http-examples-php
// https://stackoverflow.com/questions/5647461/how-do-i-send-a-post-request-with-php
class Bank_model extends CI_Model {

  public function __construct() {
    parent::__construct();
    $this->load->database();
    $this->load->model('Dml_model');
  }

  function bank(){
    return $this->Dml_model->read('mst_bank', 'WHERE msbank_status = 1 ORDER BY msbank_bank_name ASC', 'msbank_id AS vals, msbank_bank_name AS label');
  }

}