<!DOCTYPE html>
<html lang="en">

<head>
  <title>Medicatrix Healthcare</title>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta http-equiv="content-type" content="text/html;charset=utf-8" />
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.png">
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/css.css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="<?php echo base_url(); ?>assets/css/material-dashboard.min.css?v=2.0.2" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="<?php echo base_url(); ?>assets/css/demo.css" rel="stylesheet" />
  <!-- select2 -->
  <link href="<?php echo base_url(); ?>assets/css/select2.min.css" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet" />
</head>

<body class="">

  <div class="wrapper">

    <div class="sidebar" data-color="purple" data-background-color="black" data-image="<?php echo base_url(); ?>assets/img/sidebar-1.jpg">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"
        Tip 2: you can also add an image using data-image tag
      -->

      <div class="logo">
        <a href="#" class="simple-text logo-mini">MC</a>
        <a href="#" class="simple-text logo-normal">Medicatrix Care</a>
      </div>

      <div class="sidebar-wrapper">

        <div class="user">
          <div class="photo"><img src="<?php echo base_url(); ?>assets/img/default-avatar.png" /></div>
          <div class="user-info">
            <a data-toggle="collapse" href="#" class="username"><span><?php echo $_SESSION['name']; ?><!-- <b class="caret"></b> --></span></a>
            <!-- <div class="collapse" id="collapseExample">
              <ul class="nav">
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url() . 'administrator/profile'; ?>">
                    <span class="sidebar-mini"><i class="material-icons">face</i></span>
                    <span class="sidebar-normal"> My Profile </span>
                  </a>
                </li>
              </ul>
            </div> -->
          </div>
        </div>
        <!-- <?php echo $class; ?>
        <?php echo $method; ?>
        <?php echo $supmenu; ?>
        <?php echo $submenu; ?> -->
        <ul class="nav">
        <?php $admin = null; $caption = ['Logout', 'Change Password'];
        foreach ($menu as $key => $value) { ?>
          <li class="nav-item <?php echo ($value['msm_menu_category'] == $supmenu) ? 'active' : '' ; ?>">
            <a class="nav-link" data-toggle="collapse" href="#pages<?php echo $value['msm_menu_category']; ?>" <?php echo ($value['msm_menu_category'] == $supmenu) ? 'aria-expanded="true"' : '' ; ?>>
              <i class="<?php echo $value['msm_class_fa']; ?>"></i>
              <p><?php echo $value['msm_menu_category']; ?><b class="caret"></b></p>
            </a>

            <div class="collapse <?php echo ($value['msm_menu_category'] == $supmenu) ? 'show' : '' ; ?>" id="pages<?php echo $value['msm_menu_category']; ?>">
              <ul class="nav">
              <?php foreach ($value['submenu'] as $k => $val) { ?>
                <?php if ($value['msm_menu_category'] == 'Administrator' && in_array($val['mss_submenu_desc'], $caption)){
                  $admin .= '<a class="dropdown-item" id="' . $val['mss_url'] . '" href="' . base_url().$val['mss_url'] . '">' . $val['mss_submenu_desc'] . '</a>';
                } else { ?>
                <li class="nav-item <?php echo ($val['mss_submenu_desc'] == $submenu) ? 'active' : '' ; ?>">
                  <a class="nav-link" href="<?php echo base_url().$val['mss_url']; ?>">
                    <span class="sidebar-mini"><i class="<?php echo ($val['mss_submenu_desc'] == $submenu) ? '' : 'text-primary' ; ?> material-icons">label_important</i></span>
                    <span class="sidebar-normal"><?php echo $val['mss_submenu_desc']; ?></span>
                  </a>
                </li>
                <?php } ?>
              <?php } ?>
              </ul>
            </div>
          </li>
        <?php } ?>
        </ul>

      </div>

    </div>


    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top " id="navigation-example">

        <div class="container-fluid">

          <div class="navbar-wrapper">
            <div class="navbar-minimize">
              <button id="minimizeSidebar" class="btn btn-just-icon btn-white btn-fab btn-round">
                <i class="material-icons text_align-center visible-on-sidebar-regular">more_vert</i>
                <i class="material-icons design_bullet-list-67 visible-on-sidebar-mini">view_list</i>
              </button>
            </div>
            <?php $class = ($class == 'index') ? 'dashboard' : $class ; ?>
            <a class="navbar-brand" href="#pablo"><?php echo ucfirst($class); ?></a>
          </div>

          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index"
            aria-expanded="false" aria-label="Toggle navigation" data-target="#navigation-example">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>

          <div class="collapse navbar-collapse justify-content-end">

            <form class="navbar-form">
              <div class="input-group no-border">
                <input type="text" value="" class="form-control" placeholder="Search...">
                <button type="submit" class="btn btn-white btn-round btn-just-icon">
                  <i class="material-icons">search</i>
                  <div class="ripple-container"></div>
                </button>
              </div>
            </form>

            <ul class="navbar-nav">
              <!-- <li class="nav-item">
                <a class="nav-link" href="#pablo">
                  <i class="material-icons">dashboard</i>
                  <p class="d-lg-none d-md-block">
                    Stats
                  </p>
                </a>
              </li>

              <li class="nav-item dropdown">
                <a class="nav-link" href="http://example.com/" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">notifications</i>
                  <span class="notification">5</span>
                  <p class="d-lg-none d-md-block">
                    Some Actions
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink"><?php echo $admin; ?></div>
              </li> -->

              <li class="nav-item dropdown">
                <a class="nav-link" href="#" id="navbarAdmin" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarAdmin"><?php echo $admin; ?></div>
                <form method="post" class="logout" action="<?php echo base_url(); ?>index/logout"></form>
              </li>
            </ul>

          </div>

        </div>

      </nav>
      <!-- End Navbar -->