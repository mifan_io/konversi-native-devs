<div class="content">
  <div class="content">
    <div class="container-fluid">

      <div class="row">
        <div class="col-md-12">
          <div class="card ">

            <div class="card-header card-header-info card-header-icon">
              <div class="card-icon">
                <i class="material-icons">library_add</i>
              </div>
              <h4 class="card-title">Dashboard Manage care</h4>
            </div>

            <div class="card-body ">
              <div class="row">
                <div class="col-md-12">

                  <div class="material-datatables">
                    <table class="table table-striped table-no-bordered table-hover datatables" cellspacing="0" width="100%" style="width:100%">
                      <thead>
                        <tr>
                          <th><i class="fa fa-hashtag"></i></th>
                          <th>Case No</th>
                          <th>Date</th>
                          <th>Ben Type</th>
                          <th>NIK</th>
                          <th>Full Name</th>
                          <th>Serv IN</th>
                          <th>Provider</th>
                          <th>Treating</th>
                          <th>Reff Doctor</th>
                          <th>Status</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php foreach ($data as $key => $val) { ?>
                        <tr>
                          <td><?php echo ($key + 1); ?></td>
                          <td><?php echo $val['cmm_case_no']; ?></td>
                          <td><?php echo $val['mprov_stamp']; ?></td>
                          <td><?php echo $val['cmm_benefit_type']; ?></td>
                          <td><?php echo $val['cmm_member_no']; ?></td>
                          <td><?php echo $val['cmm_member_name']; ?></td>
                          <td><?php echo $val['cmm_service_in']; ?></td>
                          <td><?php echo $val['mprov_name']; ?></td>
                          <td><?php echo $val['cmm_treating']; ?></td>
                          <td><?php echo $val['cmm_reff_doctor']; ?></td>
                          <td><span class="label <?php echo $label[$val['cmm_status']]; ?>"><?php echo $status[$val['cmm_status']]; ?></span></td>
                        </tr>
                      <?php } ?>
                      </tbody>
                    </table>
                  </div>

                </div>
              </div>
            </div>

          </div>
        </div>
      </div>

    </div>
  </div>
</div>