<div class="content">
  <div class="content">
    <div class="container-fluid">

      <?php if (isset($_SESSION['users_input']) && $_SESSION['users_input'] == true) { ?>
      <div class="alert alert-<?php echo $_SESSION['users_status']; ?>">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <i class="material-icons">close</i>
        </button>
        <span><b class="text-capitalize"> <?php echo $_SESSION['users_status']; ?> - </b> <?php echo $_SESSION['users_text']; ?></span>
      </div>
      <?php } unset($_SESSION['users_input']); unset($_SESSION['users_status']); unset($_SESSION['users_text']); ?>

      <div class="row">
        <div class="col-md-12">
          <div class="card ">

            <div class="card-header card-header-success card-header-icon">
              <div class="card-icon">
                <i class="material-icons">assignment</i>
              </div>
              <h4 class="card-title">Setup Produk Polis Benefit</h4>
            </div>

            <div class="card-body ">
              <form method="post" class="form-horizontal">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">Periode Polis (<span id="periode"></span>)</div>
                    <div class="form-group">
                      <div class="input-group no-border">
                        <input type="text" id="no" class="form-control" placeholder="Nomor Polis" readonly="readonly">
                        <button onclick="large('noticeModal');" type="button" data-toggle="modal" class="btn btn-white btn-round btn-just-icon" data-target="#noticeModal">
                          <i class="material-icons">search</i>
                          <div class="ripple-container"></div>
                        </button>
                      </div>
                    </div>

                    <div class="row benefit-option" style="display: none;">
                      <div class="col-md-6">
                        <div class="form-group">
                          <div class="input-group no-border">
                            <select class="selectpicker" name="prdplan_ben_type" id="benefit" data-style="select-with-transition" title="Choose Benefit Plan" data-size="7" onchange="chained('policy/benefit_type', val.value, '#type', this.value); _benefit('policy/benefit_data', code.value, '#benefit_list', this.value, type.value);" required="required"></select>
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="input-group no-border">
                            <select class="selectpicker" name="prdplan_ben_code" id="type" data-style="select-with-transition" title="Choose Benefit Type" data-size="7" onchange="_benefit('policy/benefit_data', code.value, '#benefit_list', benefit.value, this.value); SaveAs.value = this.value;" required="required"></select>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <div class="input-group no-border">
                            <select class="selectpicker" name="prdplan_provider_category" id="provider" data-style="select-with-transition" title="Choose Provider" data-size="7" required="required">
                            <?php foreach ($provider as $key => $val) { ?>
                              <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                            <?php } ?>
                            </select>
                          </div>
                        </div>

                        <div class="input-group form-control-md">
                          <div class="form-group bmd-form-group">
                            <label for="SaveAs" class="bmd-label-floating">Save As</label>
                            <input type="text" class="form-control" id="SaveAs" name="prdplan_plan_code" required="required">
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              
                <div class="row">
                  <div class="col-md-12">
                    <div class="material-datatables">
                      <table class="table table-striped table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                          <tr>
                            <th><i class="fa fa-hashtag"></i></th>
                            <th>Ben Code</th>
                            <th>Deskripsi</th>
                            <th>Kategori</th>
                            <td align=center><b>Covered (%)</b></td>
                            <th>Max Visit</th>
                            <th>Max Day</th>
                            <th>Ben Amount</th>
                            <th>
                              Ascharge
                              <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                  <input class="form-check-input" type="checkbox" id="check" onclick="checkall(this, '.checker');">
                                  <span class="form-check-sign">
                                    <span class="check"></span>
                                  </span>
                                </label>
                              </div>
                            </th>
                          </tr>
                        </thead>
                        <tbody id="benefit_list"></tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="row text-right">
                  <div class="col-md-9">&nbsp;</div>
                  <div class="col-md-3">
                    <input type="hidden" id="val" class="form-control" name="no">
                    <input type="hidden" id="val2" class="form-control">
                    <input type="hidden" id="val3" class="form-control">
                    <input type="hidden" id="code" class="form-control" name="prdplan_product_code">
                    <button type="submit" class="btn btn-fill btn-primary submit" disabled="disabled">Submit</button>
                  </div>
                </div>
              </form>
            </div>

          </div>
        </div>
      </div>

    </div>
  </div>
</div>

<!-- notice modal -->
<div class="modal fade bs-example-modal-lg" id="noticeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="padding-right: 50%">
  <div class="modal-dialog modal-notice modal-lg" style="width: 80%;">
    <div class="modal-content" style="width: 150%;">
      <div class="modal-header">
        <h5 class="modal-title" id="myModalLabel">Inquery Data Policy</h5>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
          <i class="material-icons">close</i>
        </button>
      </div>
      <div class="modal-body">
        <div class="instruction">
          <div class="row">
            <div class="col-md-12">
              <div class="material-datatables">
                <table class="table table-striped table-no-bordered table-hover datatables" cellspacing="0" width="100%" style="width:100%">
                  <thead>
                    <tr>
                      <th><i class="fa fa-hashtag"></i></th>
                      <th>No. Polis</th>
                      <th>Policy Name</th>
                      <th>Start Date</th>
                      <th>End Date</th>
                      <th>Member(s)</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php foreach ($data as $key => $val) { ?>
                    <tr>
                      <td><?php echo ($key + 1); ?></td>
                      <td>
                        <a data-dismiss="modal" href="#" onclick=" display('.benefit-option'); code('policy/code', '<?php echo $val['pol_pol_no']; ?>', '#code'); chained('policy/benefit_list', '<?php echo $val['pol_pol_no']; ?>', '#benefit'); binding('#benefit<?php echo $key; ?>');">
                          <?php echo $val['pol_pol_no']; ?>
                          </a>
                      </td>
                      <td><?php echo $val['pol_company_name']; ?></td>
                      <td><?php echo $val['pol_start_date']; ?></td>
                      <td><?php echo $val['pol_end_date']; ?></td>
                      <td class="text-right"><?php echo $val['pol_member']; ?></td>
                      <td>
                        <?php echo $val['pol_status']; ?>
                        <input type="hidden" id="benefit<?php echo $key; ?>" value='<?php echo json_encode($val); ?>'>
                      </td>
                    </tr>
                  <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end notice modal -->