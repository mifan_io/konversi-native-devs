<div class="content">
  <div class="container-fluid">

    <?php if (isset($_SESSION['users_input']) && $_SESSION['users_input'] == true) { ?>
    <div class="alert alert-<?php echo $_SESSION['users_status']; ?>">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <i class="material-icons">close</i>
      </button>
      <span><b class="text-capitalize"> <?php echo $_SESSION['users_status']; ?> - </b> <?php echo $_SESSION['users_text']; ?></span>
    </div>
    <?php } unset($_SESSION['users_input']); unset($_SESSION['users_status']); unset($_SESSION['users_text']); ?>

    <div class="col-md-12 mr-auto ml-auto">
      <div class="wizard-container">
        <div class="card card-wizard active" data-color="purple" id="wizardProfile">
          <form action="" method="post">
            <!--        You can switch " data-color="primary" "  with one of the next bright colors: "green", "orange", "red", "blue"       -->
            <div class="card-header text-center">
              <h3 class="card-title">Member Policy</h3>
            </div>

            <div class="wizard-navigation">
              <ul class="nav nav-pills">
                <li class="nav-item" style="width: 25%;">
                  <a class="nav-link active" href="#about" data-toggle="tab" role="tab">Profile</a>
                </li>
                <li class="nav-item" style="width: 25%;">
                  <a class="nav-link active" href="#member" data-toggle="tab" role="tab">Membership</a>
                </li>
                <li class="nav-item" style="width: 25%;">
                  <a class="nav-link" href="#account" data-toggle="tab" role="tab">Benefit</a>
                </li>
                <li class="nav-item" style="width: 25%;">
                  <a class="nav-link" href="#address" data-toggle="tab" role="tab">Finance</a>
                </li>
              </ul>
              <div class="moving-tab" style="width: 219.109px; transform: translate3d(-8px, 0px, 0px); transition: transform 0s ease 0s;">About</div>
            </div>

            <div class="card-body">
              <div class="tab-content">

                <div class="tab-pane active" id="about">
                  <h5 class="info-text">Profile</h5>
                  <div class="row justify-content-center">
                    <div class="col-md-12">

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <div class="input-group no-border">
                            <input type="text" id="no" class="form-control" placeholder="Nomor Polis" readonly="readonly">
                            <button onclick="large('noticeModal');" type="button" data-toggle="modal" class="btn btn-white btn-round btn-just-icon" data-target="#noticeModal">
                              <i class="material-icons">search</i>
                              <div class="ripple-container"></div>
                            </button>
                          </div>
                        </div>
                        <div class="form-group">Periode Polis (<span id="periode"></span>)</div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput1" class="bmd-label-floating">Perusahaan</label>
                          <input type="text" class="form-control" id="exampleInput1" name="pol_company_name" required="required">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput2" class="bmd-label-floating">Alamat</label>
                          <input type="text" class="form-control" id="exampleInput2" name="pol_address" required="required">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput3" class="bmd-label-floating">Kota</label>
                          <input type="text" class="form-control" id="exampleInput3" name="pol_city" required="required">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput4" class="bmd-label-floating">Province</label>
                          <input type="text" class="form-control" id="exampleInput4"  name="pol_province" required="required">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput5" class="bmd-label-floating">Zipcode</label>
                          <input type="number" class="form-control num" id="exampleInput5"  name="pol_zipcode" required="required">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput6" class="bmd-label-floating">Phone</label>
                          <input type="number" class="form-control num" id="exampleInput6" name="pol_phone" required="required">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput7" class="bmd-label-floating">Fax</label>
                          <input type="number" class="form-control num" id="exampleInput7"  name="pol_fax" required="required">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput8" class="bmd-label-floating">Contact</label>
                          <input type="number" class="form-control num" id="exampleInput8" name="pol_cp" required="required">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput9" class="bmd-label-floating">Email CP</label>
                          <input type="email" class="form-control" id="exampleInput9" name="pol_cp_email" required="required">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput10" class="bmd-label-floating">Start Date</label>
                          <input type="text" class="form-control datepicker" id="exampleInput10" name="pol_start_date" required="required">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput11" class="bmd-label-floating">End Date</label>
                          <input type="text" class="form-control datepicker" id="exampleInput11" name="pol_end_date" required="required">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <!-- <label for="exampleInput12" class="bmd-label-floating">Holding</label> -->
                          <select class="selectpicker" name="pol_holding_mode" id="exampleInput12" data-style="select-with-transition" title="Choose Holding Mode" data-size="7" required="required">
                          <?php foreach ($holding as $key => $val) { ?>
                            <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                          <?php } ?>
                          </select>
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput13" class="bmd-label-floating">Branch</label>
                          <input type="text" class="form-control" id="exampleInput13" name="pol_branch_code" required="required">
                        </div>
                      </div>

                    </div>
                  </div>
                </div>

                <div class="tab-pane" id="member">
                  <h5 class="info-text">Benefit</h5>
                  <div class="row justify-content-center">
                    <div class="col-md-12">

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput14" class="bmd-label-floating">Insurance</label>
                          <input type="text" class="form-control num" id="exampleInput14" name="pol_insurance_code" required="required">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label class="col-sm-2 col-form-label label-checkbox">Benefit Type</label>
                          <div class="col-sm-10 checkbox-radios"><?php echo $polbt_benefit_type; ?></div>
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <select class="selectpicker" name="pol_product_code" data-style="select-with-transition" title="Choose Product" data-size="7" required="required"><?php echo $pol_product_code; ?></select>
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <select class="selectpicker" name="pol_services_type" data-style="select-with-transition" title="Choose Service" data-size="7" required="required"><?php echo $pol_services_type; ?></select>
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput15" class="bmd-label-floating">Cost Share</label>
                          <input type="text" class="form-control num" id="exampleInput15" name="pol_cost_share" required="required">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput16" class="bmd-label-floating">Recovery Claim</label>
                          <input type="text" class="form-control num" id="exampleInput16" name="pol_recovery_claim" required="required">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput17" class="bmd-label-floating">Expired Claim</label>
                          <input type="text" class="form-control num" id="exampleInput17" name="pol_kadaluarsa_claim" required="required">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label class="col-sm-2 col-form-label label-checkbox">Family Benefit</label>
                          <div class="col-sm-10 checkbox-radios">
                          <?php foreach ($family as $key => $val) { ?>
                            <div class="form-check form-check-inline">
                              <label class="form-check-label">
                                <input class="form-check-input" name="pol_family_benefit" type="radio" value="<?php echo $key; ?>"> <?php echo $val; ?>
                                <span class="circle">
                                  <span class="check"></span>
                                </span>
                              </label>
                            </div>
                          <?php } ?>
                          </div>
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput18" class="bmd-label-floating">Refund Endorst</label>
                          <input type="text" class="form-control num" id="exampleInput18" name="pol_refund_endorsement" required="required">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput19" class="bmd-label-floating">SLA</label>
                          <input type="text" class="form-control num" id="exampleInput19" name="pol_service_level_day" required="required">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput20" class="bmd-label-floating">Case Monitoring Amount</label>
                          <input type="text" class="form-control num" id="exampleInput20" name="pol_casemonitoring_platform" required="required">
                        </div>
                      </div>

                    </div>
                  </div>
                </div>

                <div class="tab-pane" id="account">
                  <h5 class="info-text">Benefit</h5>
                  <div class="row justify-content-center">
                    <div class="col-md-12">

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput14" class="bmd-label-floating">Insurance</label>
                          <input type="text" class="form-control num" id="exampleInput14" name="pol_insurance_code" required="required">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label class="col-sm-2 col-form-label label-checkbox">Benefit Type</label>
                          <div class="col-sm-10 checkbox-radios"><?php echo $polbt_benefit_type; ?></div>
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <select class="selectpicker" name="pol_product_code" data-style="select-with-transition" title="Choose Product" data-size="7" required="required"><?php echo $pol_product_code; ?></select>
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <select class="selectpicker" name="pol_services_type" data-style="select-with-transition" title="Choose Service" data-size="7" required="required"><?php echo $pol_services_type; ?></select>
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput15" class="bmd-label-floating">Cost Share</label>
                          <input type="text" class="form-control num" id="exampleInput15" name="pol_cost_share" required="required">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput16" class="bmd-label-floating">Recovery Claim</label>
                          <input type="text" class="form-control num" id="exampleInput16" name="pol_recovery_claim" required="required">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput17" class="bmd-label-floating">Expired Claim</label>
                          <input type="text" class="form-control num" id="exampleInput17" name="pol_kadaluarsa_claim" required="required">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label class="col-sm-2 col-form-label label-checkbox">Family Benefit</label>
                          <div class="col-sm-10 checkbox-radios">
                          <?php foreach ($family as $key => $val) { ?>
                            <div class="form-check form-check-inline">
                              <label class="form-check-label">
                                <input class="form-check-input" name="pol_family_benefit" type="radio" value="<?php echo $key; ?>"> <?php echo $val; ?>
                                <span class="circle">
                                  <span class="check"></span>
                                </span>
                              </label>
                            </div>
                          <?php } ?>
                          </div>
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput18" class="bmd-label-floating">Refund Endorst</label>
                          <input type="text" class="form-control num" id="exampleInput18" name="pol_refund_endorsement" required="required">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput19" class="bmd-label-floating">SLA</label>
                          <input type="text" class="form-control num" id="exampleInput19" name="pol_service_level_day" required="required">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput20" class="bmd-label-floating">Case Monitoring Amount</label>
                          <input type="text" class="form-control num" id="exampleInput20" name="pol_casemonitoring_platform" required="required">
                        </div>
                      </div>

                    </div>
                  </div>
                </div>

                <div class="tab-pane" id="address">
                  <h5 class="info-text">Finance</h5>
                  <div class="row justify-content-center">
                    <div class="col-md-12">

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <select class="selectpicker" name="pol_currency" data-style="select-with-transition" title="Choose Currency" data-size="7" required="required">
                          <?php foreach ($currency as $key => $val) { ?>
                            <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                          <?php } ?>
                          </select>
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput14" class="bmd-label-floating">Fee TPA</label>
                          <input type="text" class="form-control num" id="exampleInput21" name="pol_fee_tpa" required="required">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput14" class="bmd-label-floating">Fee Card Member</label>
                          <input type="text" class="form-control num" id="exampleInput21" name="pol_fee_card" required="required">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput14" class="bmd-label-floating">Dedicated Account</label>
                          <input type="text" class="form-control num" id="exampleInput21" name="pol_dedicated_account" required="required">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <select class="selectpicker" name="pol_payment_mode" data-style="select-with-transition" title="Choose Payment Mode" data-size="7" required="required">
                            <?php echo $pol_payment_mode; ?>
                          </select>
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <select class="selectpicker" name="pol_payment_to" data-style="select-with-transition" title="Choose Payment To" data-size="7" required="required">
                          <?php foreach ($payment as $key => $val) { ?>
                            <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                          <?php } ?>
                          </select>
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <select class="selectpicker" name="pol_agent" data-style="select-with-transition" title="Choose Agent" data-size="7" required="required">
                          <?php foreach ($agent as $key => $val) { ?>
                            <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                          <?php } ?>
                          </select>
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput14" class="bmd-label-floating">Commision</label>
                          <input type="text" class="form-control num" id="exampleInput21" name="pol_commision" required="required">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput14" class="bmd-label-floating">ACC Number</label>
                          <input type="text" class="form-control num" id="exampleInput21" name="pol_acc_number" required="required">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput14" class="bmd-label-floating">ACC Name</label>
                          <input type="text" class="form-control num" id="exampleInput21" name="pol_acc_name" required="required">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <select class="selectpicker" name="pol_acc_bank" data-style="select-with-transition" title="Choose Bank" data-size="7" required="required">
                          <?php foreach ($bank as $key => $val) { ?>
                            <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                          <?php } ?>
                          </select>
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput14" class="bmd-label-floating">ACC Branch</label>
                          <input type="text" class="form-control num" id="exampleInput21" name="pol_acc_branch" required="required">
                        </div>
                      </div>

                    </div>
                  </div>
                </div>

              </div>
            </div>
            <div class="card-footer">
              <div class="mr-auto">
                <button type="button" class="btn btn-previous btn-fill btn-rose btn-wd disabled">Previous</button>
                <!-- <input type="button" class="btn btn-previous btn-fill btn-rose btn-wd disabled" name="previous" value="Previous"> -->
              </div>
              <div class="ml-auto">
                <button type="button" class="btn btn-next btn-fill btn-primary btn-wd">Next</button>
                <button type="submit" class="btn btn-finish btn-fill btn-primary btn-wd" style="display: none;">Finish</button>
                <!-- <input type="button" class="btn btn-next btn-fill btn-primary btn-wd" name="next" value="Next"> -->
                <!-- <input type="submit" class="btn btn-finish btn-fill btn-primary btn-wd" name="finish" value="Finish" style="display: none;"> -->
              </div>
              <div class="clearfix"></div>
            </div>
          </form>
        </div>
      </div>
    </div>

  </div>
</div>


<!-- notice modal -->
<div class="modal fade bs-example-modal-lg" id="noticeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="padding-right: 50%">
  <div class="modal-dialog modal-notice modal-lg" style="width: 80%;">
    <div class="modal-content" style="width: 150%;">
      <div class="modal-header">
        <h5 class="modal-title" id="myModalLabel">Inquery Data Policy</h5>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
          <i class="material-icons">close</i>
        </button>
      </div>
      <div class="modal-body">
        <div class="instruction">
          <div class="row">
            <div class="col-md-12">
              <div class="material-datatables">
                <table class="table table-striped table-no-bordered table-hover datatables" cellspacing="0" width="100%" style="width:100%">
                  <thead>
                    <tr>
                      <th><i class="fa fa-hashtag"></i></th>
                      <th>No. Polis</th>
                      <th>Policy Name</th>
                      <th>Start Date</th>
                      <th>End Date</th>
                      <th>Member(s)</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php foreach ($data as $key => $val) { ?>
                    <tr>
                      <td><?php echo ($key + 1); ?></td>
                      <td>
                        <a data-dismiss="modal" href="#" onclick=" display('.benefit-option'); code('policy/code', '<?php echo $val['pol_pol_no']; ?>', '#code'); _family('policy/family_data', '<?php echo $val['pol_pol_no']; ?>', '#class_list'); binding('#benefit<?php echo $key; ?>');">
                          <?php echo $val['pol_pol_no']; ?>
                          </a>
                      </td>
                      <td><?php echo $val['pol_company_name']; ?></td>
                      <td><?php echo $val['pol_start_date']; ?></td>
                      <td><?php echo $val['pol_end_date']; ?></td>
                      <td class="text-right"><?php echo $val['pol_member']; ?></td>
                      <td>
                        <?php echo $val['pol_status']; ?>
                        <input type="hidden" id="benefit<?php echo $key; ?>" value='<?php echo json_encode($val); ?>'>
                      </td>
                    </tr>
                  <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end notice modal -->