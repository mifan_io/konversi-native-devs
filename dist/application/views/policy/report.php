<div class="content">
  <div class="content">
    <div class="container-fluid">

      <?php if (isset($_SESSION['users_input']) && $_SESSION['users_input'] == true) { ?>
      <div class="alert alert-<?php echo $_SESSION['users_status']; ?>">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <i class="material-icons">close</i>
        </button>
        <span><b class="text-capitalize"> <?php echo $_SESSION['users_status']; ?> - </b> <?php echo $_SESSION['users_text']; ?></span>
      </div>
      <?php } unset($_SESSION['users_input']); unset($_SESSION['users_status']); unset($_SESSION['users_text']); ?>

      <div class="row">
        <div class="col-md-12">
          <div class="card ">

            <div class="card-header card-header-tabs card-header-info">
              <div class="nav-tabs-navigation">
                <div class="nav-tabs-wrapper">
                  <ul class="nav nav-tabs" data-tabs="tabs">
                    <li class="nav-item">
                      <a class="nav-link active" href="#link1" data-toggle="tab">
                        <i class="material-icons">assignment</i> Class Benefit
                        <div class="ripple-container"></div>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#link2" data-toggle="tab">
                        <i class="material-icons">assignment_late</i> Exclusion Description
                        <div class="ripple-container"></div>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>

            <div class="card-body ">
              <div class="tab-content tab-space">

                <div class="form-group">Periode Polis (<span id="periode"></span>)</div>
                <div class="form-group">
                  <div class="input-group no-border">
                    <input type="text" id="no" class="form-control" placeholder="Nomor Polis" readonly="readonly">
                    <button onclick="large('noticeModal');" type="button" data-toggle="modal" class="btn btn-white btn-round btn-just-icon" data-target="#noticeModal">
                      <i class="material-icons">search</i>
                      <div class="ripple-container"></div>
                    </button>
                  </div>
                </div>

                <div class="tab-pane active" id="link1">
                  <div class="material-datatables">
                    <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                      <thead>
                        <tr>
                          <th><i class="fa fa-hashtag"></i></th>
                          <th>CLASS NAME</th>
                          <th>BENEFIT</th>
                          <th>PLAN</th>
                        </tr>
                      </thead>
                      <tbody id="klass"></tbody>
                    </table>
                  </div>
                </div>

                <div class="tab-pane" id="link2">
                  <div class="material-datatables">
                    <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                      <thead>
                        <tr>
                          <th><i class="fa fa-hashtag"></i></th>
                          <th>EXCLUTION DESCRIPTION</th>
                        </tr>
                      </thead>
                      <tbody id="exclusion"></tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>

    </div>
  </div>
</div>

<!-- notice modal -->
<div class="modal fade bs-example-modal-lg" id="noticeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="padding-right: 50%">
  <div class="modal-dialog modal-notice modal-lg" style="width: 80%;">
    <div class="modal-content" style="width: 150%;">
      <div class="modal-header">
        <h5 class="modal-title" id="myModalLabel">Inquery Data Policy</h5>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
          <i class="material-icons">close</i>
        </button>
      </div>
      <div class="modal-body">
        <div class="instruction">
          <div class="row">
            <div class="col-md-12">
              <div class="material-datatables">
                <table class="table table-striped table-no-bordered table-hover datatables" cellspacing="0" width="100%" style="width:100%">
                  <thead>
                    <tr>
                      <th><i class="fa fa-hashtag"></i></th>
                      <th>No. Polis</th>
                      <th>Policy Name</th>
                      <th>Start Date</th>
                      <th>End Date</th>
                      <th>Member(s)</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php foreach ($data as $key => $val) { ?>
                    <tr>
                      <td><?php echo ($key + 1); ?></td>
                      <td>
                        <a data-dismiss="modal" href="#" onclick=" display('.benefit-option'); _klass('policy/report_klass', '<?php echo $val['pol_pol_no']; ?>'); _exclusion('policy/report_exclusion', '<?php echo $val['pol_pol_no']; ?>'); binding('#benefit<?php echo $key; ?>');">
                          <?php echo $val['pol_pol_no']; ?>
                          </a>
                      </td>
                      <td><?php echo $val['pol_company_name']; ?></td>
                      <td><?php echo $val['pol_start_date']; ?></td>
                      <td><?php echo $val['pol_end_date']; ?></td>
                      <td class="text-right"><?php echo $val['pol_member']; ?></td>
                      <td>
                        <?php echo $val['pol_status']; ?>
                        <input type="hidden" id="benefit<?php echo $key; ?>" value='<?php echo json_encode($val); ?>'>
                      </td>
                    </tr>
                  <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end notice modal -->

<!-- notice modal -->
<div class="modal fade bs-example-modal-lg" id="report" tabindex="-1" role="dialog" aria-labelledby="Label" aria-hidden="true" style="padding-right: 50%">
  <div class="modal-dialog modal-notice modal-lg" style="width: 80%;">
    <div class="modal-content" style="width: 150%;">
      <div class="modal-header">
        <h5 class="modal-title" id="Label">Inquery Data Policy</h5>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
          <i class="material-icons">close</i>
        </button>
      </div>
      <div class="modal-body">
        <div class="instruction">
          <div class="row">
            <div class="col-md-12">
              <div class="material-datatables">
                <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                  <thead>
                    <tr>
                      <th><i class="fa fa-hashtag"></i></th>
                      <th>BENEFIT CODE</th>
                      <th>BENEFIT DESCRIPTION</th>
                      <th>CATEGORY</th>
                      <th>AMOUNT LIMIT</th>
                    </tr>
                  </thead>
                  <tbody id="reporting"></tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end notice modal -->