<div class="content">
  <div class="content">
    <div class="container-fluid">

      <?php if (isset($_SESSION['users_input']) && $_SESSION['users_input'] == true) { ?>
      <div class="alert alert-<?php echo $_SESSION['users_status']; ?>">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <i class="material-icons">close</i>
        </button>
        <span><b class="text-capitalize"> <?php echo $_SESSION['users_status']; ?> - </b> <?php echo $_SESSION['users_text']; ?></span>
      </div>
      <?php } unset($_SESSION['users_input']); unset($_SESSION['users_status']); unset($_SESSION['users_text']); ?>

      <div class="row">
        <div class="col-md-12">
          <div class="card ">

            <div class="card-header card-header-tabs card-header-primary">
              <div class="nav-tabs-navigation">
                <div class="nav-tabs-wrapper">
                  <ul class="nav nav-tabs" data-tabs="tabs">
                    <li class="nav-item">
                      <a class="nav-link active" href="#link1" data-toggle="tab">
                        <i class="material-icons">account_balance_wallet</i> Topup
                        <div class="ripple-container"></div>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#link2" data-toggle="tab">
                        <i class="material-icons">history</i> History
                        <div class="ripple-container"></div>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>

            <div class="card-body ">
              <div class="tab-content tab-space">

                <div class="form-group">Periode Polis (<span id="periode"></span>)</div>
                <div class="form-group">
                  <div class="input-group no-border">
                    <input type="text" id="no" class="form-control" placeholder="Nomor Polis" readonly="readonly">
                    <button onclick="large('noticeModal');" type="button" data-toggle="modal" class="btn btn-white btn-round btn-just-icon" data-target="#noticeModal">
                      <i class="material-icons">search</i>
                      <div class="ripple-container"></div>
                    </button>
                  </div>
                </div>

                <div class="tab-pane active" id="link1">
                  <form method="post" class="form-horizontal">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="row benefit-option" style="display: none;">
                          <div class="col-md-6">
                            <div class="input-group form-control-md">
                              <div class="form-group bmd-form-group">
                                <label for="balance" class="bmd-label-floating">Balance DA</label>
                                <input type="text" class="form-control" id="balance" readonly="readonly" style="background: transparent;">
                              </div>
                            </div>
                          </div>

                          <div class="col-md-6">
                            <div class="input-group form-control-md">
                              <div class="form-group bmd-form-group">
                                <label for="lastTopup" class="bmd-label-floating">LAST DATE TOPUP</label>
                                <input type="text" class="form-control" id="lastTopup" readonly="readonly" style="background: transparent;">
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="row benefit-option" style="display: none;">
                          <div class="col-md-6">
                            <div class="input-group form-control-md">
                              <div class="form-group bmd-form-group">
                                <label for="received" class="bmd-label-floating">DATE RECEIVED</label>
                                <input type="text" class="form-control datepicker" id="received" name="pda[pda_date_transfer]" required="required" style="background: transparent;">
                              </div>
                            </div>

                            <div class="input-group form-control-md">
                              <div class="form-group bmd-form-group">
                                <label for="topup" class="bmd-label-floating">TOPUP AMOUNT</label>
                                <input type="text" class="form-control num" id="topup" name="amount" required="required" style="background: transparent;">
                              </div>
                            </div>
                          </div>

                          <div class="col-md-6">
                            <div class="form-group">
                              <div class="input-group no-border">
                                <select class="selectpicker" name="pda[pda_bank_code]" data-style="select-with-transition" title="Choose Bank" data-size="7" required="required">
                                <?php foreach ($bank as $key => $val) { ?>
                                  <option value="<?php echo $val['vals']; ?>"><?php echo $val['label']; ?></option>
                                <?php } ?>
                                </select>
                              </div>
                            </div>

                            <div class="input-group form-control-md">
                              <div class="form-group bmd-form-group">
                                <label for="keterangan" class="bmd-label-floating">KETERANGAN</label>
                                <input type="text" class="form-control" id="keterangan" name="pda[pda_keterangan]" required="required" style="background: transparent;">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row text-right">
                      <div class="col-md-9">&nbsp;</div>
                      <div class="col-md-3">
                        <input type="hidden" id="val" class="form-control" name="no">
                        <button type="submit" class="btn btn-fill btn-primary submit" disabled="disabled">Submit</button>
                        <div style="display: none;" id="datas"></div>
                      </div>
                    </div>
                  </form>
                </div>

                <div class="tab-pane" id="link2">
                  <div class="material-datatables">
                    <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                      <thead>
                        <tr>
                          <th><i class="fa fa-hashtag"></i></th>
                          <th>Date Topup</th>
                          <th>Bank Name</th>
                          <th>Proyeksi Amount</th>
                          <th>% Proyeksi</th>
                          <th>Status Proyeksi</th>
                          <th>Top UP Amount</th>
                          <th>Keterangan</th>
                        </tr>
                      </thead>
                      <tbody id="history"></tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>

    </div>
  </div>
</div>

<!-- notice modal -->
<div class="modal fade bs-example-modal-lg" id="noticeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="padding-right: 50%">
  <div class="modal-dialog modal-notice modal-lg" style="width: 80%;">
    <div class="modal-content" style="width: 150%;">
      <div class="modal-header">
        <h5 class="modal-title" id="myModalLabel">Inquery Data Policy</h5>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
          <i class="material-icons">close</i>
        </button>
      </div>
      <div class="modal-body">
        <div class="instruction">
          <div class="row">
            <div class="col-md-12">
              <div class="material-datatables">
                <table class="table table-striped table-no-bordered table-hover datatables" cellspacing="0" width="100%" style="width:100%">
                  <thead>
                    <tr>
                      <th><i class="fa fa-hashtag"></i></th>
                      <th>No. Polis</th>
                      <th>Policy Name</th>
                      <th>Start Date</th>
                      <th>End Date</th>
                      <th>Member(s)</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php foreach ($data as $key => $val) { ?>
                    <tr>
                      <td><?php echo ($key + 1); ?></td>
                      <td>
                        <a data-dismiss="modal" href="#" onclick=" display('.benefit-option'); _topup('policy/topup_data', '<?php echo $val['pol_pol_no']; ?>', '#class_list'); binding('#benefit<?php echo $key; ?>');">
                          <?php echo $val['pol_pol_no']; ?>
                          </a>
                      </td>
                      <td><?php echo $val['pol_company_name']; ?></td>
                      <td><?php echo $val['pol_start_date']; ?></td>
                      <td><?php echo $val['pol_end_date']; ?></td>
                      <td class="text-right"><?php echo $val['pol_member']; ?></td>
                      <td>
                        <?php echo $val['pol_status']; ?>
                        <input type="hidden" id="benefit<?php echo $key; ?>" value='<?php echo json_encode($val); ?>'>
                      </td>
                    </tr>
                  <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end notice modal -->