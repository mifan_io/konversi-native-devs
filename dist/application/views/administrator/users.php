<div class="content">
  <div class="content">
    <div class="container-fluid">

      <?php if (isset($_SESSION['users_input']) && $_SESSION['users_input'] == true) { ?>
      <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <i class="material-icons">close</i>
        </button>
        <span><b> Success - </b> <?php echo $_SESSION['users_text']; ?></span>
      </div>
      <?php } unset($_SESSION['users_input']); unset($_SESSION['users_text']); ?>

      <div class="row">
        <div class="col-md-12 ml-auto mr-auto">
          <div class="page-categories">

            <ul class="nav nav-pills nav-pills-primary nav-pills-icons justify-content-center" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#link7" role="tablist">
                  <i class="material-icons">people</i> List Users
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#link8" role="tablist">
                  <i class="material-icons">person_add</i> Form Users
                </a>
              </li>
            </ul>

            <div class="tab-content tab-space tab-subcategories">

              <div class="tab-pane active" id="link7">
                <div class="card">

                  <div class="card-header">
                    <h4 class="card-title">Data Users</h4>
                    <p class="card-category">List Users</p>
                  </div>

                  <div class="card-body">
                    <div class="toolbar">
                    <!--        Here you can write extra buttons/actions for the toolbar              -->
                    </div>
                    <div class="material-datatables">
                      <table class="table table-striped table-no-bordered table-hover datatables" cellspacing="0"
                        width="100%" style="width:100%">
                        <thead>
                          <tr>
                            <th>Username</th>
                            <th>Full Name</th>
                            <th>Jabatan</th>
                            <th>Division</th>
                            <th>System</th>
                            <th class="disabled-sorting text-center"><i class="fa fa-cogs"></i></th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($users as $key => $value) { ?>
                          <tr>
                            <td><?php echo $value['mlog_username']; ?></td>
                            <td><?php echo $value['mlog_name']; ?></td>
                            <td><?php echo $value['mlog_jabatan']; ?></td>
                            <td><?php echo $value['mlog_function']; ?></td>
                            <td><?php echo $value['mlog_system']; ?></td>
                            <td class="text-center">
                              <a href="#" class="btn btn-link btn-danger btn-just-icon remove" title="Iddle" onclick="submited('submit-<?php echo $value['mlog_username']; ?>');"><i class="material-icons">close</i></a>
                              <form action="<?php echo base_url(); ?>administrator/iddle" method="post" class="submit-<?php echo $value['mlog_username']; ?>">
                                <input type="hidden" name="username" value="<?php echo $value['mlog_username']; ?>">
                              </form>
                            </td>
                          </tr>
                        <?php } ?>
                        </tbody>
                      </table>
                    </div>
                  </div>

                </div>
              </div>

              <div class="tab-pane" id="link8">
                <div class="card">

                  <div class="card-header">
                    <h4 class="card-title">Form Input</h4>
                    <p class="card-category">Create New Users</p>
                  </div>

                  <div class="card-body">
                    <form method="post" class="form-horizontal">
                      <div class="row">
                        <div class="col-md-6">
                          <div class="row">
                            <label class="col-md-3 col-form-label">SYSTEM TYPE</label>
                            <div class="col-md-9">
                              <div class="form-group has-default">
                                <select class="selectpicker" name="system" id="system" onchange="valid_user();" data-style="select-with-transition" title="Choose System" data-size="7" required="required">
                                <?php foreach ($system as $key => $val){ ?>
                                  <option value="<?php echo $key ; ?>"><?php echo $val ; ?></option>
                                <?php } ?>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <label class="col-md-3 col-form-label">FULL NAME</label>
                            <div class="col-md-9">
                              <div class="form-group has-default">
                                <input type="text" name="name" class="form-control" required="required">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <label class="col-md-3 col-form-label">JABATAN</label>
                            <div class="col-md-9">
                              <div class="form-group has-default">
                                <input type="text" name="jabatan" class="form-control" required="required">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <label class="col-md-3 col-form-label">EMAIL</label>
                            <div class="col-md-9">
                              <div class="form-group has-default">
                                <input type="email" name="email" class="form-control" required="required">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <label class="col-md-3 col-form-label">DIVISION</label>
                            <div class="col-md-9">
                              <div class="form-group has-default">
                                <select class="selectpicker" name="function" data-style="select-with-transition" title="Choose Division" data-size="7" required="required">
                                <?php foreach ($divisi as $key => $val){ ?>
                                  <option value="<?php echo $key ; ?>"><?php echo $val ; ?></option>
                                <?php } ?>
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="row">
                            <label class="col-md-3 col-form-label">USERNAME</label>
                            <div class="col-md-9">
                              <div class="form-group has-username">
                                <input type="text" name="username" id="username" class="form-control" onkeyup="valid_user();" required="required">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <label class="col-md-3 col-form-label">PASSWORD</label>
                            <div class="col-md-9">
                              <div class="form-group has-password">
                                <input type="password" name="password" onkeyup="valid_pass();" class="password form-control" required="required">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <label class="col-md-3 col-form-label">RE-TYPE PASSWORD</label>
                            <div class="col-md-9">
                              <div class="form-group has-password">
                                <input type="password" name="re_password" onkeyup="valid_pass();" class="re_password form-control" required="required">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row pull-right">
                        <div class="col-md-9">
                          <button type="submit" class="btn btn-fill btn-primary submit" disabled="disabled">Submit</button>
                        </div>
                      </div>
                    </form>
                  </div>

                </div>
              </div>

            </div>

          </div>
        </div>
      </div>

    </div>
  </div>
</div>