var _submit = false;
var base_url = $('#base_url').val();
var enroll = {EMP: 'Employee', SPO: 'Spouse', CHL: 'Child'};
var category = {H:'Per-Hari', K:'Per-Kunjungan', T:'Per-Tahun'};

$(document).ready(function () {
  $('.datepicker').datetimepicker({
    format: 'DD-MM-YYYY'
  });

  $('.datestart').datetimepicker({
    format: 'DD-MM-YYYY'
  });

  $('.dateend').datetimepicker({
    format: 'DD-MM-YYYY',
    useCurrent: false
  });

  $('.datestart').on('dp.change', function (e) {
    $('.dateend').data('DateTimePicker').minDate(e.date);
    $('.datestart').change();
  });
  
  $('.dateend').on('dp.change', function (e) {
    $('.datestart').data('DateTimePicker').maxDate(e.date);
    $('.dateend').change();
  });

  $('.num').on('input', function (e) {
    $(this).val($(this).val().replace(/[^0-9]/g, ''));
  });

  $('#logout').attr('href', '#');

  $('#logout').click(function(){
    $('.logout').submit();
  });

  $('.select2').select2();

  if($('.inDate').length){
    $('.inDate').each(function(l){
      var id = formatDate($(this).html());
      $(this).html(id);
    });
  }

  if($('.rupiah').length){
    $('.rupiah').each(function(l){
      var rp = rupiah($(this).html());
      $(this).html(rp);
    });
  }

  //========================================================================
  $('.datatables').DataTable({
    'pagingType': 'full_numbers',
    'lengthMenu': [
      [10, 25, 50, -1],
      [10, 25, 50, 'All']
    ],
    responsive: true,
    language: {
      search: '_INPUT_',
      searchPlaceholder: 'Search records',
    }
  });

  var table = $('.datatable').DataTable();

  // Edit record
  table.on('click', '.edit', function () {
    $tr = $(this).closest('tr');
    var data = table.row($tr).data();
    alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
  });

  // Delete a record
  table.on('click', '.remove', function (e) {
    $tr = $(this).closest('tr');
    table.row($tr).remove().draw();
    e.preventDefault();
  });

  //Like record
  table.on('click', '.like', function () {
    alert('You clicked on Like button');
  });
  //========================================================================

});

function rupiah(num){
  return new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(num);
}

function redirect(url){
  window.location = base_url + url;
}

function display(idc, css = ''){
  $(idc).css('display', css);
}

function large(id){
  setTimeout(() => {
    $('#' + id).css({'padding-right': '28%', 'display': 'block'});
  }, 100);
}

function _karyawan(id){
  var obj = JSON.parse($(id).val());

  $('#mmbr_acc_no').val(obj.mmbr_acc_no);
  $('#mmbr_acc_name').val(obj.mmbr_acc_name);
  $('#mmbr_bank').val(obj.mmbr_bank);
  $('#mmbr_bank_branch').val(obj.mmbr_bank_branch);

  /*$('#mmbr_marital_status').val(obj.mmbr_marital_status);
  $('#mmbr_marital_status').attr('readonly','readonly');*/

  $('#mmbr_phone').val(obj.mmbr_phone);
  $('#mmbr_handphone').val(obj.mmbr_handphone);
  $('#mmbr_email').val(obj.mmbr_email);
  $('#mmbr_departement').val(obj.mmbr_departement);
  $('#mmbr_jabatan').val(obj.mmbr_jabatan);
  $('#mmbr_cabang').val(obj.mmbr_cabang);

  $('#mmbr_acc_no').attr('readonly','readonly');
  $('#mmbr_acc_name').attr('readonly','readonly');
  $('#mmbr_bank').attr('readonly','readonly');
  $('#mmbr_bank_branch').attr('readonly','readonly');

  $('#mmbr_phone').attr('readonly','readonly');
  $('#mmbr_handphone').attr('readonly','readonly');
  $('#mmbr_email').attr('readonly','readonly');
  $('#mmbr_departement').attr('readonly','readonly');
  $('#mmbr_jabatan').attr('readonly','readonly');
  $('#mmbr_cabang').attr('readonly','readonly');

  if ($('#nik').val() != '') {
    if ($('#mmbr_enrol_type').length) {
      $('#mmbr_enrol_type option[value="NM"]').remove();
    }

    if ($('#mmbr_enrol_code').length) {
      $('#mmbr_enrol_code option[value="EMP"]').remove();
    }

    if ($('#mmbr_marital_status').length) {
      $('#mmbr_marital_status option[value="M"]').remove();
    }
  }

  if (obj.mmbr_marital_status == 'M') {
    console.log(obj.mmbr_marital_status);
    if ($('#mmbr_enrol_code').length) {
      $('#mmbr_enrol_code option[value="SPO"]').remove();
    }
  }

  if ($('.selectpicker').length) {
    $('.selectpicker').selectpicker('refresh');
  }
}

function _claimtype(id){
  $('.A-display').css('display', 'none');
  $('.' + $(id).val() + '-display').css('display', '');
}

function _provider(id){
  var obj = JSON.parse($(id).val());
  $('#prov').val(obj.mprov_prov_code + ' - ' + obj.mprov_name);

  $('#_name').val(obj.mprov_name);
  $('#polbt_pol_no').val(obj.mprov_prov_code);

  if (typeof obj.mprov_deadline_payment !== 'undefined'){
    $('#_services').val(obj.mprov_deadline_payment);
  }
}

function binding(id){
  var obj = JSON.parse($(id).val());
  $('#no').val(obj.pol_pol_no +' - '+ obj.pol_company_name);
  $('#periode').text(obj.pol_start_date +' - '+ obj.pol_end_date);
  
  $('#end').val(obj.end);
  $('#start').val(obj.start);
  $('#ending').val(inDate(obj.end));
  $('#_name').val(obj.pol_company_name);
  $('#polbt_pol_no').val(obj.pol_pol_no);

  if (typeof obj.pol_service_level_day !== 'undefined'){
    $('#_services').val(obj.pol_service_level_day);
  }
}

function submited(username){
  $('.' + username).submit();
}

function valid_submit(){
  var system = $('#system').val();
  var username = $('#username').val();
  var password = $('.password').val();
  var re_password = $('.re_password').val();

  if (system.trim() != '' && username.trim() != '' && password.trim() != '' && re_password.trim() != '') {
    if(_submit){
      $('.submit').removeAttr('disabled');
    } else {
      $('.submit').attr('disabled', 'disabled');
    }
  }
}

function valid_pass(){
  var password = $('.password').val();
  var re_password = $('.re_password').val();
  if (password == re_password && password != '' && re_password != '') {
    $('.has-password').attr('class', 'has-password form-group has-success');
    // $('.submit').removeAttr('disabled');
    _submit = true;
  } else {
    $('.has-password').attr('class', 'has-password form-group has-danger');
    // $('.submit').attr('disabled', 'disabled');
    _submit = false;
  }
  valid_submit();
}

function formatDate(date) {
  if (date === '0000-00-00' || date === '0000-00-00 00:00:00' || !date) {
    return '';
  } else {
    var date = new Date(date);
    var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();

    return day + ' ' + monthNames[monthIndex] + ' ' + year;
  }
}

function inDate(date) {
  if (date === '0000-00-00' || date === '0000-00-00 00:00:00' || !date) {
    return '';
  } else {
    var date = new Date(date);

    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();

    return day + '-' + month + '-' + year;
  }
}

function chart(chain, data){
  var dataPreferences = {
    labels: ['62%', '32%', '6%'],
    series: [62, 32, 6]
  };

  var optionsPreferences = {
    height: '230px'
  };

  Chartist.Pie(chain, data, optionsPreferences);
}

function datatables_gen(chain, tr){
  $('.datatables-new').DataTable().destroy();
  $(chain).html(tr);

  $('.datatables-new').DataTable({
    'pagingType': 'full_numbers',
    'lengthMenu': [
      [10, 25, 50, -1],
      [10, 25, 50, 'All']
    ],
    responsive: true,
    language: {
      search: '_INPUT_',
      searchPlaceholder: 'Search records',
    }
  });

  console.log($('.datatables-new'));
}

function _hiddenJson(name, value = '', id = ''){
  return '<input type="hidden" name="' + name + '" id="' + id + '" value=' + "'" + JSON.stringify(value) + "'" + '>'
}

function _hidden(name, value = '', id = ''){
  return '<input type="hidden" name="' + name + '" id="' + id + '" value="' + value + '">'
}

function _input(name, value = '', label, id, required = 'required="required"'){
  var input = '';
  input += '<div class="input-group form-control-md">';
    input += '<div class="form-group bmd-form-group">';
      input += '<label for="exampleInput' + id + '" class="bmd-label-floating">' + label + '</label>'
      input += '<input type="text" class="form-control" value="' + value + '" id="exampleInput' + id + '" name="' + name + '" ' + required + '>'
    input += '</div>';
  input += '</div>';
  return input;
}

function _radio(name, value, checked = true, klas = null, id = null, label = ''){
  return _radicheck('radio', name, value, checked, klas, id, label);
}

function _checkbox(name, value, checked = true, klas = null, id = null, label = ''){
  return _radicheck('checkbox', name, value, checked, klas, id, label);
}

function _radicheck(type, name, value, checked = true, klas = null, id = null, label = ''){
  var radicheck = '';
  var check = (checked) ? 'checked="checked"' : '' ;
  var klass = {radio: 'circle', checkbox: 'form-check-sign'} ;

  radicheck += '<div class="form-check">';
    radicheck += '<label class="form-check-label">';
      radicheck += '<input type="hidden" value="0" name="' + name + '">';
      radicheck += '<input class="form-check-input ' + klas + '" id="' + id + '" type="' + type + '" value="' + value + '" name="' + name + '" ' + check + '>&nbsp' + label;
      radicheck += '<span class="' +klass[type]+ '"><span class="check"></span></span>';
    radicheck += '</label>';
  radicheck += '</div>';
  return radicheck;
}

function _select(name, label, option, id = null, required = 'required="required"'){
  var select = '';
  select += '<select class="selectpicker" name="' + name + '" id="' + id + '" data-style="select-with-transition" title="Choose ' + label + '" data-size="7" ' + required + '>';
    select += option;
  select += '</select>';
  return select;
}

function checkall(parent, child){
  var checker = document.querySelectorAll(child);
  for(var i = 0; i < checker.length; i++) {
    checker[i].checked = parent.checked;
  }
}

function _vals(val, val2 = null, val3 = null, val4 = null){
  var data = {param: val};
  $('#code').val(val);

  if (val2){
    data.param2 = val2;
    $('#val2').val(val2);
  }

  if (val3){
    data.param3 = val3;
    $('#val3').val(val3);
  }

  if (val4){
    data.param4 = val4;
    $('#val4').val(val4);
  }

  return data
}

function _val(val, val2 = null, val3 = null, val4 = null, val5 = null){
  var data = {param: val};
  $('#val').val(val);

  if (val2){
    data.param2 = val2;
    $('#val2').val(val2);
  }

  if (val3){
    data.param3 = val3;
    $('#val3').val(val3);
  }

  if (val4){
    data.param4 = val4;
    $('#val4').val(val4);
  }


  if (val5){
    data.param5 = val5;
    $('#val5').val(val5);
  }

  return data
}

async function valid_user(){
  var sys = $('#system').val();
  var user = $('#username').val();

  if (user.trim() != '' && sys.trim() != '' ) {
    var atad = {username: user, system: sys};
    var data = await $.post(base_url + 'administrator/username', atad);

    if (typeof data === 'undefined') {
      $('.has-username').attr('class', 'has-username form-group has-success');
      // $('.submit').removeAttr('disabled');
      _submit = true;
    } else {
      $('.has-username').attr('class', 'has-username form-group has-danger');
      // $('.submit').attr('disabled', 'disabled');
      _submit = false;
    }
    valid_submit();
  }
}

async function chainedNull(url, val, chain, val2 = null, val3 = null, val4 = null){
  var opt = '<option value="">All Data</option>';
  var atad = _val(val, val2, val3, val4);

  var data = await $.post(base_url + url, atad);
  for(var i in data){
    opt += '<option value="' + data[i].vals + '">' + data[i].label + '</option>';
  }

  $(chain).html(opt);

  $('.select2').select2();
  console.log($('.select2'));

  $('.selectpicker').selectpicker('refresh');
}

async function chained(url, val, chain, val2 = null, val3 = null, val4 = null){
  var opt = '';
  var atad = _val(val, val2, val3, val4);

  var data = await $.post(base_url + url, atad);
  for(var i in data){
    opt += '<option value="' + data[i].vals + '">' + data[i].label + '</option>';
  }

  $(chain).html(opt);

  $('.select2').select2();
  console.log($('.select2'));

  $('.selectpicker').selectpicker('refresh');
}

async function granting(url, val, chain, val2){
  var tr = '';
  var atad = _val(val, val2);

  var menu = await $.post(base_url + url, atad);

  for(var i in menu){
    var td = '';
    var submenu = menu[i].submenu;

    for(var j in submenu){
      if (submenu.length > 0) {
        var checked = (submenu[j].mua_idmenu) ? 'checked="checked"' : '' ;
        td += '<tr><td><label class="col-md-3 col-form-label label-checkbox" style="color:#444444;">' + submenu[j].mss_submenu_desc + '</label></td>';
        td += '<td>' + _checkbox('menu[' + submenu[j].mss_id + ']', submenu[j].mss_id, checked) + '</td></tr>';
      }
    }
    tr += '<tr><td colspan="2"><label class="col-md-3 col-form-label label-checkbox text-primary text-primary text-uppercase">' + menu[i].msm_menu_category + '</label></td></tr>' + td;
  }

  $(chain).html(tr);
  $('.submit').removeAttr('disabled');
}

async function plan(id, url, chain){
  var tr = '';
  var obj = JSON.parse($('#policy' + id).val());
  var atad = {no: obj.pol_pol_no, product: obj.pol_product_code};

  var data = await $.post(base_url + url, atad);

  for(var i in data){
    var td = '';
    var submenu = data[i].submenu;

    for(var j in submenu){
      if (submenu.length > 0) {
        td += '<tr><td><span class="col-md-3 col-form-label label-checkbox" style="color:#444444;">' + (parseInt(j) + 1) + '</span></td>';
        td += '<td><span class="col-md-3 col-form-label label-checkbox" style="color:#444444;">' + submenu[j].mco_mastercode + '</span></td>';
        td += '<td><span class="col-md-3 col-form-label label-checkbox" style="color:#444444;">' + submenu[j].mco_description + '</span></td>';
        td += '<td><span class="col-md-3 col-form-label label-checkbox" style="color:#444444;">' + submenu[j].prdplan_plan_code + '</span></td>';
        td += '<td>' + _checkbox('plan[' + submenu[j].prdplan_plan_code + ']', submenu[j].mco_mastercode, false, 'checker') + '</td></tr>';
      }
    }
    tr += '<tr><td colspan="4"><span class="col-md-3 col-form-label label-checkbox text-primary text-primary text-uppercase">' + data[i].polbt_benefit_type + ' (' + data[i].mco_description + ')</span></td></tr>' + td;
  }

  $(chain).html(tr);

  binding('#policy' + id);

  $('.submit').removeAttr('disabled');
}

async function code(url, val, chain){
  var atad = _val(val);

  var code = await $.post(base_url + url, atad);
  $(chain).val(code.vals);
}

async function _benefit(url, val, chain, val2 = null, val3 = null, val4 = null){
  var tr = '';
  var atad = _vals(val, val2, val3, val4);

  var data = await $.post(base_url + url, atad);

  for(var i in data){
    var j = parseInt(i);
    var opt = '';
    var code = parseInt(data[i].prdplan_ben_code.substring(1, 5));
    var amount = parseInt(data[i].prdplan_benefit_amount);
    var lbl = (code === 9999) ? 'class="table-danger"' : '';
    var cek = (amount === 999999) ? true : false ;

    for(var l in category){
      var selected = (l == data[i].prdplan_category) ? 'selected="selected"' : '' ;
      opt += '<option value="' + l + '" ' + selected + '>' + category[l] + '</option>';
    }

    tr += '<tr ' + lbl + '><td>' + (j + 1) + '</td>';
    tr += '<td>' + data[i].prdplan_ben_code + '</td>';
    tr += '<td>' + data[i].mbi_ben_description + '</td>';
    tr += '<td>' + _select('benefit[' + j + '][prdplan_category]', 'Category', opt) + '</td>';
    tr += '<td>' + _input('benefit[' + j + '][prdplan_covered]', 100, 'Covered', 'cover' + i) + '</td>';
    tr += '<td>' + _input('benefit[' + j + '][prdplan_case_disability]', data[i].prdplan_case_disability, 'Max Visit','visit' + i) + '</td>';
    tr += '<td>' + _input('benefit[' + j + '][prdplan_frequently]', data[i].prdplan_frequently,'Max Day','day' + i) + '</td>';
    tr += '<td>' + _input('benefit[' + j + '][prdplan_benefit_amount]', data[i].prdplan_benefit_amount,'Ben Amount','amount' + i) + '</td>';
    tr += '<td>' + _checkbox('cek[' + j + ']', 1, cek, 'checker') + '</td></tr>';
  }

  $(chain).html(tr);

  $('.selectpicker').selectpicker('refresh');
  console.log($('.selectpicker'));

  if (data) {
    $('.submit').removeAttr('disabled');
  } else {
    $('.submit').attr('disabled','disabled');
  }
  document.querySelector('#check').checked = false;
}

async function _class(url, val, chain, val2 = null, val3 = null, val4 = null){
  var tr = '';
  var atad = _vals(val, val2, val3, val4);

  var data = await $.post(base_url + url, atad);

  for(var i in data){
    var j = parseInt(i);

    tr += '<tr><td>' + (j + 1) + '</td>';
    tr += '<td>' + data[i].prdclass_plan_name + '</td>';
    tr += '<td>' + data[i].prdclass_class_code + '</td>';
    tr += '<td>' + data[i].prdclass_provider_category + '</td>';
    tr += '<td>' + data[i].prdclass_benefit_type + '</td>';
    tr += '<td>' + data[i].mco_description + ' (' + data[i].prdclass_benefit_plan + ')</td></tr>';
    // tr += '<td>' + _checkbox('cek[' + j + ']', 1, false, 'checker') + '</td></tr>';
  }

  $(chain).html(tr);

  $('.selectpicker').selectpicker('refresh');
  console.log($('.selectpicker'));

  if (data) {
    $('.submit').removeAttr('disabled');
  } else {
    $('.submit').attr('disabled','disabled');
  }
}

async function _family(url, val, chain){
  var kec = 0;
  var pla = '';
  var ben = '';
  var atad = _val(val);

  var data = await $.post(base_url + url, atad);
  var plan = data.plan;
  var benefit = data.benefit;

  if (plan) {
    for(var i in plan){
      var l = parseInt(i);
      pla += '<tr><td>' + (l + 1) + '</td>';
      pla += '<td>' + plan[i].prdclass_plan_name;
      pla += _hidden('plan[' + l + '][benfac_benefit_class]', plan[i].prdclass_class_code) + '</td>';
      pla += '<td>' + _input('plan[' + l + '][benfac_benefit_amount]', '', 'Amount Benefit', 'amount' + l, '') + '<td></tr>';
    }
  } else {
    pla = '<tr><td colspan="3">Data Plan Tidak Ada</td></tr>';
  }

  if (benefit) {
    for(var i in benefit){
      var l = parseInt(i);
      var cek = (benefit[i].cek) ? true : false;
      
      if (benefit[i].cek) {
        kec++;
      }

      ben += '<tr><td>' + (l + 1) + '</td>';
      ben += '<td>' + benefit[i].prdplan_ben_type + ' (' + benefit[i].mco_description + ')</td>';
      ben += '<td valign="middle">' + _checkbox('benefit[' + l + '][benfam_benefit_type]', benefit[i].prdplan_ben_type, cek, 'checker') + '<td></tr>';
    }
  } else {
    ben = '<tr><td colspan="3">Data Benefit Tidak Ada</td></tr>';
  }

  $('#data_plan').html(pla);
  $('#data_benefit').html(ben);

  if (benefit && plan) {
    $('.submit').removeAttr('disabled');
  } else {
    $('.submit').attr('disabled','disabled');
  }

  document.querySelector('#check').checked = (benefit && kec === benefit.length) ? true : false;
}

async function _topup(url, val, chain){
  var tr = '';
  var tag = '';
  var atad = _val(val);
  var data = await $.post(base_url + url, atad);

  var topup = data.topup;
  var history = data.history;

  var dap = parseFloat(topup.pol_dedicated_account_proyeksi);
  var dan = parseFloat(topup.pol_dedicated_account_normal)
  var da = parseFloat(topup.pol_dedicated_account);
  var persen = (da > 0) ? (dap / da) * 100 : 0 ;

  $('#balance').val(dap + ' (' + persen.toFixed(2) + '%) ' + topup.pol_dedicated_account_status_proyeksi);
  $('#lastTopup').val(formatDate(topup.history));
  $('#topup').val(dan - dap);

  tag += _hidden('pda[pda_amount_proyeksi]', dap);
  tag += _hidden('pda[pda_persen_proyeksi]', persen.toFixed(2));
  tag += _hidden('pda[pda_status_proyeksi]', topup.pol_dedicated_account_status_proyeksi);

  tag += _hidden('pol[pol_dedicated_account]', da);
  tag += _hidden('pol[pol_dedicated_account_normal]', dan);
  tag += _hidden('pol[pol_dedicated_account_proyeksi]', dap);
  tag += _hidden('pol[pol_dedicated_account_balance]', topup.pol_dedicated_account_balance);


  for(var i in history){
    var l = parseInt(i);
    tr += '<tr><td>' + (l + 1) + '</td>';
    tr += '<td>' + history[i].pda_date_transfer + '</td>';
    tr += '<td>' + history[i].msbank_bank_name + '</td>';
    tr += '<td class="text-right">' + history[i].pda_amount_proyeksi + '</td>';
    tr += '<td class="text-right">' + history[i].pda_persen_proyeksi + '</td>';
    tr += '<td>' + history[i].pda_status_proyeksi + '</td>';
    tr += '<td class="text-right">' + history[i].pda_amount + '</td>';
    tr += '<td>' + history[i].pda_keterangan + '</td></tr>';
  }

  $('#datas').html(tag);
  $('#history').html(tr);

  if (topup) {
    $('.submit').removeAttr('disabled');
  } else {
    $('.submit').attr('disabled','disabled');
  }

  document.querySelector('#received').autofocus = true;
}

async function _suspend(url, val){
  var atad = _val(val);
  var data = await $.post(base_url + url, atad);

  var ac = data.ac;
  var su = data.su;

  $('#ac').val(ac.kount);
  $('#su').val(su.kount);

  if (data) {
    $('.submit').removeAttr('disabled');
  } else {
    $('.submit').attr('disabled','disabled');
  }
}

async function _klass(url, val){
  var tr1 = '';
  var atad = _val(val);
  var klass = await $.post(base_url + url, atad);

  if(klass){
    for(var i in klass){
      var l = parseInt(i);
      tr1 += '<tr><td>' + (l + 1) + '</td>';
      tr1 += '<td> Class' + klass[i].prdclass_class_code + '</td>';
      tr1 += '<td>' + klass[i].mco_description + '</td>';
      tr1 += '<td><a href="#" class="text-info" onclick="_report(' + "'policy/reporting', '" + val + "_product_benefit_plan" + "', '" + klass[i].prdplan_ben_type + "', '" + klass[i].prdplan_plan_code + "'" + ')">' + klass[i].prdplan_plan_code + '</a></td></tr>';
    }
  }

  $('#klass').html(tr1);
}

async function _exclusion(url, val){
  var tr2 = '';
  var atad = _val(val);
  var exclusion = await $.post(base_url + url, atad);

  if(exclusion){
    for(var i in exclusion){
      var l = parseInt(i);
      tr2 += '<tr><td>' + (l + 1) + '</td>';
      tr2 += '<td>' + exclusion[i].pe_exclution_desc + '</td></tr>';
    }
  }

  $('#exclusion').html(tr2);
}

async function _report(url, val, val2, val3){
  var tr = '';
  var atad = _val(val, val2, val3);
  var data = await $.post(base_url + url, atad);

  for(var i in data){
    var l = parseInt(i);
    var charge = (data[i].prdplan_benefit_amount == 999999) ? 'Ascharge' : rupiah(data[i].prdplan_benefit_amount) ;
    tr += '<tr><td>' + (l + 1) + '</td>';
    tr += '<td>' + data[i].prdplan_ben_code + '</td>';
    tr += '<td>' + data[i].mbi_ben_description + '</td>';
    tr += '<td>' + category[data[i].prdplan_category] + '</td>';
    tr += '<td>' + charge + '</td></tr>';
  }

  $('#reporting').html(tr);
  $('#report').modal('show');
  large('report');
}

async function _reportAll(url, val, val2, val3, val4, val5){
  var tr1 = '';
  var tr2 = '';
  var tr3 = '';
  var tr4 = '';
  var tr5 = '';

  var ftr1 = '';
  var ftr2 = '';
  var ftr3 = '';
  var ftr4 = '';
  var ftr5 = '';

  var atad = _val(val, val2, val3, val4, val5);
  var data = await $.post(base_url + url, atad);

  console.log(data);

  var claims = data.claims;

  var claim = data.claim;
  var group = data.group;
  var visit = data.visit;
  var highest = data.highest;
  var deseases = data.deseases;

/*=============================Claim================================================*/
  var t_proposed = 0;
  var t_approved = 0;
  var t_excess = 0;
  var t_papprove = 0;
  var t_pexcess = 0;
  var t_code = 0;
  var t_tpapprove = 0;
  var t_tpexcess = 0;

  for(var i in claim){
    var l = parseInt(i);
    var proposed = (claim[i].subval) ? parseFloat(claim[i].subval.proposed) : 0;
    var approved = (claim[i].subval) ? parseFloat(claim[i].subval.approved) : 0;
    var excess = (claim[i].subval) ? parseFloat(claim[i].subval.excess) : 0;
    var code = (claim[i].subval) ? parseFloat(claim[i].subval.code) : 0;

    var papprove = (approved / proposed)  * 100;
    var pexcess  = (excess / proposed)  * 100;

    papprove = isNaN(papprove) ? 0 : papprove ;
    pexcess = isNaN(pexcess) ? 0 : pexcess ;

    var tpapprove = (approved !== 0) ? (claims.approved / approved)  * 100 : 0 ;
    var tpexcess  = (code !== 0) ? (claims.code / code)  * 100 : 0 ;


    tpapprove = isNaN(tpapprove) ? 0 : tpapprove ;
    tpexcess = isNaN(tpexcess) ? 0 : tpexcess ;

    tr1 += '<tr><td>' + (l + 1) + '</td>';
    tr1 += '<td>' + claim[i].label + '</td>';
    tr1 += '<td class="text-right">' + rupiah(proposed) + '</td>';
    tr1 += '<td class="text-right">' + rupiah(approved) + '</td>';
    tr1 += '<td class="text-right">' + excess + '</td>';
    tr1 += '<td class="text-right">' + papprove + '</td>';
    tr1 += '<td class="text-right">' + pexcess + '</td>';
    tr1 += '<td class="text-right">' + code + '</td>';
    tr1 += '<td class="text-right">' + tpapprove + '</td>';
    tr1 += '<td class="text-right">' + tpexcess + '</td></tr>';

    t_proposed += proposed;
    t_approved += approved;
    t_excess += excess;
    t_papprove += papprove;
    t_pexcess += pexcess;
    t_code += code;
    t_tpapprove += tpapprove;
    t_tpexcess += tpexcess;
  }

  ftr1 += '<tr><th colspan="2" class="text-primary">Total</th>';
  ftr1 += '<th class="text-right">' + rupiah(t_proposed) + '</th>';
  ftr1 += '<th class="text-right">' + rupiah(t_approved) + '</th>';
  ftr1 += '<th class="text-right">' + t_excess + '</th>';
  ftr1 += '<th class="text-right">' + t_papprove + '</th>';
  ftr1 += '<th class="text-right">' + t_pexcess + '</th>';
  ftr1 += '<th class="text-right">' + t_code + '</th>';
  ftr1 += '<th class="text-right">' + t_tpapprove + '</th>';
  ftr1 += '<th class="text-right">' + t_tpexcess + '</th></tr>';

/*============================End Claim================================================*/

/*=============================Group================================================*/
  var t_papproved = 0;
  var t_pcode = 0;
  t_proposed = 0;
  t_approved = 0;
  t_code = 0;
  var counter = 0;

  for(var i in group){

    var l = parseInt(i);

    group[i].proposed = parseFloat(group[i].proposed);
    group[i].approved = parseFloat(group[i].approved);
    group[i].code = parseFloat(group[i].code);

    var papproved = (group[i].approved / group[i].proposed) * 100;
    var pcode = (group[i].code / claims.code) * 100;

    pcode = parseFloat(pcode.toFixed(2));

    tr2 += '<tr><td>' + (l + 1) + '</td>';
    tr2 += '<td>' + group[i].person + '</td>';
    tr2 += '<td class="text-right">' + rupiah(group[i].proposed) + '</td>';
    tr2 += '<td class="text-right">' + rupiah(group[i].approved) + '</td>';
    tr2 += '<td class="text-right">' + group[i].code + '</td>';
    tr2 += '<td class="text-right">' + papproved + '</td>';
    tr2 += '<td class="text-right">' + pcode + '</td></tr>';

    t_proposed += group[i].proposed
    t_approved += group[i].approved
    t_code += group[i].code
    t_papproved += papproved
    t_pcode += pcode

    counter++;
  }

  ftr2 += '<tr><th colspan="2" class="text-primary">Total</th>';
  ftr2 += '<th class="text-right">' + rupiah(t_proposed) + '</th>';
  ftr2 += '<th class="text-right">' + rupiah(t_approved) + '</th>';
  ftr2 += '<th class="text-right">' + t_code + '</th>';
  ftr2 += '<th class="text-right">' + (t_papproved / counter) + '</th>';
  ftr2 += '<th class="text-right">' + t_pcode + '</th></tr>';

/*============================End Group================================================*/

/*============================Highest================================================*/
  var t_propose = 0;
  var t_approve = 0;

  for(var i in highest){
    var l = parseInt(i);
    highest[i].propose = parseFloat(highest[i].propose);
    highest[i].approve = parseFloat(highest[i].approve);

    tr3 += '<tr><td>' + (l + 1) + '</td>';
    tr3 += '<td>' + highest[i].mmbr_employee_id + '</td>';
    tr3 += '<td>' + highest[i].parent + '</td>';
    tr3 += '<td>' + highest[i].mmbr_name + '</td>';
    tr3 += '<td>' + enroll[highest[i].mmbr_enrol_code] + '</td>';
    tr3 += '<td class="text-right">' + rupiah(highest[i].propose) + '</td>';
    tr3 += '<td class="text-right">' + rupiah(highest[i].approve) + '</td></tr>';

    t_propose += highest[i].propose;
    t_approve += highest[i].approve;
  }

  ftr3 += '<tr><th colspan="5" class="text-primary">Total</th>';
  ftr3 += '<th class="text-right">' + rupiah(t_propose) + '</th>';
  ftr3 += '<th class="text-right">' + rupiah(t_approve) + '</th></tr>';
/*============================End Highest================================================*/


/*============================Deseases================================================*/
  t_propose = 0;
  t_approve = 0;

  for(var i in deseases){
    var l = parseInt(i);
    deseases[i].propose = parseFloat(deseases[i].propose);
    deseases[i].approve = parseFloat(deseases[i].approve);

    tr4 += '<tr><td>' + (l + 1) + '</td>';
    tr4 += '<td>' + deseases[i].clm_icd1 + '</td>';
    tr4 += '<td>' + deseases[i].micd_description + '</td>';
    tr4 += '<td>' + deseases[i].code + '</td>';
    tr4 += '<td class="text-right">' + rupiah(deseases[i].propose) + '</td>';
    tr4 += '<td class="text-right">' + rupiah(deseases[i].approve) + '</td></tr>';

    t_propose += deseases[i].propose;
    t_approve += deseases[i].approve;
  }

  ftr4 += '<tr><th colspan="4" class="text-primary">Total</th>';
  ftr4 += '<th class="text-right">' + rupiah(t_propose) + '</th>';
  ftr4 += '<th class="text-right">' + rupiah(t_approve) + '</th></tr>';
/*============================End Deseases================================================*/

/*============================Visit================================================*/
  t_propose = 0;
  t_approve = 0;

  for(var i in visit){
    var l = parseInt(i);
    visit[i].propose = parseFloat(visit[i].propose);
    visit[i].approve = parseFloat(visit[i].approve);

    tr5 += '<tr><td>' + (l + 1) + '</td>';
    tr5 += '<td>' + visit[i].clm_provider_code + '</td>';
    tr5 += '<td>' + visit[i].clm_provider_name + '</td>';
    tr5 += '<td>' + visit[i].code + '</td>';
    tr5 += '<td class="text-right">' + rupiah(visit[i].propose) + '</td>';
    tr5 += '<td class="text-right">' + rupiah(visit[i].approve) + '</td></tr>';

    t_propose += visit[i].propose;
    t_approve += visit[i].approve;
  }

  ftr5 += '<tr><th colspan="4" class="text-primary">Total</th>';
  ftr5 += '<th class="text-right">' + rupiah(t_propose) + '</th>';
  ftr5 += '<th class="text-right">' + rupiah(t_approve) + '</th></tr>';
/*============================End Visit================================================*/

  $('#claim').html(tr1);
  $('#fclaim').html(ftr1);

  $('#group').html(tr2);
  $('#fgroup').html(ftr2);

  $('#highest').html(tr3);
  $('#fhighest').html(ftr3);

  $('#deseases').html(tr4);
  $('#fdeseases').html(ftr4);

  $('#visit').html(tr5);
  $('#fvisit').html(ftr5);
}

async function remunerasi(url, val, val2, val3, val4, val5){
  var tr = '';
  var ftr = '';

  var end_count = 0;
  var end_sum = 0;
  var end_sumpaid = 0;
  var end_fee = 0;

  var end_pol_fee_tpa = 0;
  var end_code = 0;
  var end_feetot = 0;
  var end_feepaid = 0;
  var end_unpaid = 0;

  var atad = _val(val, val2, val3, val4, val5);
  var data = await $.post(base_url + url, atad);
  console.log(data);

  for(var i in data){
    var l = parseInt(i);

    if(val === 'remunerasi'){

      data[i].pol_fee_tpa = parseFloat(data[i].pol_fee_tpa);
      data[i].pol_fee_card = parseFloat(data[i].pol_fee_card);
      data[i].counter = parseFloat(data[i].counter);
      data[i].mcd_commision = parseFloat(data[i].mcd_commision);
      data[i].count = parseFloat(data[i].count);
      data[i].sum = parseFloat(data[i].sum);

      var sumpaid = (data[i].pol_fee_tpa + data[i].pol_fee_card) * data[i].counter;
      var fee = (data[i].mcd_commision / 100) * sumpaid;

      sumpaid = (isNaN(sumpaid)) ? 0 : sumpaid ;
      fee = (isNaN(fee)) ? 0 : fee ;

      tr += '<tr><td>' + (l + 1) + '</td>';
      tr += '<td>' + data[i].mag_agent_name + '</td>';
      tr += '<td>' + data[i].mcd_channel_name + '</td>';
      tr += '<td class="text-right">' + rupiah(data[i].count) + '</td>';
      tr += '<td class="text-right">' + rupiah(data[i].sum) + '</td>';
      tr += '<td class="text-right">' + rupiah(sumpaid) + '</td>';
      tr += '<td class="text-right">' + rupiah(fee) + '</td>';
      tr += '<td class="text-right">' + rupiah(0) + '</td>';
      tr += '<td class="text-right">' + rupiah(fee) + '</td></tr>';

      end_count += data[i].count;
      end_sum += data[i].sum;
      end_sumpaid += sumpaid;
      end_fee += fee;

    } else {

      data[i].code = parseFloat(data[i].code);
      data[i].null = parseFloat(data[i].null);
      data[i].pol_fee_tpa = parseFloat(data[i].pol_fee_tpa);

      var feetot = data[i].pol_fee_tpa * data[i].code;
      var feepaid = data[i].pol_fee_tpa * data[i].null;
      var unpaid = feetot - feepaid;

      feetot = (isNaN(feetot)) ? 0 : feetot;
      feepaid = (isNaN(feepaid)) ? 0 : feepaid;
      unpaid = (isNaN(unpaid)) ? 0 : unpaid;

      tr += '<tr><td>' + (l + 1) + '</td>';
      tr += '<td>' + data[i].pol_pol_no + '</td>';
      tr += '<td>' + data[i].pol_company_name + '</td>';
      tr += '<td>' + formatDate(data[i].pol_start_date) + '</td>';
      tr += '<td>' + formatDate(data[i].pol_end_date) + '</td>';
      tr += '<td>' + data[i].mcd_channel_name + '</td>';
      tr += '<td>' + data[i].mag_agent_name + '</td>';
      tr += '<td class="text-right">' + rupiah(data[i].pol_fee_tpa) + '</td>';
      tr += '<td class="text-right">' + rupiah(data[i].code) + '</td>';
      tr += '<td class="text-right">' + rupiah(feetot) + '</td>';
      tr += '<td class="text-right">' + rupiah(feepaid) + '</td>';
      tr += '<td class="text-right">' + rupiah(unpaid) + '</td></tr>';

      end_pol_fee_tpa += data[i].pol_fee_tpa;
      end_code += data[i].code;
      end_feetot += feetot;
      end_feepaid += feepaid;
      end_unpaid += unpaid;

    }
  }

  if(val === 'remunerasi'){

    ftr += '<tr><th class="text-primary" colspan="3">Total</th>';
    ftr += '<th class="text-right">' + rupiah(end_count) + '</th>';
    ftr += '<th class="text-right">' + rupiah(end_sum) + '</th>';
    ftr += '<th class="text-right">' + rupiah(end_sumpaid) + '</th>';
    ftr += '<th class="text-right">' + rupiah(end_fee) + '</th>';
    ftr += '<th class="text-right">' + rupiah(0) + '</th>';
    ftr += '<th class="text-right">' + rupiah(end_fee) + '</th></tr>';

  } else {

    ftr += '<tr><th class="text-primary" colspan="7">Total</th>';
    ftr += '<th class="text-right">' + rupiah(end_pol_fee_tpa) + '</th>';
    ftr += '<th class="text-right">' + rupiah(end_code) + '</th>';
    ftr += '<th class="text-right">' + rupiah(end_feetot) + '</th>';
    ftr += '<th class="text-right">' + rupiah(end_feepaid) + '</th>';
    ftr += '<th class="text-right">' + rupiah(end_unpaid) + '</th></tr>';

  }

  $('#reports').html(tr);
  $('#freports').html(ftr);
}

async function graph(url, val){
  var atad = _val(val);
  var data = await $.post(base_url + url, atad);

  var persen1 = (data.pol_dedicated_account_proyeksi > 0) ? (data.pol_dedicated_account_proyeksi / data.pol_dedicated_account_normal) * 100 : 0 ;
  var persen2 = (data.pol_dedicated_account_balance > 0) ? (data.pol_dedicated_account_balance / data.pol_dedicated_account) * 100 : 0 ;

  var pbalance = parseFloat(data.pol_dedicated_account_proyeksi);
  var pusage = parseFloat(data.pol_dedicated_account - data.pol_dedicated_account_balance);
  var payment = parseFloat(data.pol_dedicated_account_balance - data.pol_dedicated_account_proyeksi);

  payment = (payment > 0) ? payment : 0;

  var psum = parseFloat(pbalance) + parseFloat(pusage) + parseFloat(payment);

  psum = parseFloat(psum);

  var per_pbalance = (pbalance > 0) ? (pbalance / psum) * 100 : 0 ;
  var per_pusage = (pusage > 0) ? (pusage / psum) * 100 : 0 ;
  var per_payment = (payment > 0) ? (payment / psum) * 100 : 0 ;


  var rbalance = parseFloat(data.pol_dedicated_account_balance);
  var rusage = parseFloat(data.pol_dedicated_account - data.pol_dedicated_account_balance);
  var rsum = parseFloat(rbalance) + parseFloat(rusage);

  rsum = parseFloat(rsum);

  var per_rbalance = (rbalance > 0) ? (rbalance / rsum) * 100: 0;
  var per_rusage = (rusage > 0) ? (rusage / rsum) * 100: 0;


  $('#Proyeksi-lbl').html('<span class="text-primary">' + rupiah(data.pol_dedicated_account_proyeksi) + '</span> | <span class="text-rose">' + persen1.toFixed(2) + '%</span>');
  $('#Proyeksi').attr('style', 'width:' + persen1.toFixed(2) + '%');

  $('#Realisasi-lbl').html('<span class="text-rose">' + rupiah(data.pol_dedicated_account_proyeksi) + '</span> | <span class="text-primary">' + persen2.toFixed(2) + '%</span>');
  $('#Realisasi').attr('style', 'width:' + persen2.toFixed(2) + '%; background-color: #e91e63;');

  var pdata = {
    labels: [per_pbalance.toFixed(2), per_pusage.toFixed(2), per_payment.toFixed(2)],
    series: [per_pbalance.toFixed(2), per_pusage.toFixed(2), per_payment.toFixed(2)]
  };

  var rdata = {
    labels: [per_rbalance.toFixed(2), per_rusage.toFixed(2)],
    series: [per_rbalance.toFixed(2), per_rusage.toFixed(2)]
  };

  chart('#Proyeksi-pie', pdata);
  chart('#Realisasi-pie', rdata);
}

async function famember(url, val, chain, val2){
  var tr = '';
  var atad = _val(val, val2);
  var data = await $.post(base_url + url, atad);

  for(var i in data){
    var l = parseInt(i);
    tr += '<tr><td>' + (l + 1) + '</td>';
    tr += '<td>' + data[i].mmbr_member_no + '</td>';
    tr += '<td>' + data[i].mmbr_name + '</td>';
    tr += '<td>' + data[i].mmbr_card_nomor + '</td>';
    tr += '<td>' + enroll[data[i].mmbr_enrol_code] + '</td>';
    tr += '<td>' + formatDate(data[i].mmbr_dob) + '</td>';
    tr += '<td>' + _checkbox('member[' + i + '][mbs_member_no]', data[i].mmbr_member_no, false, 'checker') + '</td></tr>';
  }

  $(chain).html(tr);

  if (data) {
    $('.submit').removeAttr('disabled');
  } else {
    $('.submit').attr('disabled','disabled');
  }
  document.querySelector('#check').checked = false;
}

async function karyawan(url, val, chain){
  var tr = '';
  var atad = _val(val);
  var data = await $.post(base_url + url, atad);

  for(var i in data){
    var l = parseInt(i);
    tr += '<tr><td>' + (l + 1) + _hiddenJson('karyawan' + l, data[i], 'karyawan' + l) + '</td>';
    tr += '<td><a onclick="famember(' + "'member/family'" + ', val.value, ' + "'#family_list'" + ', ' + "'" + data[i].mmbr_member_no + "'" + ');nik.value=' + "'" + data[i].mmbr_name + ' [' + formatDate(data[i].mmbr_dob) + '] (' + data[i].mmbr_member_no + ' | ' + data[i].mmbr_card_nomor + ')' + "';" + '_card.value=' + "'" + data[i].mmbr_card_nomor + "';" + '_member.value=' + "'" + data[i].mmbr_member_no + "';" + ' _karyawan(' + "'#" + 'karyawan' + l + "'" + ');" data-dismiss="modal" href="#">' + data[i].mmbr_member_no + '</a></td>';
    tr += '<td>' + data[i].mmbr_name + '</td>';
    tr += '<td>' + data[i].mmbr_card_nomor + '</td>';
    tr += '<td>' + enroll[data[i].mmbr_enrol_code] + '</td>';
    tr += '<td>' + formatDate(data[i].mmbr_dob) + '</td></tr>';
  }

  datatables_gen('#list-karyawan', tr);
}

async function card(url, val){
  var tr = '';
  var atad = _val(val);
  var label = ['rose', 'primary'];
  var status = ['Not Active', 'Active'];
  var data = await $.post(base_url + url, atad);

  for(var i in data){
    var l = parseInt(i);
    tr += '<tr><td>' + (l + 1) + '</td>';
    tr += '<td><a href="' + base_url + 'member/detail/' + val + '/' + data[i].mmbr_member_no + '/' + data[i].mmbr_card_nomor + '">' + data[i].mmbr_card_nomor + '</a></td>';
    tr += '<td><small class="label">' + data[i].mmbr_employee_id + '</small></td>';
    tr += '<td><small class="label">' + data[i].member_name + '</small></td>';
    tr += '<td><small class="label">' + data[i].employee + '</small></td>';
    tr += '<td><small class="label">' + data[i].dob + '</small></td>';
    tr += '<td><small class="label">' + data[i].mmbr_sex + '</small></td>';
    tr += '<td><small class="label">' + data[i].mmbr_age + '</small></td>';
    tr += '<td><small class="label">' + data[i].mmbr_class_code + '</small></td>';
    tr += '<td><small class="label">' + data[i].plan + '</small></td>';
    tr += '<td><small class="label">' + data[i].start_date + '</small></td>';
    tr += '<td><small class="label">' + data[i].end_date + '</small></td>';
    tr += '<td><small class="label text-' + label[data[i].mmbr_status] + '">' + status[data[i].mmbr_status] + '</small></td></tr>';
  }

  datatables_gen('.member-card', tr);
}

async function external(url, val, val2){
  var proposed = 0;
  var approve = 0;
  var excess = 0;
  var ftr = '';
  var tr = '';
  var atad = _val(val, val2);
  var data = await $.post(base_url + url, atad);

  $('#ci_reff_no').html(data.ci_reff_no);
  $('#external-ci_reff_no').html(data.ci_reff_no);
  $('#external-ci_date').html(formatDate(data.ci_date));
  $('#external-ci_provider_name').html(data.ci_provider_name);
  $('#external-ci_policy_name').html(data.ci_policy_name);
  $('#external-ci_member_name').html(data.ci_member_name);
  $('#external-ci_card_no').html(data.ci_card_no);
  $('#external-ci_benefit_type').html(data.ci_benefit_type);
  $('#external-ci_proposed_amount').html(rupiah(data.ci_proposed_amount));
  $('#external-ci_approved_amount').html(rupiah(data.ci_approved_amount));
  $('#external-ci_excess_member').html(rupiah(data.ci_excess_member));
  $('#external-ci_excess_ssa').html(rupiah(data.ci_excess_ssa));

  if (data.detail) {
    var detail = data.detail;
    for (var i in detail) {
      var l = parseInt(i);
      tr += '<tr><td>' + (l + 1) + '</td>';
      tr += '<td>' + detail[i].cid_benefit_code + '</td>';
      tr += '<td>' + detail[i].mbi_ben_description + '</td>';
      tr += '<td class="text-right">' + rupiah(detail[i].cid_proposed_amount) + '</td>';
      tr += '<td class="text-right">' + rupiah(detail[i].cid_approve_amount) + '</td>';
      tr += '<td class="text-right">' + rupiah(detail[i].cid_excess_amount) + '</td></tr>';

      proposed += parseInt(detail[i].cid_proposed_amount);
      approve += parseInt(detail[i].cid_approve_amount);
      excess += parseInt(detail[i].cid_excess_amount);
    }
  }

  ftr += '<tr><td colspan="3" class="text-right">Total</td>';
  ftr += '<td class="text-right">' + rupiah(proposed) + '</td>';
  ftr += '<td class="text-right">' + rupiah(approve) + '</td>';
  ftr += '<td class="text-right">' + rupiah(excess) + '</td></tr>';

  $('#list-DetailEx').html(tr);
  $('#foot-DetailEx').html(ftr);

  $('#DetailEx').modal('show');
  large('DetailEx');
}

async function preview(url, val, chain){
  var tr = '';
  var atad = _val(val);
  var data = await $.post(base_url + url, atad);
  var status = {1: 'REGISTER', A: 'APPROVE'};

  for(var i in data){
    var l = parseInt(i)
    var sts = (typeof status[data[i].clm_status] === 'undefined') ? data[i].clm_status : status[data[i].clm_status];

    tr += '<tr><td><small>' + (l + 1) + '</small></td>';
    tr += '<td><small>' + data[i].clm_claim_code + '</small></td>';
    tr += '<td><small>' + data[i].clm_register_no + '</small></td>';
    tr += '<td><small>' + data[i].clm_member_no + '</small></td>';
    tr += '<td><small>' + data[i].clm_member_name + '</small></td>';
    tr += '<td><small>' + data[i].clm_provider_name + '</small></td>';
    tr += '<td><small>' + data[i].clm_input_source + '</small></td>';
    tr += '<td><small>' + data[i].clm_claim_type + '</small></td>';
    tr += '<td><small>' + data[i].clm_benefit_type + '</small></td>';
    tr += '<td><small>' + data[i].clm_icd1 + '</small></td>';
    tr += '<td><small>' + formatDate(data[i].clm_service_in) + '</small></td>';
    tr += '<td><small>' + formatDate(data[i].clm_service_out) + '</small></td>';
    tr += '<td><small>' + rupiah(data[i].clm_proposed_amount) + '</small></td>';
    tr += '<td><small>' + rupiah(data[i].clm_approved_amount) + '</small></td>';
    tr += '<td><small>' + rupiah(data[i].clm_excess_amount) + '</small></td>';
    tr += '<td><small>' + sts + '</small></td></tr>';
  }

  datatables_gen(chain, tr);
  $('#Claime').html(tr);
}