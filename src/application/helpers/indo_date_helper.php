<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//--Convert into Indonesian Format Date
function get_indonesian_date($sys_date){
  $d_sys = substr($sys_date, 8, 2);
  $y_sys = substr($sys_date, 0, 4);

  //--Month
  $m_sys = substr($sys_date, 5, 2);

  if($m_sys == "01")
    $month_sys = "Januari";
  else if($m_sys == "02")
    $month_sys = "Februari";
  else if($m_sys == "03")
    $month_sys = "Maret";
  else if($m_sys == "04")
    $month_sys = "April";
  else if($m_sys == "05")
    $month_sys = "Mei";
  else if($m_sys == "06")
    $month_sys = "Juni";
  else if($m_sys == "07")
    $month_sys = "July";
  else if($m_sys == "08")
    $month_sys = "Agustus";
  else if($m_sys == "09")
    $month_sys = "September";
  else if($m_sys == "10")
    $month_sys = "Oktober";
  else if($m_sys == "11")
    $month_sys = "November";
  else if($m_sys == "12")
    $month_sys = "Desember";
  //--End Month

  $system_date = $d_sys." ".$month_sys." ".$y_sys;
  return $system_date;
}

//--Convert into Symple Indonesian Format Date
function get_indonesian_simple_date($sys_date){
  if ($sys_date == '0000-00-00' || $sys_date == '0000-00-00 00:00:00') {
    return null;
  } else {
    $d_sys = substr($sys_date, 8, 2);
    $y_sys = substr($sys_date, 0, 4);

    //--Month
    $m_sys = substr($sys_date, 5, 2);

    if($m_sys == "01")
      $month_sys = "Jan";
    else if($m_sys == "02")
      $month_sys = "Feb";
    else if($m_sys == "03")
      $month_sys = "Mar";
    else if($m_sys == "04")
      $month_sys = "Apr";
    else if($m_sys == "05")
      $month_sys = "Mei";
    else if($m_sys == "06")
      $month_sys = "Jun";
    else if($m_sys == "07")
      $month_sys = "Jul";
    else if($m_sys == "08")
      $month_sys = "Aug";
    else if($m_sys == "09")
      $month_sys = "Sept";
    else if($m_sys == "10")
      $month_sys = "Okt";
    else if($m_sys == "11")
      $month_sys = "Nov";
    else if($m_sys == "12")
      $month_sys = "Des";
    //--End Month

    $system_date_simple = $d_sys." ".$month_sys." ".$y_sys;
    return $system_date_simple;
  }
}

//--Convert into Pengkodean
function get_indonesian_romawi_date($sys_date){
  $d_sys = substr($sys_date, 8, 2);
  $y_sys = substr($sys_date, 2, 2);

  //--Month
  $m_sys = substr($sys_date, 5, 2);

  if($m_sys == "01")
    $month_sys = "I";
  else if($m_sys == "02")
    $month_sys = "II";
  else if($m_sys == "03")
    $month_sys = "III";
  else if($m_sys == "04")
    $month_sys = "IV";
  else if($m_sys == "05")
    $month_sys = "V";
  else if($m_sys == "06")
    $month_sys = "VI";
  else if($m_sys == "07")
    $month_sys = "VII";
  else if($m_sys == "08")
    $month_sys = "VIII";
  else if($m_sys == "09")
    $month_sys = "IX";
  else if($m_sys == "10")
    $month_sys = "X";
  else if($m_sys == "11")
    $month_sys = "XI";
  else if($m_sys == "12")
    $month_sys = "XII";
  //--End Month

  $system_date_kodean = $month_sys."/".$y_sys;
  return $system_date_kodean;
}


function get_age($tanggal){ 
  list($hari,$bulan,$tahun) = explode("-",$tanggal); 
  $format_tahun = date("Y") - $tahun; 
  $format_bulan = date("m") - $bulan; 
  $format_hari = date("d") - $hari; 
  
  if ($format_hari < 0 || $format_bulan < 0) $format_tahun--; 
  
  return $format_tahun; 
}