<?php
// https://www.teletopiasms.no/np/frontpage/gateway/api-http-examples-php
// https://stackoverflow.com/questions/5647461/how-do-i-send-a-post-request-with-php
class Claim_model extends CI_Model {

  public function __construct() {
    parent::__construct();
    $this->load->database();
    $this->load->model('Dml_model');
  }

  function table_member($table, $member = null){
    // mod-transaction-claim
    // mod-transaction-claim-owlexa
    $where = (empty($member)) ? '' : 'WHERE mmbr_member_no = "' . $member . '"' ;
    return $this->Dml_model->one($table, $where, 'mmbr_employee_id, mmbr_card_nomor, mmbr_class_code, mmbr_name');
  }

  function detail($ref, $type){
    return $this->read('callcenter_check_in_detail', 'JOIN mst_benefit_item ON cid_benefit_code = mbi_ben_code WHERE cid_reff_code = "' . $ref . '" AND mbi_ben_type = "' . $type . '"', 'cid_benefit_code, mbi_ben_description, cid_proposed_amount, cid_approve_amount, cid_excess_amount');
  }

  function external($ref, $no){
    return $this->one('callcenter_check_in', 'WHERE ci_reff_no = "' . $ref . '" AND ci_policy_no = "' . $no . '"', 'ci_reff_no, ci_date, ci_service_out, ci_provider_code, ci_provider_name, ci_policy_no, ci_policy_name, ci_member_no, ci_card_no, ci_member_name, ci_benefit_type, ci_icd, ci_proposed_amount, ci_approved_amount, ci_excess_member, ci_excess_ssa, ci_guarantee_letter');
  }

  function register_batch(){
    $null = '';
    $new_code = '';
    $insurance = '';
    $data = $this->one('mst_code_number', 'WHERE mst_code_number.mnum_code = "2"', 'mnum_template, mnum_last_number_year, mnum_last_number_month, mnum_last_number_lenght, mnum_last_number_running, mnum_parameter');

    $year = $data['mnum_last_number_year'];
    $month = $data['mnum_last_number_month'];
    $digit = $data['mnum_last_number_lenght'];
    $last = $data['mnum_last_number_running'];
    $parameter = $data['mnum_parameter'];

    $lenght = strlen($last);
    $digit_for = $digit - $lenght;

    for ($i = 1; $i <= $digit_for; $i++) {
      $null .= '0';
    }
    $month = ($month < 10) ? '0' . $month : $month;
    
    if($parameter == 'S'){
      //$new_code = $prd.$month.$year.$insurance.$null.$last;
      $new_code = 'SSA'.$month.$year.$insurance.$null.$last;
    }elseif($parameter == 'C'){
      $template = $data['mnum_template'];
      $new_code = $template.$null.$last;
    }else if($parameter == 'M'){
      $template = $data['mnum_template'];
      //$month = get_romawi_month_date($month);
      //$new_code = $null.$last."/".$template."/".$month."/".$year;
      //$new_code = $template."-".$defsystem.".".$null.$last.".".$month.".".$year;
      $new_code = $year.$month."-".$null.$last;
    }
    return $new_code;
  }

  function admedika_daily_transaction_detail($table, $id = null){
    // mod-transaction-claim
    $cols = 'mst_benefit_item.mbi_ben_description,
            ' . $table . '.mbl_benefit_balance,
            ' . $table . '.mbl_benefit_limit_category,
             admedika_benefitcode_mapping.abcm_benefit_code,
             admedika_daily_transaction_detail.adtd_amt_incurred,
             admedika_daily_transaction_detail.adtd_amt_approved,
             admedika_daily_transaction_detail.adtd_amt_not_approved';

    $join = 'JOIN admedika_benefitcode_mapping ON admedika_daily_transaction_detail.adtd_benefit_code = admedika_benefitcode_mapping.abcm_benefit_code_admedika
             JOIN ' . $table . ' ON admedika_daily_transaction_detail.adtd_member_id = ' . $table . '.mbl_member_no AND admedika_benefitcode_mapping.abcm_benefit_code = ' . $table . '.mbl_benefit_code
             JOIN mst_benefit_item ON admedika_benefitcode_mapping.abcm_benefit_code = mst_benefit_item.mbi_ben_code AND ' . $table . '.mbl_benefit_code = mst_benefit_item.mbi_ben_code';

    $where = 'WHERE admedika_daily_transaction_detail.adtd_claim_id = "' . $id . '" AND admedika_daily_transaction_detail.adtd_validate_status = "Y" AND admedika_daily_transaction_detail.adtd_process_status = "Y"';

    return $this->Dml_model->read('admedika_daily_transaction_detail', $join . $where, $cols);
  }

  function policy($where){
    $cols = 'pol_pol_no, pol_member, pol_status,
             pol_service_level_day, pol_company_name,
             pol_product_code, pol_start_date,
             pol_fee_tpa, pol_fee_card, pol_end_date';
    return $this->read('policy', $where . 'ORDER BY pol_company_name ASC', $cols);
  }

  function provider(){
    $cols = 'mprov_city, mprov_bpjs, mprov_group,
             mprov_prov_code, mco_description,
             mprov_status, mprov_layanan_system,
             mprov_name, mprov_deadline_payment,
             mprov_telephone, mprov_province';

    $where = 'WHERE mco_status = "1" AND mprov_prov_code NOT IN("PSSA-88888", "PSSA-99999") AND mco_cat_id = "14"';
    return $this->read('mst_provider', 'JOIN mst_code ON mprov_type = mco_mastercode ' . $where, $cols);
  }

/*  function policy($method = 'read', $policy = null, $status = null){
    // mod-transaction-claim
    // mod-transaction-voucher-provider-listed
    $where = ($method == 'read') ? 'pol_status = "IN"' : null ;
    if (!empty($status)) {
      $status = (is_array($status)) ? '"' . implode('", "', $status) . '"' : $status ;

      $where[] = 'pol_services_type IN (' . $status . ')';
      $where[] = 'pol_pol_no = "' . $policy . '"';

      $where = implode(' AND ', $where);
    }

    return $this->Dml_model->{$method}('policy', 'WHERE  '. $where, 'pol_manual_claim_status, pol_services_type, pol_dedicated_account, pol_dedicated_account_normal, pol_pol_no, pol_company_name');
  }*/

  function table_claim_temp($table, $where = null/*$username, $policy, $member, $claim*/){
    // mod-transaction-claim
    // $where = 'WHERE temp_user = "' . $username . '" AND temp_policy_no = "' . $policy . '" AND temp_member_no = "' . $member . '" AND temp_claim_code = "' . $claim . '" AND temp_proposed > 0 ';
    $cols = 'temp_reason_code, temp_proposed, temp_cob,
             temp_benefit_description, temp_limit,
             temp_benefit_category, temp_excess,
             temp_benefit_code, temp_approved';

    $where = (empty($where)) ? null : ' AND ' . implode(' AND ', $where) ;

    return $this->Dml_model->read($table, 'WHERE temp_user = "' . $username . '"' . $where, 'temp_benefit_code, temp_benefit_description, temp_limit, temp_proposed, temp_approved');
  }

  function table_claim_process($table, $username, $policy, $member){
    // mod-transaction-claim
    $cols = 'temp_claim_code, temp_proposed, temp_cob,
             temp_benefit_description, temp_limit,
             temp_reason_code, temp_approved,
             temp_benefit_code, temp_excess';
    $where = 'WHERE temp_user = "' . $username . '" AND temp_policy_no = "' . $policy . '" AND temp_member_no = "' . $member . '"';
    return $this->Dml_model->read($table, $where, $cols);
  }

  function policy_benefit_family_class($policy, $type, $class){
    // mod-transaction-claim
    $where = 'WHERE benfac_pol_no = "' . $policy . '" AND benfac_benefit_type = "' . $type . '" AND benfac_benefit_class = "' . $class . '"';
    return $this->Dml_model->read('policy_benefit_family_class', $where, 'benfac_benefit_amount');
  }

  function mst_reason_claim($code = null){
    // mod-transaction-claim
    // get $code from table_claim_process
    if (!empty($code)) {
      $code = (is_array($code)) ? '"' . implode('", "', $code) . '"' : $code ;
      $code = 'WHERE msr_reason_code IN (' . $code . ')';
    }
    return $this->Dml_model->one('mst_reason_claim', $where, 'msr_reject_status, msr_delayed_status, msr_reason_name');
  }

  function table_limit_member($table, $cols, $method = 'one', $where = null, $join = null/*$policy, $member, $type, $code, $service*/){
    // mod-transaction-claim
    // get $code from table_claim_process

    // $code = (is_array($code)) ? '"' . implode('", "', $code) . '"' : $code ;
    // $where = 'WHERE mbl_benefit_limit_category = "K" AND mbl_pol_no = "' . $policy . '" AND mbl_member_no = "' . $member . '" AND mbl_benefit_type = "' . $type . '" AND mbl_benefit_code IN "' . $code . '" AND mbl_claim_last_date = "' . $service . '"';

    // 'mbl_claim_last_date, mbl_benefit_limit

    $where = implode(' AND ', $where);

    return $this->Dml_model->{$method}($table, $join . $where, $cols);
  }

  function claim_register_detail($register, $policy = null, $method = 'one', $cols_join = null /**/, $join = null /**/){
    // mod-transaction-claim
    // mod-transaction-voucher-provider
    // mod-transaction-supervisor-detail
    $cols = 'clmrd_approved_amount, clmrd_cob_amount,
             clmrd_proposed_amount, clmrd_policy_no,
             clmrd_excess_amount, clmrd_cases_claim';
    $where = (empty($policy)) ? '' : ' AND clmrd_policy_no = "' . $policy . '"' ;

    $cols = ($method == 'one') ? $cols : $cols . ', pol_company_name' ;
    $join = ($method == 'one') ? '' : 'JOIN policy ON claim_register_detail.clmrd_policy_no = policy.pol_pol_no' ;
    $order = ($method == 'one') ? '' : 'ORDER BY policy.pol_company_name ASC' ;

    return $this->Dml_model->{$method}('claim_register_detail', $join . 'WHERE clmrd_register_no = "' . $register . '"' . $where . $order, $cols);
  }

  function table_product_class ($table, $cols, $code, $type){
    // mod-transaction-claim-owlexa {prdclass_benefit_plan}
    // mod-transaction-claim-owlexa-sent {prdclass_plan_name}
    return $this->Dml_model->one($table, 'WHERE prdclass_class_code = "' . $code . '" AND prdclass_benefit_type = "' . $type . '"', 'DISTINCT ' . $cols);
  }

  function table_claim($table, $code){
    // mod-transaction-claim-owlexa-sent
    $cols = 'clm_excess_amount, clm_icd_desc3,
             clm_benefit_type, clm_icd_desc2,
             clm_register_no, clm_icd_desc1,
             clm_service_out, clm_member_no,
             clm_service_in, clm_claim_type,
             clm_proposed_amount, clm_icd3,
             clm_approved_amount, clm_icd2,
             clm_provider_code, clm_icd1';
    return $this->Dml_model->one($table, 'WHERE clm_claim_code = "' . $code . '"', $cols);
  }

  function temp_claim_upload($username){
    // mod-transaction-claim-upload
    $cols = 'tclaim_claim_amount_approve, tclaim_service_in,
             tclaim_claim_amount_excess, tclaim_service_out,
             tclaim_claim_amount_propose, tclaim_member_no,
             tclaim_benefit_type, tclaim_benefit_code';

    $order = 'ORDER BY tclaim_member_no, tclaim_service_in, tclaim_benefit_type, tclaim_benefit_code ASC';

    return $this->Dml_model->read('temp_claim_upload', 'WHERE tclaim_username = "' . $username . '"' . $order, $cols);
  }

  function mst_benefit_item($type, $code){
    // mod-transaction-claim-upload
    return $this->Dml_model->one('mst_benefit_item', 'WHERE mbi_status = "1" AND mbi_ben_type = "' . $type . '" AND mbi_ben_code = "' . $code . '"', 'mbi_ben_description');
  }

  function table_bentype($table){
    // mod-transaction-preview-claim
    // mod-transaction-prod-analysis
    $join = 'JOIN mst_code ON ' . $table . '.polbt_benefit_type = mst_code.mco_mastercode';
    $where = 'WHERE mst_code.mco_cat_id = "7" AND mst_code.mco_status = "1"';

    return $this->Dml_model->read($table, $join . $where, 'DISTINCT ' . $table . '.polbt_benefit_type, mst_code.mco_description');
  }

  function mst_code($code, $status = null){
    // mod-transaction-supervisor-owlexa
    // mod-transaction-preview-claim-detail
    // mod-transaction-supervisor-report-detail
    // mod-transaction-preview-claim-detail-excel
    // mod-transaction-supervisor-detail-external
    $status = (empty($status)) ? '' : ' AND mco_status = "1"' ;

    return $this->Dml_model->one('mst_code', 'WHERE mco_cat_id = "7" AND mco_mastercode = "' . $code . '"' . $status, 'mco_description, mco_mastercode');
  }

  function mst_login(){
    // mod-transaction-prod-analysis
    return $this->Dml_model->read('mst_login', 'WHERE mlog_status = "1" AND mlog_system = "A" AND mlog_function = "claimanalysis"', 'mlog_username, mlog_name'); 
  }

  function mst_provider($code){
    // mod-transaction-register
    // mod-transaction-voucher-provider
    return $this->Dml_model->one('mst_provider', 'WHERE mprov_prov_code = "' . $code . '"', 'mprov_deadline_payment, mprov_name');
  }

  function temp_claim_approve_external($username, $method = 'read'){
    // mod-transaction-supervisor-owlexa
    // mod-transaction-supervisor-owlexa-batch
    // mod-transaction-supervisor-detail-external
    // mod-transaction-supervisor-detail-external-batch
    $cols = 'tu_proposed_amount, tu_icd_code,
             tu_approve_amount, tu_member_no,
             tu_provider_code, tu_service_in,
             tu_provider_name, tu_claim_date,
             tu_benefit_type, tu_member_name,
             tu_icd_description, tu_reff_no,
             tu_excess_amount, tu_policy_no,
             tu_policy_name, tu_service_out';

    $cols = ($method == 'one') ? 'DISTINCT tu_provider_code' : $cols;

    return $this->Dml_model->{$method}('temp_claim_approve_external', 'WHERE tu_user = "' . $username . '"', $cols);
  }

  function callcenter_check_in_detail($type, $code){
    // mod-transaction-supervisor-detail-external-batch
    $cols = 'mst_benefit_item.mbi_ben_description, "" as limit_benefit,
             callcenter_check_in_detail.cid_proposed_amount,
             callcenter_check_in_detail.cid_approve_amount,
             callcenter_check_in_detail.cid_excess_amount,
             callcenter_check_in_detail.cid_benefit_code';

    $join = 'JOIN mst_benefit_item ON callcenter_check_in_detail.cid_benefit_code = mst_benefit_item.mbi_ben_code';

    $where = 'WHERE mst_benefit_item.mbi_ben_type = "' . $type . '" AND callcenter_check_in_detail.cid_reff_code = "' . $code . '"';

    return $this->Dml_model->read('callcenter_check_in_detail', $join . $where, $cols);
  }

  function owlexa_claim_detail($id, $type){
    // mod-transaction-supervisor-owlexa-batch
    $cols = 'mst_benefit_item.mbi_ben_description,
             owlexa_claim_detail.owd_ben_incurred,
             owlexa_claim_detail.owd_ben_approved,
             owlexa_claim_detail.owd_ben_declined,
             owlexa_claim_detail.owd_ben_code,
             "" AS limit_benefit';
    $join = 'JOIN mst_benefit_item ON owlexa_claim_detail.owd_ben_code = mst_benefit_item.mbi_ben_code';
    $where = 'WHERE owlexa_claim_detail.owd_claim_id = "' . $id . '" AND mst_benefit_item.mbi_ben_type = "' . $type . '"';

    return $this->Dml_model->read('owlexa_claim_detail', $join . $where, $cols);
  }

  function tabel_claim($tabelclaim, $tablemember, $register){
    // mod-transaction-voucher-provider
    // mod-transaction-voucher-reimbursement
    $cols = 'clm_proposed_amount, clm_member_no,
             clm_approved_amount, clm_icd_desc1,
             clm_excess_amount, clm_claim_date,
             clm_provider_name, clm_claim_code,
             clm_input_source, clm_cob_amount,
             clm_benefit_type, clm_service_in,
             clm_member_name, clm_service_out,
             clm_provider_reimburs, clm_icd1,
             clm_exgratia_amount, mmbr_name';
    $join = 'JOIN ' . $tablemember . ' ON concat(left(clm_member_no,7),"A") = ' . $tablemember . '.mmbr_member_no';
    $where = 'WHERE clm_status = "A" AND clm_register_no = "' . $register . '"';

    return $this->Dml_model->read($tabelclaim, $join . $where, $cols);
  }

  function mst_email_notification($from){
    // mod-transaction-voucher-provider {Voucher Claim Provider}
    return $this->Dml_model->read('mst_email_notification', 'WHERE men_form_name = "' . $from . '" AND men_status = "1"', 'men_email');
  }

  function claim_register($register, $status = null){
    // mod-transaction-voucher-provider
    $cols = 'clmreg_receive_date, clmreg_invoice_no, clmreg_claim_type,
             clmreg_sla_finish_date, clmreg_register_status,
             clmreg_claim_proposed, clmreg_claim_approve,
             clmreg_claim_excess, clmreg_invoice_date';
    $status = (empty($status)) ? '' : ' AND clmreg_register_status = "Y"' ;

    return $this->Dml_model->one('claim_register', 'WHERE clmreg_register_no = "' . $register . '"' . $status, $cols);
  }

  function claim_register_($code = null){
    // mod-transaction-voucher-provider
    $cols = (empty($code)) ? 'DISTINCT clmreg_claimer_code, clmreg_claimer_name' : 'clmreg_register_no, clmreg_receive_date' ;
    $in = (empty($code)) ? '"X", "Y", "V"' : '"X", "Y"' ;
    $where = (empty($code)) ? ' AND clmreg_paid_status != "9"' : ' AND clmreg_claimer_code = "' . $code . '"' ;
    $order = (empty($code)) ? ' ORDER BY clmreg_claimer_name' : '' ;

    return $this->Dml_model->read('claim_register', 'WHERE clmreg_claim_type = "P" AND clmreg_register_status IN (' . $in . ')' . $where . $order, $cols);
  }

  function voucher_payment_claim($code, $limit = 1, $category = null, $search = null){
    // mod-transaction-voucher-provider-print-master
    // mod-transaction-voucher-reimbursement-print-master
    $cols = 'vpc_sla_finish_date, vpc_batch_no,
             vpc_voucher_amount, vpc_voucher_no,
             vpc_count_member, vpc_payment_date,
             vpc_payment_status, vpc_voucher_date';

    $where = (empty($category)) ? '' : ' AND ' . $category . 'LIKE "%' . $search . '%"' ;
    $limit = ' LIMIT ' . ($limit - 1) . ', ' . ($limit * 50);

    return $this->Dml_model->read('voucher_payment_claim', 'WHERE vpc_status = "1" AND vpc_claimer_code = "' . $code . '" ' . $where . ' ORDER BY vpc_voucher_date DESC ' . $limit);
  }

  function tabelclaim($table, $register){
    // mod-transaction-voucher-reimbursement
    $cols = 'SUM(clm_proposed_amount), SUM(clm_cob_amount),
             SUM(clm_approved_amount), COUNT(clm_claim_code),
             SUM(clm_exgratia_amount), SUM(clm_excess_amount)';
    return $this->Dml_model->read($table, 'WHERE clm_status = "A" AND clm_register_no = "' . $register . '"', $cols);
  }

  // function (){return $this->Dml_model->();}

  function custom($query){
    return $this->Dml_model->custom($query);
  }

  function procedure($procedure){
    return $this->Dml_model->procedure($procedure);
  }

  function creade($create, $read, $condition, $data){
    return $this->Dml_model->creade($create, $read, $condition, $data);
  }

  function one($table, $condition = null, $fields = "*"){
    return $this->Dml_model->one($table, $condition, $fields);
  }

  function create_batch($table, $data){
    return $this->Dml_model->create_batch($table, $data);
  }

  function create($table, $data = null){
    return $this->Dml_model->create($table, $data);
  }

  function read($table, $condition = null, $fields = "*"){
    return $this->Dml_model->read($table, $condition, $fields);
  }

  function update($table, $where, $data = null){
    return $this->Dml_model->update($table, $where, $data);
  }

  function delete($table, $where){
    return $this->Dml_model->delete($table, $where);
  }

  function upload($data, $url = null, $rename = null){
    return $this->Dml_model->upload($data, $url, $rename);
  }

}