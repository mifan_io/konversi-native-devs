<?php
// https://www.teletopiasms.no/np/frontpage/gateway/api-http-examples-php
// https://stackoverflow.com/questions/5647461/how-do-i-send-a-post-request-with-php
class Finance_model extends CI_Model {

  public function __construct() {
    parent::__construct();
    $this->load->database();
    $this->load->model('Dml_model');
  }

  function temp_invoice_member($username, $policy, $voucher = null){
    // mod-finance-invoice-member
    return $this->Dml_model->read('temp_invoice_member', 'WHERE tim_username = "' . $username . '" AND tim_policy_no = "' . $policy . '"' . $voucher, 'tim_voucher_no');
  }

  function voucher_membership($policy = null){
    // mod-finance-invoice-member
    $policy = (empty($policy)) ? '' : ' AND vm_policy_code = "' . $policy . '"' ;
    return $this->Dml_model->read('voucher_membership', 'WHERE (vm_invoice_code = "" OR vm_invoice_code IS NULL) ' . $policy . ' ORDER BY vm_voucher_date DESC', 'DISTINCT(vm_policy_code) vm_policy_code, vm_policy_name');
  }

  function voucher_member($username, $policy){
    // mod-finance-invoice-member
    $_voucher = ', (SELECT tim_voucher_no FROM temp_invoice_member WHERE tim_username = "' . $username . '" AND tim_policy_no = "' . $policy . '" AND tim_voucher_no = vm_voucher_code) tim_voucher_no';

    return $this->Dml_model->one('voucher_membership', 'WHERE vm_invoice_code = "" AND vm_policy_code = "' . $policy . '" ORDER BY vm_voucher_date DESC', 'DISTINCT(vm_policy_code) vm_policy_code, vm_voucher_date' . $_voucher);
  }

  function invoice($username, $policy){
    // mod-finance-invoice-member
    $_voucher = 'SELECT tim_voucher_no FROM temp_invoice_member WHERE tim_username = "' . $username . '" AND tim_policy_no = "' . $policy . '"';
    return $this->Dml_model->one('voucher_membership', 'WHERE vm_policy_code = "' . $policy . '" AND vm_voucher_code IN (' . $_voucher . ')', 'SUM(vm_voucher_members) member, SUM(vm_voucher_amount) amount');
  }

  function detail($table, $username, $policy){
    // mod-finance-invoice-member
    $cols = 'mmbr_employee_id, mmbr_card_nomor,
             mmbr_member_no, mmbr_enrol_code,
             mmbr_start_date, mmbr_end_date,
             mmbr_sex, mmbr_marital_status,
             mmbr_name, mmbr_jabatan,
             mmbr_dob, mmbr_age';
    $invoice = 'SELECT tim_voucher_no FROM temp_invoice_member WHERE tim_username = "' . $username . '" AND tim_policy_no = "' . $policy . '"';
    return $this->Dml_model->read($table, 'WHERE mmbr_invoice_no IN (' . $invoice . ')', $cols);
  }

  function invoice_member($invoice){
    // mod-finance-invoice-member-print
    $cols = 'vm_enrol_type, vm_endors_code,
             vm_policy_code, vm_policy_name,
             vm_invoice_code, vm_invoice_date,
             policy.pol_city, policy.pol_province,
             vm_invoice_amount, policy.pol_address,
             policy.pol_end_date, vm_voucher_members,
             policy.pol_zipcode, policy.pol_start_date,
             vm_voucher_tpa_amount, vm_voucher_card_amount,
             voucher_membership.vm_invoice_ppn, voucher_membership.vm_invoice_netto';
    return $this->Dml_model->one('voucher_membership', 'JOIN policy ON vm_policy_code = policy.pol_pol_no WHERE vm_invoice_code = "' . $invoice . '"', $cols);
  }

  function membership($limit = 1, $where = null){
    // mod-finance-invoice-member-print-master
    $limit = ' LIMIT ' . ($limit - 1) . ', ' . ($limit * 50);
    $where = (empty($where)) ? '' : ' AND (' . $where . ')';
    return $this->Dml_model->read('voucher_membership', 'WHERE (vm_invoice_code <> "" OR vm_invoice_code IS NOT NULL) ' . $where . ' GROUP BY vm_invoice_code, vm_invoice_date, vm_invoice_paid, vm_invoice_paid_date' . $limit);
  }

  function invoice_tpa($id_pol, $limit = 1, $where = null){
    // mod-finance-invoicetpa-master
    $limit = ' LIMIT ' . ($limit - 1) . ', ' . ($limit * 50);
    $where = (empty($where)) ? '' : ' AND (' . $where . ')';
    return $this->Dml_model->read('invoice_tpa', 'WHERE invtpa_payment_status = "N" AND invoice_tpa.invtpa_policy_number = "' . $id_pol . $where . '" ORDER BY invtpa_stamp_date DESC' . $limit, 'invtpa_invoice_code, invtpa_date, invtpa_count_member, invtpa_amount_invoice, invtpa_amount_card, (invtpa_amount_invoice + invtpa_amount_card) AS total');
  }

  function invoice_tpa_print($id_invoice){
    // mod-finance-invoice-tpa-print
    $cols = 'invtpa_duedate, pol_zipcode, pol_fax,
             invtpa_amount_invoice, pol_province,
             invtpa_policy_name, pol_address,
             invtpa_amount_card, invtpa_date,
             invtpa_policy_number, pol_city,
             invtpa_count_member, pol_phone';
    return $this->Dml_model->one('invoice_tpa', 'JOIN policy ON invoice_tpa.invtpa_policy_number = policy.pol_pol_no WHERE invtpa_invoice_code = "' . $id_invoice . '"', $cols);
  }

  function claim_detail($provider, $voucher){
    // mod-finance-paybills-claim-provider
    return $this->Dml_model->one('claim_register_detail', 'JOIN voucher_payment_claim ON claim_register_detail.clmrd_register_no = voucher_payment_claim.vpc_batch_no WHERE voucher_payment_claim.vpc_claimer_code = "' . $provider . '" AND voucher_payment_claim.vpc_voucher_no = "' . $voucher . '"', 'claim_register_detail.clmrd_policy_no, claim_register_detail.clmrd_register_no');
  }

  function email_notif($from){
    // mod-finance-paybills-claim-provider
    // mod-finance-paybills-claim-reimbursment
    // mod-finance-receivepay-membership
    return $this->Dml_model->read('mst_email_notification', 'WHERE men_form_name = "' . $from . '" AND men_status = 1', 'men_email');
  }

  function bank_notif($bank){
    // mod-finance-receivepay-eoc
    // mod-finance-receivepay-membership
    // mod-finance-paybills-claim-provider
    // mod-finance-paybills-claim-reimbursment
    return $this->Dml_model->one('mst_bank', 'WHERE msbank_id = "' . $bank . '"', 'msbank_bank_name');
  }

  function claim_notif($claim){
    // mod-finance-paybills-claim-provider
    return $this->Dml_model->one('claim_register', 'WHERE clmreg_register_no = "' . $claim . '"', 'clmreg_claimer_name');
  }

  function claimer($provider = null, $type = 'P',$voucher = null){
    // mod-finance-paybills-claim-provider
    // mod-finance-paybills-claim-reimbursment
    $where = [];

    $cols = (empty($provider)) ? 'DISTINCT vpc_claimer_code, vpc_claimer_name' : 'vpc_voucher_no, vpc_voucher_date, vpc_voucher_amount' ;
    $cols = (empty($voucher)) ? $cols : 'vpc_voucher_date, vpc_batch_no, vpc_sla_finish_date, vpc_voucher_amount, vpc_voucher_user, vpc_count_member, vpc_voucher_no' ;

    $where[] = (empty($voucher)) ? '' : 'vpc_voucher_no = "' . $voucher . '"' ;
    $where[] = (empty($provider)) ? '' : 'vpc_claimer_code = "' . $provider . '"' ;
    $where[] = (trim($type) == '') ? '' : '(vpc_claim_type = "' . $type . '" AND vpc_payment_status = "N")' ;

    $order = (empty($provider)) ? ' vpc_claimer_code ASC' : ' vpc_voucher_no DESC' ;

    $method = (trim($type) == '') ? 'one' : 'read' ;

    foreach ($where as $key => $val) {
      if (empty($val) || trim($val) == '') {
        unset($where[$key]);
      }
    }

    $where = implode(' AND ', $where);

    return $this->Dml_model->{$method}('voucher_payment_claim', 'WHERE ' . $where . ' ORDER BY' . $order, $cols);
  }

  function bank(){
    // mod-finance-paybills-claim-provider
    // mod-finance-paybills-claim-reimbursment
    return $this->Dml_model->one('mst_bank', 'WHERE msbank_status = 1 ORDER BY msbank_bank_name ASC', 'msbank_id, msbank_bank_name');
  }

  function reg_detail($provider, $voucher){
    // mod-finance-paybills-claim-provider
    return $this->Dml_model->read('claim_register_detail', 'WHERE voucher_payment_claim.vpc_claimer_code = "' . $provider . '" AND voucher_payment_claim.vpc_voucher_no = "' . $voucher . '"', 'claim_register_detail.clmrd_policy_no');
  }

  function reg_claim_detail($table, $voucher, $provider = null){
    // mod-finance-paybills-claim-provider
    // mod-finance-paybills-claim-reimbursment
    $cols = 'clm_proposed_amount, clm_member_no,
             clm_approved_amount, clm_icd_desc1,
             clm_excess_amount, clm_cob_amount,
             clm_provider_name, clm_service_in,
             clm_input_source, clm_claim_date,
             clm_benefit_type, clm_claim_code,
             clm_member_name, clm_service_out,
             clm_provider_reimburs, clm_icd1,
             clm_exgratia_amount, mmbr_name';
    $join = 'JOIN ' . $table . '_mst_member ON concat(left(clm_member_no,7),"A") = ' . $table . '_mst_member.mmbr_member_no';
    $group = (empty($provider)) ? '' : 'GROUP BY ' . $table . '_mst_member.mmbr_member_no, 
              ' . $table . '_mst_member.mmbr_name, 
              ' . $table . '_mst_member.mmbr_acc_no,
              ' . $table . '_mst_member.mmbr_acc_name, 
              ' . $table . '_mst_member.mmbr_bank, 
              ' . $table . '_mst_member.mmbr_bank_branch';

    $where = [];
    $where[] = (empty($voucher)) ? '' : 'vpc_voucher_no = "' . $voucher . '"' ;
    $where[] = (empty($provider)) ? '' : 'vpc_claimer_code = "' . $provider . '"' ;
    foreach ($where as $key => $val) {
      if (empty($val) || trim($val) == '') {
        unset($where[$key]);
      }
    }

    $where = implode(' AND ', $where);

    $batch = $this->Dml_model->one('voucher_payment_claim', 'WHERE ' . $where, 'vpc_batch_no');
    return $this->Dml_model->read($table . '_claim', $join . 'WHERE clm_status = "V" AND clm_register_no = "' . $batch['vpc_batch_no'] . '"');
  }

  function policy($policy){
    // mod-finance-paybills-claim-reimbursment
    return $this->Dml_model->one('policy', 'WHERE pol_pol_no = "' . $policy . '"', 'pol_company_name');
  }

  function finance_plan_payment($planfinance = null, $status = null){
    // mod-finance-payment-reimbursement
    $where = [];
    $where[] = (empty($status)) ? '' : 'fp_status_planning != "' . $status . '"' ;
    $where[] = (empty($planfinance)) ? '' : 'fp_planfinance_code = "' . $planfinance . '"' ;

    foreach ($where as $key => $val) {
      if (empty($val) || trim($val) == '') {
        unset($where[$key]);
      }
    }

    $where = implode(' AND ', $where);

    $method = (empty($status)) ? 'one' : 'read' ;

    return $this->Dml_model->{$method}('finance_plan_payment', 'WHERE ' . $where, 'fp_provider_policy_code, fp_provider_policy_name, fp_register_no, fp_planfinance_date');
  }

  function voucher_eoc($policy = null, $invoice = null, $detail = false){
    // mod-finance-receivepay-eoc
    $method = ($detail == false) ? 'read' : 'one' ;

    $cols = (empty($voucher)) ? 'DISTINCT vieoc_policy_code, vieoc_policy_name' : 'DISTINCT vieoc_invoice_no, vieoc_invoice_date, vieoc_claim_cases, vieoc_excess_amount' ;
    $cols = ($detail == false) ? $cols : 'vieoc_invoice_no, vieoc_invoice_date, vieoc_claim_cases, vieoc_excess_amount, vieoc_invoice_user' ;

    $where = [];
    $where[] = (empty($policy)) ? '' : 'vieoc_policy_code = "' . $policy . '"' ;
    $where[] = (empty($invoice)) ? '' : 'vieoc_invoice_no = "' . $invoice . '"' ;

    foreach ($where as $key => $val) {
      if (empty($val) || trim($val) == '') {
        unset($where[$key]);
      }
    }

    $where = (empty($where)) ? '' : ' AND '.implode(' AND ', $where) ;

    $order = (empty($voucher)) ? ' ORDER BY vieoc_policy_name ASC' : ' ORDER BY vieoc_invoice_no DESC' ;
    $order = ($detail == false) ? $order : '' ; 

    return $this->Dml_model->{$method}('voucher_eoc', 'WHERE vieoc_payment_status = "N"' . $where . $order, $cols);
  }

  function voucher_eoc_detail($policy, $invoice){
    // mod-finance-receivepay-eoc
    $cols = 'vieod_provider_name, vieod_claim_date,
             vieod_icd_description, vieod_serv_in,
             vieod_claim_proposed, vieod_icd_code,
             vieod_claim_approved, vieod_serv_out,
             vieod_provider_code, vieod_member_no,
             vieod_claim_excess, vieod_claim_code,
             vieod_claim_source, vieod_claim_type,
             vieod_benefit_type, vieod_claim_note,
             vieod_member_name';
    return $this->Dml_model->read('voucher_eoc_detail', 'JOIN voucher_eoc ON vieod_invoice_no = vieoc_invoice_no WHERE vieod_invoice_no = "' . $invoice . '" AND vieoc_policy_code = "' . $policy . '"', $cols);
  }

  function mst_member($table, $invoice){
    // mod-finance-receivepay-membership
    $cols = 'mmbr_enrol_code, mmbr_sex,
             mmbr_employee_id, mmbr_dob,
             mmbr_enrol_type, mmbr_name,
             mmbr_member_no, mmbr_jabatan';
    return $this->Dml_model->read($table . '_mst_member', 'WHERE mmbr_invoice_no = "' . $invoice . '"', $cols);
  }

  function email_voucher_membership($policy = null, $invoice = null){
    // mod-finance-receivepay-membership
    $cols = 'vm_voucher_card_amount, vm_enrol_type,
             vm_voucher_tpa_amount, vm_endors_code,
             vm_voucher_members, vm_policy_name,
             vm_voucher_amount, vm_invoice_date';

    $where = [];
    $where[] = (empty($policy)) ? '' : 'vieoc_policy_code = "' . $policy . '"' ;
    $where[] = (empty($invoice)) ? '' : 'vieoc_invoice_no = "' . $invoice . '"' ;

    foreach ($where as $key => $val) {
      if (empty($val) || trim($val) == '') {
        unset($where[$key]);
      }
    }

    $where = implode(' AND ', $where) ;

    return $this->Dml_model->one('voucher_membership', 'WHERE ' . $where, $cols);
  }

  function select_voucher_membership($policy = null, $invoice = null){
    // mod-finance-receivepay-membership
    $cols = (empty($policy)) ? 'DISTINCT vm_policy_code, vm_policy_name' : 'vm_invoice_code, vm_voucher_date, SUM(vm_voucher_members)' ;
    $cols = (empty($invoice)) ? $cols : 'vm_voucher_date, SUM(vm_voucher_members), SUM(vm_voucher_amount), vm_voucher_user' ;

    $where = [];
    $where[] = (empty($policy)) ? '' : 'vieoc_policy_code = "' . $policy . '"' ;
    $where[] = (empty($invoice)) ? '' : 'vieoc_invoice_no = "' . $invoice . '"' ;

    foreach ($where as $key => $val) {
      if (empty($val) || trim($val) == '') {
        unset($where[$key]);
      }
    }

    $where = (empty($where)) ? '' : ' AND '.implode(' AND ', $where) ;

    $method = (empty($invoice)) ? 'read' : 'one' ;

    $group = (empty($policy)) ? '' : ' GROUP BY vm_invoice_code, vm_voucher_date' ;
    $group = (empty($invoice)) ? $group : ' GROUP BY vm_voucher_date, vm_voucher_user' ;

    $order = (empty($policy)) ? ' ORDER BY vm_policy_name ASC' : ' ORDER BY vm_invoice_code DESC' ;
    $order = (empty($invoice)) ? $order : '' ;

    return $this->Dml_model->{$method}('voucher_membership', 'WHERE vm_invoice_paid = "N" AND (vm_invoice_code != "" OR vm_invoice_code IS NULL) ' . $where . $group . $order, '');
  }

  function procedure($procedure){
    return $this->Dml_model->procedure($procedure);
  }

  function create($table, $data = null){
    return $this->Dml_model->create($table, $data);
  }

  function update($table, $where, $data = null){
    return $this->Dml_model->update($table, $where, $data);
  }

  function delete($table, $where){
    return $this->Dml_model->delete($table, $where);
  }

  function upload($data, $url = null, $rename = null){
    return $this->Dml_model->upload($data, $url, $rename);
  }

}