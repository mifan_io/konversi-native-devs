<?php
// https://www.teletopiasms.no/np/frontpage/gateway/api-http-examples-php
// https://stackoverflow.com/questions/5647461/how-do-i-send-a-post-request-with-php
/*
cdate
cdatetime
ctime
systemcode
*/
class Administrator_model extends CI_Model {

  public function __construct() {
    parent::__construct();
    $this->load->database();
    $this->load->model('Dml_model');
  }

  function hashing($username, $password){
    $hash = 'adzhwa'.$username.'9113'.$password.'086666!@#$%^&*()ClUmSyArt';
    return md5(md5($hash));
  }

  function username($username, $system, $password = null){
    if(!empty($password)){
      $hashing = $this->hashing($username, $password);
      $password = ' AND mlog_password = "' . $hashing . '"' ;
    }

    return $this->Dml_model->one('mst_login', 'WHERE mlog_username = "' . $username . '" AND mlog_system = "' . $system . '"' . $password, 'mlog_username');
  }

  function admin_system($system){
    return $this->Dml_model->read('mst_login', 'WHERE mlog_system = "' . $system . '" AND mlog_status = 1', 'mlog_username AS vals, mlog_name AS label');
  }

  function div(){
    return $this->Dml_model->read('mst_code', 'WHERE mco_cat_id = 23 AND mco_status = 1', 'mco_mastercode, mco_description');
  }

  function setPassword($username, $password, $system){
    $data['mlog_password'] = $this->hashing($username, $password);

    $this->Dml_model->update('mst_login', 'mlog_username = "' . $username . '" AND mlog_system = "' . $system . '"', $data);
  }

  function users(){
    $mlog_system = "CASE mlog_system 
                    WHEN 'A' THEN 'Healthcare'
                    WHEN 'B' THEN 'Call Center'
                    WHEN 'C' THEN 'Provider'
                    WHEN 'D' THEN 'SHIS'
                    WHEN 'E' THEN 'Dashboard'
                    END";
    return $this->Dml_model->read('mst_login', 'WHERE mlog_status = 1 ORDER BY mlog_name', 'mlog_status, mlog_username, mlog_name, mlog_jabatan, mlog_function, (' . $mlog_system . ') AS mlog_system');
  }

  function menu($username, $type){
    $id_menu = null;
    $access = '(SELECT mua_idmenu FROM mst_system_access WHERE mua_username = "' . $username . '" AND mua_system = "' . $type . '" AND mua_idmenu =  mst_submenu.mss_id) AS mua_idmenu';

    $menu = $this->Dml_model->read('mst_menu', 'WHERE msm_system = "' . $type . '" AND msm_status = 1', 'msm_id, msm_menu_category');

    foreach ($menu as $key => $value) {
      $id_menu[] = $value['msm_id'];
    }

    $menu_id = implode(', ', $id_menu);

    $submenu = $this->Dml_model->read('mst_submenu', 'WHERE mss_menu_category IN (' . $menu_id . ') AND mss_status = 1', 'mss_id, mss_submenu_desc, mss_menu_category super_id,' . $access);

    foreach ($menu as &$arr1) {
      $offer_id = $arr1['msm_id'];  

      $match = array_filter($submenu, function($v) use ($offer_id){
        return $v['super_id'] == $offer_id;
      });

      // $match = array_pop($match);
      $arr1['submenu'] = array_values($match);
    }

    return $menu;
  }

  function create_batch($table, $data){
    return $this->Dml_model->create_batch($table, $data);
  }

  function create($table, $data = null){
    return $this->Dml_model->create($table, $data);
  }

  function update($table, $where, $data = null){
    return $this->Dml_model->update($table, $where, $data);
  }

  function delete($table, $where){
    return $this->Dml_model->delete($table, $where);
  }

}