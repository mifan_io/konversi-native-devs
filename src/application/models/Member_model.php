<?php
// https://www.teletopiasms.no/np/frontpage/gateway/api-http-examples-php
// https://stackoverflow.com/questions/5647461/how-do-i-send-a-post-request-with-php
class Member_model extends CI_Model {

  public function __construct() {
    parent::__construct();
    $this->load->database();
    $this->load->model('Dml_model');
  }

  function family($no, $member){
    return $this->read($no . '_mst_member', 'WHERE mmbr_status = "1" AND mmbr_terminate_date = "0000-00-00" AND LEFT(mmbr_member_no,7) = "' . substr($member,0,7) . '" AND mmbr_pol_no = "' . $no . '" ORDER BY mmbr_member_no ASC', 'mmbr_member_no, mmbr_name, mmbr_employee_id, mmbr_card_nomor, mmbr_class_code, mmbr_enrol_code, mmbr_dob , mmbr_enrol_type, mmbr_sex, mmbr_age, mmbr_marital_status');
  }

  function karyawan($no){
    return $this->read($no . '_mst_member', 'WHERE mmbr_status = "1" AND mmbr_terminate_date = "0000-00-00" AND RIGHT(mmbr_member_no,1) = "A"', 'mmbr_member_no, mmbr_name, mmbr_employee_id, mmbr_card_nomor, mmbr_class_code, mmbr_enrol_code, mmbr_dob, mmbr_phone, mmbr_handphone, mmbr_email, mmbr_departement, mmbr_jabatan, mmbr_cabang, mmbr_acc_no, mmbr_acc_name, mmbr_bank, mmbr_bank_branch, mmbr_marital_status');
  }

  function benefit($no){
    $insql = ', CONCAT("Class :: ", prdclass_class_code, " ", (SELECT GROUP_CONCAT(DISTINCT menu.prdclass_benefit_plan separator " | ") FROM ' . $no . '_product_class menu WHERE menu.prdclass_class_code = ' . $no . '_product_class.prdclass_class_code)) label';
    return $this->read($no . '_product_class', 'ORDER BY prdclass_class_code', 'DISTINCT prdclass_class_code vals' . $insql);
  }

  function suspend($no){
    return $this->read('member_suspend_history', 'JOIN ' . $no . '_mst_member ON mbs_policy_no = mmbr_pol_no AND mbs_member_no = mmbr_member_no WHERE mbs_policy_no = "' . $no . '" AND (mbs_reactive_date = "0000-00-00" OR mbs_reactive_date IS NULL)', 'DISTINCT mbs_member_no mmbr_member_no, mmbr_name, mmbr_card_nomor, mmbr_enrol_code, mmbr_dob');
  }

  function card($no){
    $cols = 'member.mmbr_sex, DATE_FORMAT(member.mmbr_start_date, "%d %b %Y") start_date,
             member.mmbr_age, DATE_FORMAT(member.mmbr_end_date, "%d %b %Y") end_date,
             member.mmbr_member_no, DATE_FORMAT(member.mmbr_dob, "%d %b %Y") dob,
             member.mmbr_class_code, member.mmbr_card_nomor, mmbr_status,
             member.mmbr_employee_id, member.mmbr_name member_name,';
    $cols .= '(SELECT GROUP_CONCAT(emp.mmbr_name  separator ", ") FROM ' . $no . '_mst_member AS emp WHERE RIGHT (emp.mmbr_member_no, 1) = "A" AND LEFT (emp.mmbr_member_no, 7) = LEFT (member.mmbr_member_no, 7)) employee,';
    $cols .= '(SELECT GROUP_CONCAT(DISTINCT(plan.prdclass_plan_name)  separator ", ") FROM ' . $no . '_product_class plan WHERE plan.prdclass_class_code = member.mmbr_class_code) plan,';
    $cols .= '(SELECT GROUP_CONCAT(plan.prdclass_benefit_plan  separator ", ") FROM ' . $no . '_product_class plan WHERE plan.prdclass_class_code = member.mmbr_class_code AND plan.prdclass_benefit_type = "IP") inpatient,';
    $cols .= '(SELECT GROUP_CONCAT(plan.prdclass_benefit_plan  separator ", ") FROM ' . $no . '_product_class plan WHERE plan.prdclass_class_code = member.mmbr_class_code AND plan.prdclass_benefit_type = "OP") outpatient,';
    $cols .= '(SELECT GROUP_CONCAT(plan.prdclass_benefit_plan  separator ", ") FROM ' . $no . '_product_class plan WHERE plan.prdclass_class_code = member.mmbr_class_code AND plan.prdclass_benefit_type = "DE") dental,';
    $cols .= '(SELECT GROUP_CONCAT(plan.prdclass_benefit_plan  separator ", ") FROM ' . $no . '_product_class plan WHERE plan.prdclass_class_code = member.mmbr_class_code AND plan.prdclass_benefit_type = "GL") glasses,';
    $cols .= '(SELECT GROUP_CONCAT(plan.prdclass_benefit_plan  separator ", ") FROM ' . $no . '_product_class plan WHERE plan.prdclass_class_code = member.mmbr_class_code AND plan.prdclass_benefit_type = "MA") maternity';

    $where = 'WHERE member.mmbr_member_no <> ""';
    $order = 'ORDER BY employee ASC';

    return $this->read($no . '_mst_member member', $where . ' ' . $order, $cols);
  }

  function child($no, $member){
    $data = $this->one($no . '_mst_member', 'WHERE mmbr_enrol_code = "CHL" AND mmbr_pol_no = "' . $no . '" AND LEFT(mmbr_member_no,7) = "' . substr($member,0,7) . '"', 'COUNT(mmbr_enrol_code) count');
    return $data['count'];
  }

  function gen_card($no, $where = '"%A"'){
    if ($where != '"%A"') {
      $where  = substr($where, 0, 15);
    }
    $card = $this->one($no . '_mst_member', 'WHERE mmbr_card_nomor LIKE ' . $where . ' ORDER BY ' . $no . '_mst_member.mmbr_card_nomor DESC', 'mmbr_card_nomor');
    return (substr($card['mmbr_card_nomor'], 0, 15) + 1) ;
  }

  function policy($no){
    $policy = $this->one('policy_code_number', 'WHERE mnum_code = "1" AND mnum_policy_code = "' . $no . '"', 'mnum_template, mnum_last_number_year, mnum_last_number_month, mnum_last_number_lenght, mnum_last_number_running, mnum_parameter');

    $null = '';
    $new_code = '';
    $number = $policy['mnum_last_number_running'];
    $last = strlen($number);
    $parameter = $policy['mnum_parameter'];
    $month = $policy['mnum_last_number_month'];
    $year = $policy['mnum_last_number_year'];


    $digit = $policy['mnum_last_number_lenght'];
    $digit_for = $digit - $last;

    for ($i = 1; $i <= $digit_for; $i++) {
      $null .= '0';
    }

    if($month < '10'){
      $conv_month = '0'.$month;
    }else{
      $conv_month = $month;
    }
    
    if($parameter == 'S'){
      //$new_code = $kdprd.$kdmonth.$kdyear.$kdinsurance.$null.$number;
      $new_code = 'SSA'.$kdmonth.$kdyear.$kdinsurance.$null.$number;
    }elseif($parameter == 'C'){
      $template = $policy['mnum_template'];
      $new_code = $template.$null.$number;
    }else if($parameter == 'M'){
      $template = $policy['mnum_template'];
      //$conv_month = get_romawi_month_date($month);
      //$new_code = $null.$number."/".$template."/".$conv_month."/".$year;
      $new_code = $template.$null.$number;
    }
    return $new_code;
  }

  function profile($no, $member, $card){
    $insql  = ', (SELECT GROUP_CONCAT(DISTINCT prdclass_plan_name separator ", ") FROM  ' . $no . '_product_class WHERE prdclass_class_code = mmbr_class_code ) prdclass_plan_name';
    $insql .= ', (SELECT GROUP_CONCAT(CONCAT(mst_code.mco_description, " | ", prdclass_benefit_plan, " : ", prdclass_benefit_type) separator ", ") FROM ' . $no . '_product_class INNER JOIN mst_code ON prdclass_benefit_type = mst_code.mco_mastercode WHERE prdclass_class_code = mmbr_class_code AND mst_code.mco_cat_id = "7") policy';
    $insql .= ', (SELECT GROUP_CONCAT(CONCAT(prdclass_benefit_type, " : ", prdclass_benefit_plan) separator ", ") FROM ' . $no . '_product_class INNER JOIN mst_code ON prdclass_benefit_type = mst_code.mco_mastercode WHERE prdclass_class_code = mmbr_class_code AND mst_code.mco_cat_id = "7") benefit';
    // $insql .= ', (SELECT GROUP_CONCAT(prdclass_benefit_plan separator ", ") FROM ' . $no . '_product_class WHERE prdclass_class_code = mmbr_class_code ) prdclass_benefit_plan';
    // $insql .= ', (SELECT GROUP_CONCAT(CONCAT(DISTINCT mst_code.mco_description, ": ", prdclass_benefit_type, ": ", prdclass_benefit_plan) separator ", ") FROM ' . $no . '_product_class INNER JOIN mst_code ON prdclass_benefit_type = mst_code.mco_mastercode WHERE prdclass_class_code = mmbr_class_code AND mst_code.mco_cat_id = "7") plafond';
    $base = $this->base_member($no, $member, $card, $insql);

    $product = explode(', ', $base['benefit']);
    $param = [];

    foreach ($product as $key => $val){
      $vals = explode(' : ', $val);
      $param['type'][$key] = '"' . $vals[0] . '"';
      $param['code'][$key] = '"' . $vals[1] . '"';
    };

    $insql = ', (SELECT mco_description FROM mst_code WHERE mco_cat_id = 7 AND mco_mastercode = mastercode) description';

    $base['benefit_data'] = $this->read($no . '_product_benefit_plan', 'JOIN mst_benefit_item ON prdplan_ben_code = mst_benefit_item.mbi_ben_code AND prdplan_ben_type = mst_benefit_item.mbi_ben_type WHERE prdplan_ben_type IN (' . implode(', ', $param['type']) . ') AND prdplan_plan_code IN (' . implode(', ', $param['code']) . ')', 'prdplan_ben_type mastercode, prdplan_ben_type, prdplan_plan_code, prdplan_ben_code, mst_benefit_item.mbi_ben_description, prdplan_provider_category, prdplan_benefit_amount, prdplan_category' . $insql);

    $base['plafond'] = $this->read($no . '_mst_member_limit', 'JOIN mst_benefit_item ON mbl_benefit_code = mbi_ben_code AND mbl_benefit_type = mbi_ben_type WHERE mbl_member_no = "' . $member . '" AND mbl_benefit_type IN (' . implode(', ', $param['type']) . ') AND mbl_benefit_limit_category = "T"', 'mbl_benefit_type mastercode, mbl_benefit_type, mbl_benefit_code, mbi_ben_description, mbl_benefit_limit_category, mbl_benefit_limit, mbl_benefit_balance, mbl_benefit_proposed, mbl_benefit_approved, mbl_benefit_excess, mbl_claim_last_date' . $insql);

    $base['bpjs'] = $this->read($no . '_member_bpjs_payment', 'WHERE pbpjs_member_no = "' . $no . '" ORDER BY pbbjs_payment_date DESC', 'pbpjs_periode_month,  pbbjs_payment_date, pbbjs_class, pbbjs_payment_amount, pbbjs_payment_by, pbbjs_stamp, pbbjs_user');

    return $base;
  }

  function valid_member($no, $member){
    return $this->Dml_model->one($no . '_mst_member', 'WHERE mmbr_member_no = "' . $member . '"', 'mmbr_member_no');
  }

  function base_member($no, $member, $card, $insql = null){
    $cols = 'mmbr_vip, mmbr_dob, mmbr_sex,
             mmbr_bpjs_ppk1_code, mmbr_name,
             mmbr_enrol_type, mmbr_end_date,
             mmbr_terminate_reason, mmbr_age,
             mmbr_bpjs_nomor, mmbr_enrol_code,
             mmbr_bpjs_class, mmbr_card_nomor,
             mmbr_start_date, mmbr_class_code,
             mmbr_employee_id, mmbr_member_no,
             mmbr_marital_status, mmbr_cabang,
             mmbr_bpjs_ppk1_desc, mmbr_status,
             mmbr_bpjs_status, mmbr_departement,
             mmbr_bpjs_premi, mmbr_terminate_date';
    
    return $this->one($no . '_mst_member', 'WHERE mmbr_member_no = "' . $member . '" AND mmbr_card_nomor = "' . $card . '"', $cols . ' ' . $insql);
  }

  function limit($no, $id, $member){
    $cols  =  'plan.prdplan_ben_type,
               plan.prdplan_ben_code,
               plan.prdplan_category,
               plan.prdplan_benefit_amount,
               plan.prdplan_frequently,
               plan.prdplan_case_disability,
               plan.prdplan_covered,
               pro.prdclass_benefit_plan,
               pro.prdclass_plan_name,
               pro.prdclass_benefit_type';

    $cols .=  ', (SELECT COUNT(benfac_benefit_amount) FROM policy_benefit_family_class WHERE benfac_pol_no = ' . $no . ' AND benfac_benefit_class = pro.prdclass_plan_name AND benfac_benefit_type = pro.prdclass_benefit_type) benfac_benefit_amount';
    $cols .=  ', (SELECT GROUP_CONCAT(pol_family_benefit) FROM policy WHERE pol_pol_no = "' . $no . '") pol_family_benefit';
    $cols .=  ', (SELECT COUNT(mmbr_member_no) FROM ' . $no . '_mst_member WHERE mmbr_member_no LIKE "%' . substr($member, 0, 7) . '%") family';


    return $this->read($no . '_product_class pro', 'JOIN ' . $no . '_product_benefit_plan plan ON pro.prdclass_benefit_plan = plan.prdplan_plan_code WHERE pro.prdclass_class_code = "' . $id . '"', $cols);
  }

  function code($where = null){
    return $this->read('mst_code', $where, 'mco_mastercode vals, mco_description label');
  }

  function custom($query){
    return $this->Dml_model->custom($query);
  }

  function procedure($procedure){
    return $this->Dml_model->procedure($procedure);
  }

  function one($table, $condition = null, $fields = "*"){
    return $this->Dml_model->one($table, $condition, $fields);
  }

  function create_batch($table, $data){
    return $this->Dml_model->create_batch($table, $data);
  }

  function create($table, $data = null){
    return $this->Dml_model->create($table, $data);
  }

  function read($table, $condition = null, $fields = "*"){
    return $this->Dml_model->read($table, $condition, $fields);
  }

  function update($table, $where, $data = null){
    return $this->Dml_model->update($table, $where, $data);
  }

  function delete($table, $where){
    return $this->Dml_model->delete($table, $where);
  }

  function upload($data, $url = null, $rename = null){
    return $this->Dml_model->upload($data, $url, $rename);
  }

}