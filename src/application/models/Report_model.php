<?php
// https://www.teletopiasms.no/np/frontpage/gateway/api-http-examples-php
// https://stackoverflow.com/questions/5647461/how-do-i-send-a-post-request-with-php
class Report_model extends CI_Model {

  public function __construct() {
    parent::__construct();
    $this->load->database();
    $this->load->model('Dml_model');
  }

  function  graph($no){
    return $this->one('policy', 'WHERE pol_pol_no = "' . $no . '"', 'pol_dedicated_account, pol_dedicated_account_proyeksi, pol_dedicated_account_balance, pol_dedicated_account_status_proyeksi, pol_dedicated_account_status_realisasi, pol_currency, pol_dedicated_account_normal');
  }

  function benefit($no){
    $table = $no . '_policy_benefit_type';
    $cols = 'DISTINCT ' . $table . '.polbt_benefit_type vals, mst_code.mco_description label';
    $join = 'JOIN mst_code ON ' . $table . '.polbt_benefit_type = mst_code.mco_mastercode';
    $where = 'WHERE mst_code.mco_status = "1" AND mst_code.mco_cat_id = "7"';
    return $this->read($table, $join . ' ' . $where, $cols);
  }

  function provider($no){
    $table = $no . '_claim';
    return $this->read($table, '', 'DISTINCT clm_provider_code vals, clm_provider_name label');
  }

  function claims($no, $start, $end){
    $table = $no . '_claim';
    $cols = 'SUM(clm_proposed_amount) proposed, SUM(clm_approved_amount) approved, SUM(clm_excess_amount) excess, COUNT(clm_claim_code) code';
    $where = 'WHERE clm_claim_date BETWEEN "' . $start . '" AND "' . $end . '"';

    return $this->one($table, $where, $cols);
  }

  function claim($no, $start, $end){
    $table = $no . '_policy_benefit_type';
    $cols = 'DISTINCT ' . $table . '.polbt_benefit_type vals, mst_code.mco_description label';
    $join = 'JOIN mst_code ON ' . $table . '.polbt_benefit_type = mst_code.mco_mastercode';
    $where = 'WHERE mst_code.mco_cat_id = "7" AND mst_code.mco_status = "1"';

    $benefit = $this->read($table, $join . ' ' . $where, $cols);

    $vals = null;
    foreach ($benefit as $key => $val) {
      $vals[] = $val['vals'];
    }

    $table = $no . '_claim';
    $cols = 'SUM(clm_proposed_amount) proposed, SUM(clm_approved_amount) approved, SUM(clm_excess_amount) excess, COUNT(clm_claim_code) code, clm_benefit_type parent';
    $where = 'WHERE clm_benefit_type IN ("' . implode('", "', $vals) . '") AND clm_claim_date BETWEEN "' . $start . '" AND "' . $end . '"';

    $claim = $this->read($no . '_claim', $where , $cols);

    foreach ($benefit as &$arr1) {
      $offer_id = $arr1['vals'];

      $match = array_filter($claim, function($v) use ($offer_id){
        return $v['parent'] == $offer_id;
      });

      $match = array_pop($match);
      $arr1['subval'] = $match;
    }
    return $benefit;
  }

  function group($no, $start, $end, $benefit = ''){
    $table = $no . '_claim';
    $jtable = $no . '_mst_member';
    $cols = 'CONCAT(mmbr_enrol_code, mmbr_sex) person, SUM(clm_proposed_amount) proposed, SUM(clm_approved_amount) approved, SUM(clm_excess_amount) excess, COUNT(clm_claim_code) code';
    $join = 'JOIN ' . $jtable . ' ON clm_member_no = mmbr_member_no';
    $where = 'WHERE mmbr_enrol_code IN ("EMP", "SPO", "CHL") AND mmbr_sex IN ("F", "M") AND clm_claim_date BETWEEN "' . $start . '" AND "' . $end . '"';
    $group = 'GROUP BY CONCAT(mmbr_enrol_code, mmbr_sex)';
    $member = ['EMPM' => 'Karyawan', 'EMPF' => 'Karyawati', 'SPOM' => 'Istri', 'SPOF' => 'Suami', 'CHLM' => 'Putra', 'CHLF' => 'Putri'];

    $benefit = (empty($benefit)) ? '' : 'AND clm_benefit_type = "' . $benefit . '"' ;

    $data = $this->read($table, $join . ' ' . $where . ' ' . $benefit . ' ' . $group, $cols);
    foreach ($data as $key => &$val) {
      $val['person'] = $member[$val['person']];
    }

    return $data;
  }

  function highest($no, $start, $end, $benefit = ''){
    $table = $no . '_claim';
    $jtable = $no . '_mst_member';
    $cols = 'mmbr_employee_id, clm_member_no, mmbr_name, mmbr_enrol_code, SUM(clm_proposed_amount) as propose, SUM(clm_approved_amount) as approve';
    $join = 'JOIN ' . $jtable . ' ON clm_member_no = mmbr_member_no';
    $where = 'WHERE clm_claim_date BETWEEN "' . $start . '" AND "' . $end . '"';
    $group = 'GROUP BY mmbr_employee_id, clm_member_no, mmbr_name, mmbr_enrol_code';
    $order = 'ORDER BY propose DESC';

    $insql = '(SELECT member.mmbr_name FROM ' . $jtable . ' AS member WHERE member.mmbr_pol_no = "' . $no . '" AND member.mmbr_enrol_code = "EMP" AND member.mmbr_employee_id = ' . $jtable . '.mmbr_employee_id) as parent';

    $benefit = (empty($benefit)) ? '' : 'AND clm_benefit_type = "' . $benefit . '"' ;

    return $this->read($table, $join . ' ' . $where . ' ' . $benefit . ' ' . $group . ' ' . $order, $cols . ', ' . $insql);
  }

  function deseases($no, $start, $end, $benefit = ''){
    $table = $no . '_claim';
    $cols = 'clm_icd1, micd_description, COUNT(clm_claim_code) code, SUM(clm_proposed_amount) as propose, SUM(clm_approved_amount) as approve';
    $join = 'JOIN mst_icd ON clm_icd1 = micd_code';
    $where = 'WHERE clm_claim_date BETWEEN "' . $start . '" AND "' . $end . '"';
    $group = 'GROUP BY clm_icd1, micd_description ORDER BY propose DESC';

    $benefit = (empty($benefit)) ? '' : 'AND clm_benefit_type = "' . $benefit . '"' ;

    return $this->read($table, $join . ' ' . $where . ' ' . $benefit . ' ' . $group, $cols);
  }


  function visit($no, $start, $end, $benefit = '', $provider = ''){
    $table = $no . '_claim';
    $cols = 'clm_provider_code, clm_provider_name, COUNT(clm_claim_code) code, SUM(clm_proposed_amount) as propose, Sum(clm_approved_amount) as approve';
    $where = 'WHERE clm_claim_code <> "" AND clm_claim_date BETWEEN "' . $start . '" AND "' . $end . '"';
    $order = 'GROUP BY clm_provider_code, clm_provider_name ORDER BY propose DESC ';

    $benefit = (empty($benefit)) ? '' : 'AND clm_benefit_type = "' . $benefit . '"' ;
    $provider = (empty($provider)) ? '' : 'AND clm_provider_code = "' . $provider . '"' ;

    return $this->read($table, $where . ' ' . $benefit . ' ' . $provider . ' ' . $order, $cols);
  }

  function distribution(){
    return $this->read('mst_channel_distribution', 'ORDER BY mcd_channel_name ASC', 'mcd_id vals, mcd_channel_name label');
  }

  function agent($code){
    return $this->read('mst_agent', 'WHERE mag_channel_distribution = "' . $code . '" ORDER BY mag_agent_name ASC', 'mag_agent_code vals, mag_agent_name label');
  }

  function remu(){
    $cols  = 'DISTINCT mag_agent_code, mag_agent_name, pol_channel_dist, mcd_channel_name, pol_fee_tpa, pol_fee_card, mcd_commision, mcd_supporting_status, pol_pol_no';
    $cols .= ', (SELECT SUM(pol_member) FROM policy pol WHERE pol_status = "IN" AND pol.pol_channel_dist = policy.pol_channel_dist AND pol_agent = mag_agent_code) sum';
    $cols .= ', (SELECT COUNT(pol_pol_no) FROM policy pol WHERE pol_status = "IN" AND pol.pol_channel_dist = policy.pol_channel_dist AND pol_agent = mag_agent_code) count';
    return $cols;
  }

  function prod(){
    $cols = 'pol_pol_no, pol_company_name, pol_start_date, pol_end_date, mcd_channel_name, mag_agent_name, pol_fee_tpa, pol_fee_card';
    return $cols;
  }

  function reports($report = 'remunerasi', $start, $end, $distribution = '', $agent = ''){
    $no = null;
    $table = 'policy';
    $cols = ($report == 'remunerasi') ? $this->remu() : $this->prod() ;
    $join = 'JOIN mst_channel_distribution ON pol_channel_dist = mcd_id
             JOIN mst_agent ON mag_channel_distribution = mcd_id AND pol_agent = mag_agent_code AND pol_channel_dist = mag_channel_distribution';
    $where = 'WHERE pol_status = "IN" AND pol_start_date >= "' . $start . '" AND pol_end_date <= "' . $end . '"';
    $where .= (empty($distribution)) ? '' : ' AND mag_channel_distribution = "' .$distribution. '"';
    $where .= (empty($agent)) ? '' : ' AND pol_agent = "' .$agent. '"';

    $data = $this->read($table, $join . ' ' . $where, $cols);

    foreach ($data as $key => $val) {
      $no[] = $val['pol_pol_no'];
    }

    $counter = ($report == 'remunerasi') ? $this->remucounter($no) : $this->prodcounter($no);

    foreach ($data as $key => &$val) {
      if ($report == 'remunerasi') {
        $val['counter'] = $counter[$val['pol_pol_no']];
      } else {
        $val['null'] = $counter['null'][$val['pol_pol_no']];
        $val['code'] = $counter['code'][$val['pol_pol_no']];
      }
    }

    return $data;

  }

  function prodcounter($data){
    $datas[] = null;

    if(!empty($data)){
      foreach ($data as $key => $val) {
        $tbl = $val . '_mst_member';
        $cols = 'COUNT(' . $val . '_mst_member.mmbr_member_no) AS `' . $val . '`';
        $where = $val . '_mst_member.mmbr_payment_code != "" ';

        $null = $this->one($tbl, 'WHERE ' . $where, $cols);
        $code = $this->one($tbl,'', $cols);

        $datas['null'][$val] = $null[$val];
        $datas['code'][$val] = $code[$val];
      }
    return $datas;
    } else {
      return null;
    }
  }

  function remucounter($data){
    $tbl = null;
    $cols = null;
    $where = null;

    if(!empty($data)){
      foreach ($data as $key => $val) {
        $tbl[] = $val . '_mst_member';
        $cols[] = 'COUNT(' . $val . '_mst_member.mmbr_member_no) AS `' . $val . '`';
        $where[] = $val . '_mst_member.mmbr_payment_code != "" ';
      }

      $tbl = implode(', ', $tbl);
      $cols = implode(', ', $cols);
      $where = implode(' AND ', $where);

      return $this->one($tbl, 'WHERE ' . $where, $cols);
    } else {
      return null;
    }
  }

  function procedure($procedure){
    return $this->Dml_model->procedure($procedure);
  }

  function one($table, $condition = null, $fields = "*"){
    return $this->Dml_model->one($table, $condition, $fields);
  }

  function create_batch($table, $data){
    return $this->Dml_model->create_batch($table, $data);
  }

  function create($table, $data = null){
    return $this->Dml_model->create($table, $data);
  }

  function read($table, $condition = null, $fields = "*"){
    return $this->Dml_model->read($table, $condition, $fields);
  }

  function update($table, $where, $data = null){
    return $this->Dml_model->update($table, $where, $data);
  }

  function delete($table, $where){
    return $this->Dml_model->delete($table, $where);
  }

  function upload($data, $url = null, $rename = null){
    return $this->Dml_model->upload($data, $url, $rename);
  }

}