<?php
// https://www.teletopiasms.no/np/frontpage/gateway/api-http-examples-php
// https://stackoverflow.com/questions/5647461/how-do-i-send-a-post-request-with-php
class Provider_model extends CI_Model {

  public function __construct() {
    parent::__construct();
    $this->load->database();
    $this->load->model('Dml_model');
  }

  function group(){
    $cols = 'mpg_provider_induk, mpg_status,
             mpg_group_name, mpg_pks_date,
             mpg_expired_pks, mpg_pks_no,
             mpg_pks_no_provider, mpg_id,
             mpg_attach_pks, mprov_name';

    return $this->read('mst_provider_group', 'LEFT JOIN mst_provider ON mpg_provider_induk = mprov_prov_code WHERE mpg_active = "1"', $cols);
  }

  function index(){
    $cols = 'mprov_status, mco_description,
             mprov_province, mprov_prov_code,
             mpg_group_name, mprov_telephone,
             mprov_group, mprov_layanan_system,
             mprov_city, mprov_bpjs, mprov_name';

    return $this->read('mst_provider', 'JOIN mst_code ON mprov_type = mco_mastercode LEFT JOIN mst_provider_group ON mpg_id = mprov_group WHERE mco_status = "1" AND mprov_prov_code NOT IN("PSSA-88888", "PSSA-99999") AND mco_cat_id = "14"', $cols);
  }

  function custom($query){
    return $this->Dml_model->custom($query);
  }

  function procedure($procedure){
    return $this->Dml_model->procedure($procedure);
  }

  function creade($create, $read, $condition, $data){
    return $this->Dml_model->creade($create, $read, $condition, $data);
  }

  function one($table, $condition = null, $fields = "*"){
    return $this->Dml_model->one($table, $condition, $fields);
  }

  function create_batch($table, $data){
    return $this->Dml_model->create_batch($table, $data);
  }

  function create($table, $data = null){
    return $this->Dml_model->create($table, $data);
  }

  function read($table, $condition = null, $fields = "*"){
    return $this->Dml_model->read($table, $condition, $fields);
  }

  function update($table, $where, $data = null){
    return $this->Dml_model->update($table, $where, $data);
  }

  function delete($table, $where){
    return $this->Dml_model->delete($table, $where);
  }

  function upload($data, $url = null, $rename = null){
    return $this->Dml_model->upload($data, $url, $rename);
  }

}