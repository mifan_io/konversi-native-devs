<div class="content">
  <div class="content">
    <div class="container-fluid">

      <div class="row">
        <div class="col-md-12">
          <div class="card ">

            <div class="card-header card-header-success card-header-icon">
              <div class="card-icon">
                <i class="material-icons">timeline</i>
              </div>
              <h4 class="card-title">Deposit Fund</h4>
            </div>

            <div class="card-body ">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">Periode Polis (<span id="periode"></span>)</div>
                  <div class="form-group">
                    <div class="input-group no-border">
                      <input type="hidden" id="val">
                      <input type="text" id="no" class="form-control" placeholder="Nomor Polis" readonly="readonly">
                      <button onclick="large('noticeModal');" type="button" data-toggle="modal" class="btn btn-white btn-round btn-just-icon" data-target="#noticeModal">
                        <i class="material-icons">search</i>
                        <div class="ripple-container"></div>
                      </button>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6">
                  <h4 class="title">Balance Account Proyeksi <span id="Proyeksi-lbl"></span></h4>
                  <div class="progress progress-line-primary">
                    <div class="progress-bar progress-bar-primary" id="Proyeksi" role="progressbar" aria-valuenow="60"
                      aria-valuemin="0" aria-valuemax="100" style="width: 30%;">
                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <h4 class="title">Balance Account Realisasi <span id="Realisasi-lbl"></span></h4>
                  <div class="progress progress-line-rose">
                    <div class="progress-bar progress-bar-rose" id="Realisasi" role="progressbar" aria-valuenow="60"
                      aria-valuemin="0" aria-valuemax="100" style="width: 30%; background-color: #e91e63;">
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6">
                  <div class="card card-chart">
                    <div class="card-header card-header-icon card-header-primary">
                      <div class="card-icon">
                        <i class="material-icons">pie_chart</i>
                      </div>
                      <h4 class="card-title">Graph Balance Amount | Proyeksi</h4>
                    </div>
                    <div class="card-body">
                      <div id="Proyeksi-pie" class="ct-chart"></div>
                    </div>
                    <div class="card-footer">
                      <div class="row">
                        <div class="col-md-12">
                          <h6 class="card-category">Legend</h6>
                        </div>
                        <div class="col-md-12">
                          <i class="fa fa-circle text-warning"></i> Outstanding Payment
                          <i class="fa fa-circle text-danger"></i> Usage
                          <i class="fa fa-circle text-info"></i> Balance
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="card card-chart">
                    <div class="card-header card-header-icon card-header-rose">
                      <div class="card-icon">
                        <i class="material-icons">pie_chart</i>
                      </div>
                      <h4 class="card-title">Graph Balance Amount | Realisasi</h4>
                    </div>
                    <div class="card-body">
                      <div id="Realisasi-pie" class="ct-chart"></div>
                    </div>
                    <div class="card-footer">
                      <div class="row">
                        <div class="col-md-12">
                          <h6 class="card-category">Legend</h6>
                        </div>
                        <div class="col-md-12">
                          <i class="fa fa-circle text-danger"></i> Usage
                          <i class="fa fa-circle text-info"></i> Balance
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>

    </div>
  </div>
</div>


<!-- notice modal -->
<div class="modal fade bs-example-modal-lg" id="noticeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="padding-right: 50%">
  <div class="modal-dialog modal-notice modal-lg" style="width: 80%;">
    <div class="modal-content" style="width: 150%;">
      <div class="modal-header">
        <h5 class="modal-title" id="myModalLabel">Inquery Data Policy</h5>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
          <i class="material-icons">close</i>
        </button>
      </div>
      <div class="modal-body">
        <div class="instruction">
          <div class="row">
            <div class="col-md-12">
              <div class="material-datatables">
                <table class="table table-striped table-no-bordered table-hover datatables" cellspacing="0" width="100%" style="width:100%">
                  <thead>
                    <tr>
                      <th class="text-center"><i class="fa fa-hashtag"></i></th>
                      <th class="text-center">No. Polis</th>
                      <th class="text-center">Policy Name</th>
                      <th class="text-center">Start Date</th>
                      <th class="text-center">End Date</th>
                      <th class="text-center">Member(s)</th>
                      <th class="text-center">Status</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php foreach ($data as $key => $val) { ?>
                    <tr>
                      <td><?php echo ($key + 1); ?></td>
                      <td>
                        <a data-dismiss="modal" href="#" onclick="display('.benefit-option'); binding('#benefit<?php echo $key; ?>'); graph('report/graph', '<?php echo $val['pol_pol_no']; ?>');">
                          <?php echo $val['pol_pol_no']; ?>
                          </a>
                      </td>
                      <td><?php echo $val['pol_company_name']; ?></td>
                      <td><?php echo $val['pol_start_date']; ?></td>
                      <td><?php echo $val['pol_end_date']; ?></td>
                      <td class="text-right"><?php echo $val['pol_member']; ?></td>
                      <td>
                        <?php echo $val['pol_status']; ?>
                        <input type="hidden" id="benefit<?php echo $key; ?>" value='<?php echo json_encode($val); ?>'>
                      </td>
                    </tr>
                  <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end notice modal -->