<div class="content">
  <div class="content">
    <div class="container-fluid">

      <div class="row">
        <div class="col-md-12">
          <div class="card ">

            <div class="card-header card-header-info card-header-icon">
              <div class="card-icon">
                <i class="material-icons">assignment</i>
              </div>
              <h4 class="card-title">Report <?php echo ucfirst($report);?></h4>
            </div>

            <div class="card-body ">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="input-group form-control-md">
                      <div class="form-group bmd-form-group">
                        <label for="Start" class="bmd-label-floating">START REPORT</label>
                        <input type="text" class="form-control datestart" id="Start" required="required" onchange="remunerasi('report/reports', '<?php echo $report; ?>', this.value, End.value, distribution.value, agent.value);">
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="input-group form-control-md">
                      <div class="form-group bmd-form-group">
                        <label for="End" class="bmd-label-floating">END REPORT</label>
                        <input type="text" class="form-control dateend" id="End" required="required" onchange="remunerasi('report/reports', '<?php echo $report; ?>', Start.value, this.value, distribution.value, agent.value);">
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <div class="input-group no-border">
                      <select class="selectpicker" id="distribution" onchange="chainedNull('report/agent', this.value, '#agent'); remunerasi('report/reports', '<?php echo $report; ?>', Start.value, End.value, this.value, agent.value);" data-style="select-with-transition" title="Choose Distribution" data-size="7" required="required">
                        <option value="">All Distribution</option>
                      <?php foreach ($distribution as $key => $val) { ?>
                        <option value="<?php echo $val['vals']; ?>"><?php echo $val['label']; ?></option>
                      <?php } ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="input-group no-border">
                      <select class="selectpicker" id="agent" onchange="remunerasi('report/reports', '<?php echo $report; ?>', Start.value, End.value, distribution.value, this.value);" data-style="select-with-transition" title="Choose Agent" data-size="7" required="required"></select>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="material-datatables">
                    <table class="table table-striped table-hover" cellspacing="0" width="100%" style="width:100%">
                      <thead>
                        <tr>
                          <th><i class="fa fa-hashtag"></i></th>
                          <?php foreach ($cols as $key => $val) { ?>
                          <th class="text-center"><?php echo $val; ?></th>
                          <?php } ?>
                        </tr>
                      </thead>
                      <tbody id="reports"></tbody>
                      <tfoot id="freports"></tfoot>
                    </table>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>

    </div>
  </div>
</div>


<!-- notice modal -->
<div class="modal fade bs-example-modal-lg" id="noticeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="padding-right: 50%">
  <div class="modal-dialog modal-notice modal-lg" style="width: 80%;">
    <div class="modal-content" style="width: 150%;">
      <div class="modal-header">
        <h5 class="modal-title" id="myModalLabel">Inquery Data Policy</h5>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
          <i class="material-icons">close</i>
        </button>
      </div>
      <div class="modal-body">
        <div class="instruction">
          <div class="row">
            <div class="col-md-12">
              <div class="material-datatables">
                <table class="table table-striped table-no-bordered table-hover datatables" cellspacing="0" width="100%" style="width:100%">
                  <thead>
                    <tr>
                      <th class="text-center"><i class="fa fa-hashtag"></i></th>
                      <th class="text-center">No. Polis</th>
                      <th class="text-center">Policy Name</th>
                      <th class="text-center">Start Date</th>
                      <th class="text-center">End Date</th>
                      <th class="text-center">Member(s)</th>
                      <th class="text-center">Status</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php foreach ($data as $key => $val) { ?>
                    <tr>
                      <td><?php echo ($key + 1); ?></td>
                      <td>
                        <a data-dismiss="modal" href="#" onclick="display('.benefit-option'); binding('#benefit<?php echo $key; ?>'); _reportAll('report/data', '<?php echo $val['pol_pol_no']; ?>', Start.value, End.value, benefit.value, provider.value); chainedNull('report/provider', '<?php echo $val['pol_pol_no']; ?>', '#provider'); chainedNull('report/benefit', '<?php echo $val['pol_pol_no']; ?>', '#benefit');">
                          <?php echo $val['pol_pol_no']; ?>
                          </a>
                      </td>
                      <td><?php echo $val['pol_company_name']; ?></td>
                      <td><?php echo $val['pol_start_date']; ?></td>
                      <td><?php echo $val['pol_end_date']; ?></td>
                      <td class="text-right"><?php echo $val['pol_member']; ?></td>
                      <td>
                        <?php echo $val['pol_status']; ?>
                        <input type="hidden" id="benefit<?php echo $key; ?>" value='<?php echo json_encode($val); ?>'>
                      </td>
                    </tr>
                  <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end notice modal -->