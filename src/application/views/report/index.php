<div class="content">
  <div class="content">
    <div class="container-fluid">

      <div class="row">
        <div class="col-md-12">
          <div class="card ">

            <div class="card-header card-header-tabs card-header-primary">
              <div class="nav-tabs-navigation">
                <div class="nav-tabs-wrapper">
                  <ul class="nav nav-tabs" data-tabs="tabs">

                  <?php $i = 0 ; foreach ($tab as $key => $val) { ?>
                    <li class="nav-item">
                      <a class="nav-link <?php echo ($i == 0) ? 'active' : '' ; ?>" href="#link<?php echo $i; ?>" data-toggle="tab">
                        <i class="material-icons"><?php echo $logo[$i]; ?></i> <?php echo $val; ?>
                        <div class="ripple-container"></div>
                      </a>
                    </li>
                  <?php $i++; } ?>

                  </ul>
                </div>
              </div>
            </div>

            <div class="card-body ">
              <div class="tab-content tab-space">

                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">Periode Polis (<span id="periode"></span>)</div>
                    <div class="form-group">
                      <div class="input-group no-border">
                        <input type="hidden" id="val">
                        <input type="text" id="no" class="form-control" placeholder="Nomor Polis" readonly="readonly">
                        <button onclick="large('noticeModal');" type="button" data-toggle="modal" class="btn btn-white btn-round btn-just-icon" data-target="#noticeModal">
                          <i class="material-icons">search</i>
                          <div class="ripple-container"></div>
                        </button>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="Start" class="bmd-label-floating">START REPORT</label>
                          <input type="text" class="form-control datestart" id="Start" required="required" onchange="_reportAll('report/data', val.value, this.value, End.value, benefit.value, provider.value);">
                        </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="End" class="bmd-label-floating">END REPORT</label>
                          <input type="text" class="form-control dateend" id="End" required="required" onchange="_reportAll('report/data', val.value, Start.value, this.value, benefit.value, provider.value);">
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <div class="input-group no-border">
                        <select class="selectpicker" id="benefit" onchange="_reportAll('report/data', val.value, Start.value, End.value, this.value, provider.value);" data-style="select-with-transition" title="Choose Benefit" data-size="7" required="required"></select>
                      </div>
                    </div>

                    <div class="form-group">
                      <div class="input-group no-border">
                        <select class="selectpicker" id="provider" onchange="_reportAll('report/data', val.value, Start.value, End.value, benefit.value, this.value);" data-style="select-with-transition" title="Choose Provider" data-size="7" required="required"></select>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="tab-pane active" id="link0">
                  <div class="material-datatables">
                    <table class="table table-striped table-no-bordered table-hover" id="claimExcel" cellspacing="0" width="100%" style="width:100%">
                      <thead>
                        <tr>
                          <th class="text-center"><a href="#" onclick="tableToExcel('claimExcel', 'Benefit Claim');"><i class="text-success fa fa-file-excel-o"></i></a></th>
                          <th class="text-center">Benefit Type</th>
                          <th class="text-center">Proposed Claim</th>
                          <th class="text-center">Approved Claim</th>
                          <th class="text-center">Excess Claim</th>
                          <th class="text-center">% Approve Claim</th>
                          <th class="text-center">% Excess Claim</th>
                          <th class="text-center"># Case</th>
                          <th class="text-center">% Approve Total</th>
                          <th class="text-center">% Case Total</th>
                        </tr>
                      </thead>
                      <tbody id="claim"></tbody>
                      <tfoot id="fclaim"></tfoot>
                    </table>
                  </div>
                </div>

                <div class="tab-pane" id="link1">
                  <div class="material-datatables">
                    <table class="table table-striped table-no-bordered table-hover" id="groupExcel" cellspacing="0" width="100%" style="width:100%">
                      <thead>
                        <tr>
                          <th class="text-center"><a href="#" onclick="tableToExcel('groupExcel', 'Member Group');"><i class="text-success fa fa-file-excel-o"></i></a></th>
                          <th class="text-center">Member Enroll</th>
                          <th class="text-center">Proposed Claim</th>
                          <th class="text-center">Approved Claim</th>
                          <th class="text-center"># Case</th>
                          <th class="text-center">% Approve Claim</th>
                          <th class="text-center">% Case</th>
                        </tr>
                      </thead>
                      <tbody id="group"></tbody>
                      <tfoot id="fgroup"></tfoot>
                    </table>
                  </div>
                </div>

                <div class="tab-pane" id="link2">
                  <div class="material-datatables">
                    <table class="table table-striped table-no-bordered table-hover" id="highestExcel" cellspacing="0" width="100%" style="width:100%">
                      <thead>
                        <tr>
                          <th class="text-center"><a href="#" onclick="tableToExcel('highestExcel', '20 Highest Claim');"><i class="text-success fa fa-file-excel-o"></i></a></th>
                          <th class="text-center">NIK</th>
                          <th class="text-center">Employee Name</th>
                          <th class="text-center">Member Name</th>
                          <th class="text-center">Enrollment</th>
                          <th class="text-center">Proposed Claim</th>
                          <th class="text-center">Approved Claim</th>
                        </tr>
                      </thead>
                      <tbody id="highest"></tbody>
                      <tfoot id="fhighest"></tfoot>
                    </table>
                  </div>
                </div>

                <div class="tab-pane" id="link3">
                  <div class="material-datatables">
                    <table class="table table-striped table-no-bordered table-hover" id="deseasesExcel" cellspacing="0" width="100%" style="width:100%">
                      <thead>
                        <tr>
                          <th class="text-center"><a href="#" onclick="tableToExcel('deseasesExcel', 'Common Diseases');"><i class="text-success fa fa-file-excel-o"></i></a></th>
                          <th class="text-center">ICD Code</th>
                          <th class="text-center">ICD Description</th>
                          <th class="text-center"># Cases</th>
                          <th class="text-center">Proposed Claim</th>
                          <th class="text-center">Approved Claim</th> 
                        </tr>
                      </thead>
                      <tbody id="deseases"></tbody>
                      <tfoot id="fdeseases"></tfoot>
                    </table>
                  </div>
                </div>

                <div class="tab-pane" id="link4">
                  <div class="material-datatables">
                    <table class="table table-striped table-no-bordered table-hover" id="visitExcel" cellspacing="0" width="100%" style="width:100%">
                      <thead>
                        <tr>
                          <th class="text-center"><a href="#" onclick="tableToExcel('visitExcel', '20 Highest Visit');"><i class="text-success fa fa-file-excel-o"></i></a></th>
                          <th class="text-center">Prov Code</th>
                          <th class="text-center">Prov Name</th>
                          <th class="text-center"># Cases</th>
                          <th class="text-center">Proposed Claim</th>
                          <th class="text-center">Approved Claim</th>
                        </tr>
                      </thead>
                      <tbody id="visit"></tbody>
                      <tfoot id="fvisit"></tfoot>
                    </table>
                  </div>
                </div>

              </div>
            </div>

          </div>
        </div>
      </div>

    </div>
  </div>
</div>


<!-- notice modal -->
<div class="modal fade bs-example-modal-lg" id="noticeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="padding-right: 50%">
  <div class="modal-dialog modal-notice modal-lg" style="width: 80%;">
    <div class="modal-content" style="width: 150%;">
      <div class="modal-header">
        <h5 class="modal-title" id="myModalLabel">Inquery Data Policy</h5>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
          <i class="material-icons">close</i>
        </button>
      </div>
      <div class="modal-body">
        <div class="instruction">
          <div class="row">
            <div class="col-md-12">
              <div class="material-datatables">
                <table class="table table-striped table-no-bordered table-hover datatables" cellspacing="0" width="100%" style="width:100%">
                  <thead>
                    <tr>
                      <th class="text-center"><i class="fa fa-hashtag"></i></th>
                      <th class="text-center">No. Polis</th>
                      <th class="text-center">Policy Name</th>
                      <th class="text-center">Start Date</th>
                      <th class="text-center">End Date</th>
                      <th class="text-center">Member(s)</th>
                      <th class="text-center">Status</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php foreach ($data as $key => $val) { ?>
                    <tr>
                      <td><?php echo ($key + 1); ?></td>
                      <td>
                        <a data-dismiss="modal" href="#" onclick="display('.benefit-option'); binding('#benefit<?php echo $key; ?>'); _reportAll('report/data', '<?php echo $val['pol_pol_no']; ?>', Start.value, End.value, benefit.value, provider.value); chainedNull('report/provider', '<?php echo $val['pol_pol_no']; ?>', '#provider'); chainedNull('report/benefit', '<?php echo $val['pol_pol_no']; ?>', '#benefit');">
                          <?php echo $val['pol_pol_no']; ?>
                          </a>
                      </td>
                      <td><?php echo $val['pol_company_name']; ?></td>
                      <td><?php echo $val['pol_start_date']; ?></td>
                      <td><?php echo $val['pol_end_date']; ?></td>
                      <td class="text-right"><?php echo $val['pol_member']; ?></td>
                      <td>
                        <?php echo $val['pol_status']; ?>
                        <input type="hidden" id="benefit<?php echo $key; ?>" value='<?php echo json_encode($val); ?>'>
                      </td>
                    </tr>
                  <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end notice modal -->