<div class="content">
  <div class="content">
    <div class="container-fluid">

      <?php if (isset($_SESSION['users_input']) && $_SESSION['users_input'] == true) { ?>
      <div class="alert alert-<?php echo $_SESSION['users_status']; ?>">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <i class="material-icons">close</i>
        </button>
        <span><b class="text-capitalize"> <?php echo $_SESSION['users_status']; ?> - </b> <?php echo $_SESSION['users_text']; ?></span>
      </div>
      <?php } unset($_SESSION['users_input']); unset($_SESSION['users_status']); unset($_SESSION['users_text']); ?>

      <div class="row">
        <div class="col-md-12">
          <div class="card ">

            <div class="card-header card-header-tabs card-header-info">
              <div class="nav-tabs-navigation">
                <div class="nav-tabs-wrapper">
                  <ul class="nav nav-tabs" data-tabs="tabs">
                   <li class="nav-item">
                      <a class="nav-link active" href="#link0" data-toggle="tab">
                        <i class="material-icons">chrome_reader_mode</i> Data Approval Claim<div class="ripple-container"></div>
                      </a>
                    </li>

                    <li class="nav-item">
                      <a class="nav-link " href="#link1" data-toggle="tab">
                        <i class="material-icons">vertical_split</i> Approval Claim | Provider<div class="ripple-container"></div>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>

            <div class="card-body ">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">Periode Polis (<span id="periode"></span>)</div>
                  <div class="form-group">
                    <div class="input-group no-border">
                      <input type="text" id="no" class="form-control" placeholder="Nomor Polis" readonly="readonly">
                      <button type="button" data-toggle="modal" class="btn btn-white btn-round btn-just-icon" data-target="#noticeModal" onclick="large('noticeModal');">
                        <i class="material-icons">search</i>
                        <div class="ripple-container"></div>
                      </button>
                    </div>
                  </div>

                  <div class="form-group benefit-option" style="display: none;">
                    <div class="input-group no-border">
                      <input type="text" id="nik" class="form-control" placeholder="Karyawan" readonly="readonly">
                      <button type="button" data-toggle="modal" class="btn btn-white btn-round btn-just-icon" data-target="#Mkaryawan" onclick="large('Mkaryawan');">
                        <i class="material-icons">search</i>
                        <div class="ripple-container"></div>
                      </button>
                    </div>
                  </div>
                </div>
              </div>

              <div class="tab-content tab-space">
                <div class="tab-pane" id="link0">
                  <form method="post" class="form-horizontal">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="material-datatables">
                          <table class="table table-striped table-hover" cellspacing="0" width="100%" style="width:100%">
                            <thead>
                              <tr>
                                <th><i class="fa fa-hashtag"></i></th>
                                <th>KD Claim</th>
                                <th>Member No</th>
                                <th>Member Name</th>
                                <th>Claim Date</th>
                                <th>Ben Type</th>
                                <th>Serv IN</th>
                                <th>Serv OUT</th>
                                <th>Proposed</th>
                                <th>Approved</th>
                                <th>
                                  <div class="form-check form-check-inline">
                                    <label class="form-check-label">
                                      <input class="form-check-input" type="checkbox" id="nyeck" onclick="checkall(this, '.nyecker');">
                                      <span class="form-check-sign">
                                        <span class="check"></span>
                                      </span>
                                    </label>
                                  </div>
                                </th>
                              </tr>
                            </thead>
                            <tbody id="Claim"></tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                    <div class="row text-right">
                      <div class="col-md-9">&nbsp;</div>
                      <div class="col-md-3">
                        <input type="hidden" id="val" class="form-control" name="mbs_policy_no">
                        <input type="hidden" id="val2" class="form-control">
                        <input type="hidden" id="val3" class="form-control">
                        <input type="hidden" id="code" class="form-control">
                        <input type="hidden" id="_card" class="form-control">
                        <input type="hidden" id="_member" class="form-control">
                        <button type="submit" class="btn btn-fill btn-primary submit" disabled="disabled">Submit</button>
                      </div>
                    </div>
                  </form>
                </div>

                <div class="tab-pane" id="link1">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="material-datatables">
                        <table class="table table-striped table-hover datatables" cellspacing="0" width="100%" style="width:100%">
                          <thead>
                            <tr>
                              <th><i class="fa fa-hashtag"></i></th>
                              <th>No. Reff</th>
                              <th>Provider Name</th>
                              <th>Policy</th>
                              <th>Member Name</th>
                              <th>Serv IN</th>
                              <th>Benefit</th>
                              <th>Claim Amount</th>
                              <th><i class="material-icons">gavel</i></th>
                            </tr>
                          </thead>
                          <tbody id="Approval">
                          <?php foreach ($exte as $key => $val) { ?>
                            <tr>
                              <td><?php echo ($key + 1); ?><input type="hidden" name="val[<?php echo $key; ?>]" value='<?php echo json_encode($val); ?>'></td>
                              <td><a onclick="external('claim/external_data', '<?php echo $val['ci_reff_no']; ?>', '<?php echo $val['ci_policy_no']; ?>');" href="#"><small><?php echo $val['ci_reff_no']; ?></small></a></td>
                              <td><small class="inDate"><?php echo $val['ci_date']; ?></small></td>
                              <td><small><?php echo $val['ci_provider_name']; ?></small></td>
                              <td><small><?php echo $val['ci_policy_name']; ?></small></td>
                              <td><small><?php echo $val['ci_member_name']; ?></small></td>
                              <td><small><?php echo $val['ci_benefit_type']; ?></small></td>
                              <td><small class="rupiah"><?php echo $val['ci_approved_amount']; ?></small></td>
                              <td>
                                <button title="Approved Claim" class="btn btn-sm btn-success btn-round btn-fab" onclick="external('claim/external_data', '<?php echo $val['ci_reff_no']; ?>', '<?php echo $val['ci_policy_no']; ?>');">
                                  <i class="material-icons">check</i>
                                </button>
                              </td>
                            </tr>
                          <?php } ?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>

    </div>
  </div>
</div>

<!-- notice modal -->
<div class="modal fade bs-example-modal-lg" id="noticeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="padding-right: 50%">
  <div class="modal-dialog modal-notice modal-lg" style="width: 80%;">
    <div class="modal-content" style="width: 150%;">
      <div class="modal-header">
        <h5 class="modal-title" id="myModalLabel">Inquery Data Registered Claim</h5>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
          <i class="material-icons">close</i>
        </button>
      </div>
      <div class="modal-body">
        <div class="instruction">
          <div class="row">
            <div class="col-md-12">
              <div class="material-datatables">
                <table class="table table-striped table-no-bordered table-hover datatables" cellspacing="0" width="100%" style="width:100%">
                  <thead>
                    <tr>
                      <th><i class="fa fa-hashtag"></i></th>
                      <th>No. Batch</th>
                      <th>Type</th>
                      <th>Policy / Provider Name</th>
                      <th>Received Date</th>
                      <th>SLA Finish Date</th>
                      <th>Remain Day</th>
                      <th>Claim Process</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php foreach ($claim as $key => $val) { ?>
                    <?php 
                      $spareday = ((abs(strtotime ($val['clmreg_sla_finish_date']) - strtotime (cdate))) / (60 * 60 * 24));
                      $remainday  = $val['clmreg_sla_days'] - $spareday;
                    ?>
                    <tr>
                      <td><?php echo ($key + 1); ?><input type="hidden" id="registered<?php echo $key; ?>" value='<?php echo json_encode($val); ?>'></td>
                      <td>
                        <a onclick="binding('#registered<?php echo $key; ?>'); karyawan('claim/karyawan', '<?php echo $val['clmreg_register_no']; ?>', '#Claim', '<?php echo $val['clmreg_claim_type']; ?>', '<?php echo $val['clmreg_claimer_code']; ?>', '<?php echo $val['clmreg_claimer_name']; ?>');" data-dismiss="modal" href="#">
                          <?php echo $val['clmreg_register_no']; ?>
                          </a>
                      </td>
                      <td><?php echo $type[$val['clmreg_claim_type']]; ?></td>
                      <td><?php echo $val['clmreg_claimer_name']; ?></td>
                      <td class="inDate"><?php echo $val['clmreg_receive_date']; ?></td>
                      <td class="inDate"><?php echo $val['clmreg_sla_finish_date']; ?></td>
                      <td class="text-right"><?php echo $remainday; ?></td>
                      <td class="text-right rupiah"> <?php echo $val['clmreg_claim_proposed']; ?></td>
                    </tr>
                  <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end notice modal -->

<!-- notice modal -->
<div class="modal fade bs-example-modal-lg" id="Mkaryawan" tabindex="-1" role="dialog" aria-labelledby="karyawan" aria-hidden="true" style="padding-right: 50%">
  <div class="modal-dialog modal-notice modal-lg" style="width: 80%;">
    <div class="modal-content" style="width: 150%;">
      <div class="modal-header">
        <h5 class="modal-title" id="karyawan">Inquery Data Karyawan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
          <i class="material-icons">close</i>
        </button>
      </div>
      <div class="modal-body">
        <div class="instruction">
          <div class="row">
            <div class="col-md-12">
              <div class="material-datatables">
                <table class="table table-striped table-no-bordered table-hover datatables-new" cellspacing="0" width="100%" style="width:100%">
                  <thead>
                    <tr>
                      <th><i class="fa fa-hashtag"></i></th>
                      <th>No. Member</th>
                      <th>Member Name</th>
                      <th>Card No</th>
                      <th>Enroll</th>
                      <th>DOB</th>
                    </tr>
                  </thead>
                  <tbody id="list-karyawan"></tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end notice modal -->

<!-- notice modal -->
<div class="modal fade bs-example-modal-lg" id="DetailEx" tabindex="-1" role="dialog" aria-labelledby="DetailEx" aria-hidden="true" style="padding-right: 50%">
  <div class="modal-dialog modal-notice modal-lg" style="width: 80%;">
    <div class="modal-content" style="width: 150%;">
      <div class="modal-header">
        <h5 class="modal-title" id="DetailEx">Form Claim Analyst No.Reff <div id="external-ci_reff_no" class="text-primary"></div></h5>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
          <i class="material-icons">close</i>
        </button>
      </div>
      <div class="modal-body">
        <div class="instruction">
          <div class="row">
            <div class="col-md-6">
              <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                <tr class="font_label_field">
                  <th>Claim Date</th>
                  <td id="external-ci_date"></td>
                </tr>
                <tr class="font_label_field">
                  <th>Provider</th>
                  <td id="external-ci_provider_name"></td>
                </tr>
                <tr class="font_label_field">
                  <th>Policy</th>
                  <td id="external-ci_policy_name"></td>
                </tr>
                <tr class="font_label_field">
                  <th>Member Name</th>
                  <td id="external-ci_member_name"></td>
                </tr>
                <tr class="font_label_field">
                  <th>Member Card No</th>
                  <td id="external-ci_card_no"></td>
                </tr>
              </table>
            </div>

            <div class="col-md-6">
              <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                <tr class="font_label_field">
                  <th>Benefit Type</th>
                  <td id="external-ci_benefit_type"></td>
                </tr>
                <tr class="font_label_field">
                  <th>Total Proposed</th>
                  <td id="external-ci_proposed_amount"></td>
                </tr>
                <tr class="font_label_field">
                  <th>Total Approve</th>
                  <td id="external-ci_approved_amount"></td>
                </tr>
                <tr class="font_label_field">
                  <th>Total Excess Member</th>
                  <td id="external-ci_excess_member"></td>
                </tr>
                <tr class="font_label_field">
                  <th>Total Excess SSA</th>
                  <td id="external-ci_excess_ssa"></td>
                </tr>
              </table>
            </div>

            <div class="col-md-12">
              <div class="material-datatables">
                <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                  <thead>
                    <tr>
                      <th><i class="fa fa-hashtag"></i></th>
                      <th>KD Benefit</th>
                      <th>Benefit</th>
                      <th>Proposed</th>
                      <th>Approved</th>
                      <th>Excess</th>
                    </tr>
                  </thead>
                  <tbody id="list-DetailEx"></tbody>
                  <tfoot id="foot-DetailEx"></tfoot>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer justify-content-center">
        <form method="post" class="form-horizontal" action="<?php echo base_url(); ?>claim/external">
          <input type="hidden" name="ci_reff_no" id="ci_reff_no">
          <button type="submit" class="btn btn-success btn-round" data-dismiss="modal">Approved Claim <i class="material-icons">check</i></button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- end notice modal -->