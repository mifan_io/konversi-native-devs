<div class="content">
  <div class="content">
    <div class="container-fluid">

      <?php if (isset($_SESSION['users_input']) && $_SESSION['users_input'] == true) { ?>
      <div class="alert alert-<?php echo $_SESSION['users_status']; ?>">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <i class="material-icons">close</i>
        </button>
        <span><b class="text-capitalize"> <?php echo $_SESSION['users_status']; ?> - </b> <?php echo $_SESSION['users_text']; ?></span>
      </div>
      <?php } unset($_SESSION['users_input']); unset($_SESSION['users_status']); unset($_SESSION['users_text']); ?>

      <div class="row">
        <div class="col-md-12">
          <div class="card ">

            <div class="card-header card-header-primary card-header-icon">
              <div class="card-icon">
                <i class="material-icons">vertical_split</i>
              </div>
              <h4 class="card-title">Approval Claim | Provider</h4>
            </div>

            <div class="card-body ">
              <div class="row">
                <div class="col-md-12">
                  <div class="material-datatables">
                    <table class="table table-striped table-hover datatables" cellspacing="0" width="100%" style="width:100%">
                      <thead>
                        <tr>
                          <th><i class="fa fa-hashtag"></i></th>
                          <th>No. Reff</th>
                          <th>Provider Name</th>
                          <th>Policy</th>
                          <th>Member Name</th>
                          <th>Serv IN</th>
                          <th>Benefit</th>
                          <th>Claim Amount</th>
                          <th><i class="material-icons">gavel</i></th>
                        </tr>
                      </thead>

                      <tbody id="Approval">
                      <?php foreach ($exte as $key => $val) { ?>
                        <tr>
                          <td><?php echo ($key + 1); ?><input type="hidden" name="val[<?php echo $key; ?>]" value='<?php echo json_encode($val); ?>'></td>
                          <td><a onclick="external('claim/external_data', '<?php echo $val['ci_reff_no']; ?>', '<?php echo $val['ci_policy_no']; ?>');" href="#"><small><?php echo $val['ci_reff_no']; ?></small></a></td>
                          <td><small class="inDate"><?php echo $val['ci_date']; ?></small></td>
                          <td><small><?php echo $val['ci_provider_name']; ?></small></td>
                          <td><small><?php echo $val['ci_policy_name']; ?></small></td>
                          <td><small><?php echo $val['ci_member_name']; ?></small></td>
                          <td><small><?php echo $val['ci_benefit_type']; ?></small></td>
                          <td><small class="rupiah"><?php echo $val['ci_approved_amount']; ?></small></td>
                          <td>
                            <button title="Approved Claim" class="btn btn-sm btn-success btn-round btn-fab" onclick="external('claim/external_data', '<?php echo $val['ci_reff_no']; ?>', '<?php echo $val['ci_policy_no']; ?>');">
                              <i class="material-icons">check</i>
                            </button>
                          </td>
                        </tr>
                      <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>

    </div>
  </div>
</div>

<!-- notice modal -->
<div class="modal fade bs-example-modal-lg" id="DetailEx" tabindex="-1" role="dialog" aria-labelledby="DetailEx" aria-hidden="true" style="padding-right: 50%">
  <div class="modal-dialog modal-notice modal-lg" style="width: 80%;">
    <div class="modal-content" style="width: 150%;">
      <div class="modal-header">
        <h5 class="modal-title" id="DetailEx">Form Claim Analyst No.Reff <div id="external-ci_reff_no" class="text-primary"></div></h5>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
          <i class="material-icons">close</i>
        </button>
      </div>
      <div class="modal-body">
        <div class="instruction">
          <div class="row">
            <div class="col-md-6">
              <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                <tr class="font_label_field">
                  <th>Claim Date</th>
                  <td id="external-ci_date"></td>
                </tr>
                <tr class="font_label_field">
                  <th>Provider</th>
                  <td id="external-ci_provider_name"></td>
                </tr>
                <tr class="font_label_field">
                  <th>Policy</th>
                  <td id="external-ci_policy_name"></td>
                </tr>
                <tr class="font_label_field">
                  <th>Member Name</th>
                  <td id="external-ci_member_name"></td>
                </tr>
                <tr class="font_label_field">
                  <th>Member Card No</th>
                  <td id="external-ci_card_no"></td>
                </tr>
              </table>
            </div>

            <div class="col-md-6">
              <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                <tr class="font_label_field">
                  <th>Benefit Type</th>
                  <td id="external-ci_benefit_type"></td>
                </tr>
                <tr class="font_label_field">
                  <th>Total Proposed</th>
                  <td id="external-ci_proposed_amount"></td>
                </tr>
                <tr class="font_label_field">
                  <th>Total Approve</th>
                  <td id="external-ci_approved_amount"></td>
                </tr>
                <tr class="font_label_field">
                  <th>Total Excess Member</th>
                  <td id="external-ci_excess_member"></td>
                </tr>
                <tr class="font_label_field">
                  <th>Total Excess SSA</th>
                  <td id="external-ci_excess_ssa"></td>
                </tr>
              </table>
            </div>

            <div class="col-md-12">
              <div class="material-datatables">
                <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                  <thead>
                    <tr>
                      <th><i class="fa fa-hashtag"></i></th>
                      <th>KD Benefit</th>
                      <th>Benefit</th>
                      <th>Proposed</th>
                      <th>Approved</th>
                      <th>Excess</th>
                    </tr>
                  </thead>
                  <tbody id="list-DetailEx"></tbody>
                  <tfoot id="foot-DetailEx"></tfoot>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer justify-content-center">
        <form method="post" class="form-horizontal" action="<?php echo base_url(); ?>claim/external">
          <input type="hidden" name="ci_reff_no" id="ci_reff_no">
          <button type="submit" class="btn btn-success btn-round" data-dismiss="modal">Approved Claim <i class="material-icons">check</i></button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- end notice modal -->