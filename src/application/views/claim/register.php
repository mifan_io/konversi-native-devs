<div class="content">
  <div class="content">
    <div class="container-fluid">

      <?php if (isset($_SESSION['users_input']) && $_SESSION['users_input'] == true) { ?>
      <div class="alert alert-<?php echo $_SESSION['users_status']; ?>">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <i class="material-icons">close</i>
        </button>
        <span><b class="text-capitalize"> <?php echo $_SESSION['users_status']; ?> - </b> <?php echo $_SESSION['users_text']; ?></span>
      </div>
      <?php } unset($_SESSION['users_input']); unset($_SESSION['users_status']); unset($_SESSION['users_text']); ?>

      <div class="row">
        <div class="col-md-12">
          <div class="card ">

            <div class="card-header card-header-primary card-header-icon">
              <div class="card-icon">
                <i class="material-icons">class</i>
              </div>
              <h4 class="card-title">Transaction Register Claim</h4>
            </div>

            <div class="card-body ">
              <form method="post" class="form-horizontal">
                <div class="row">
                  <div class="col-md-12">

                    <div class="form-group">
                      <div class="input-group no-border">
                        <select class="selectpicker" name="clmreg_claim_type" id="type" onchange="_claimtype('#type');" data-style="select-with-transition" title="Choose Claim Type" data-size="7" required="required">
                        <?php foreach ($type as $key => $val) { ?>
                          <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                        <?php } ?>
                        </select>
                      </div>
                    </div>

                    <div class="form-group P-display A-display" style="display: none;">
                      <div class="input-group no-border">
                        <input type="text" id="prov" class="form-control" placeholder="Provider Code" readonly="readonly">
                        <button type="button" data-toggle="modal" class="btn btn-white btn-round btn-just-icon" data-target="#Mkaryawan" onclick="large('Mkaryawan');">
                          <i class="material-icons">search</i>
                          <div class="ripple-container"></div>
                        </button>
                      </div>
                    </div>

                    <div class="form-group R-display A-display" style="display: none;">Periode Polis (<span id="periode"></span>)</div>
                    <div class="form-group R-display A-display" style="display: none;">
                      <div class="input-group no-border">
                        <input type="text" id="no" class="form-control" placeholder="Nomor Polis" readonly="readonly">
                        <button type="button" data-toggle="modal" class="btn btn-white btn-round btn-just-icon" data-target="#noticeModal" onclick="large('noticeModal');">
                          <i class="material-icons">search</i>
                          <div class="ripple-container"></div>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row benefit-option" style="display: none;">
                  <div class="col-md-4">
                    <div class="input-group form-control-md">
                      <div class="form-group bmd-form-group">
                        <label for="classplancode" class="bmd-label-floating">Invoice No</label>
                        <input type="text" class="form-control num" id="classplancode" name="clmreg_invoice_no" required="required">
                      </div>
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="input-group form-control-md">
                      <div class="form-group">
                        <div class="input-group no-border">
                          <label for="classplancod" class="bmd-label-floating">Invoice Date</label>
                          <input type="text" class="form-control datepicker" id="classplancod" name="clmreg_invoice_date" value="<?php echo date('d-m-Y'); ?>" required="required">
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="input-group form-control-md">
                      <div class="form-group bmd-form-group">
                        <label for="lassplancode" class="bmd-label-floating">Received Date</label>
                        <input type="text" class="form-control datepicker" id="lassplancode" name="clmreg_receive_date" required="required" value="<?php echo date('d-m-Y'); ?>">
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row text-right">
                  <div class="col-md-9">&nbsp;</div>
                  <div class="col-md-3">
                    <input type="hidden" id="code">
                    <input type="hidden" id="_services" name="clmreg_sla_days">
                    <input type="hidden" id="_name" name="clmreg_claimer_name">
                    <input type="hidden" id="polbt_pol_no" name="clmreg_claimer_code">
                    <button type="submit" class="btn btn-fill btn-primary submit">Submit</button>
                  </div>
                </div>
              </form>
            </div>

          </div>
        </div>
      </div>

    </div>
  </div>
</div>

<!-- notice modal -->
<div class="modal fade bs-example-modal-lg" id="noticeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="padding-right: 50%">
  <div class="modal-dialog modal-notice modal-lg" style="width: 80%;">
    <div class="modal-content" style="width: 150%;">
      <div class="modal-header">
        <h5 class="modal-title" id="myModalLabel">Inquery Data Policy</h5>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
          <i class="material-icons">close</i>
        </button>
      </div>
      <div class="modal-body">
        <div class="instruction">
          <div class="row">
            <div class="col-md-12">
              <div class="material-datatables">
                <table class="table table-striped table-no-bordered table-hover datatables" cellspacing="0" width="100%" style="width:100%">
                  <thead>
                    <tr>
                      <th><i class="fa fa-hashtag"></i></th>
                      <th>No. Polis</th>
                      <th>Policy Name</th>
                      <th>Start Date</th>
                      <th>End Date</th>
                      <th>Member(s)</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php foreach ($data as $key => $val) { ?>
                    <tr>
                      <td><?php echo ($key + 1); ?></td>
                      <td>
                        <a data-dismiss="modal" href="#" onclick="display('.benefit-option'); code('policy/code', '<?php echo $val['pol_pol_no']; ?>', '#code'); binding('#benefit<?php echo $key; ?>'); karyawan('member/karyawan', '<?php echo $val['pol_pol_no']; ?>', '#list-karyawan');">
                          <?php echo $val['pol_pol_no']; ?>
                          </a>
                      </td>
                      <td><?php echo $val['pol_company_name']; ?></td>
                      <td><?php echo $val['pol_start_date']; ?></td>
                      <td><?php echo $val['pol_end_date']; ?></td>
                      <td class="text-right"><?php echo $val['pol_member']; ?></td>
                      <td>
                        <?php echo $val['pol_status']; ?>
                        <input type="hidden" id="benefit<?php echo $key; ?>" value='<?php echo json_encode($val); ?>'>
                      </td>
                    </tr>
                  <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end notice modal -->

<!-- notice modal -->
<div class="modal fade bs-example-modal-lg" id="Mkaryawan" tabindex="-1" role="dialog" aria-labelledby="karyawan" aria-hidden="true" style="padding-right: 50%">
  <div class="modal-dialog modal-notice modal-lg" style="width: 80%;">
    <div class="modal-content" style="width: 150%;">
      <div class="modal-header">
        <h5 class="modal-title" id="karyawan">Inquery Data Provider</h5>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
          <i class="material-icons">close</i>
        </button>
      </div>
      <div class="modal-body">
        <div class="instruction">
          <div class="row">
            <div class="col-md-12">
              <div class="material-datatables">
                <table class="table table-striped table-no-bordered table-hover datatables" cellspacing="0" width="100%" style="width:100%">
                  <thead>
                    <tr>
                      <th><i class="fa fa-hashtag"></i></th>
                      <th>Kode</th>
                      <th>Provider Name</th>
                      <th>Type</th>
                      <th>City</th>
                      <th>Phone</th>
                      <th>BPJS</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php foreach ($prov as $key => $val) { ?>
                    <tr>
                      <td><?php echo ($key + 1); ?><input type="hidden" id="provider<?php echo $key; ?>" value='<?php echo json_encode($val); ?>'></td>
                      <td><a onclick="display('.benefit-option'); _provider('#provider<?php echo $key; ?>');" href="#" data-dismiss="modal"><?php echo $val['mprov_prov_code']; ?></a></td>
                      <td><?php echo $val['mprov_name']; ?></td>
                      <td><?php echo $val['mco_description']; ?></td>
                      <td><?php echo $val['mprov_city']; ?></td>
                      <td><?php echo $val['mprov_telephone']; ?></td>
                      <td><?php echo $val['mprov_bpjs']; ?></td>
                    </tr>
                  <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end notice modal -->