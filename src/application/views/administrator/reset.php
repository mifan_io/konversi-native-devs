<div class="content">
  <div class="content">
    <div class="container-fluid">

      <?php if (isset($_SESSION['users_input']) && $_SESSION['users_input'] == true) { ?>
      <div class="alert alert-<?php echo $_SESSION['users_status']; ?>">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <i class="material-icons">close</i>
        </button>
        <span><b class="text-capitalize"> <?php echo $_SESSION['users_status']; ?> - </b> <?php echo $_SESSION['users_text']; ?></span>
      </div>
      <?php } unset($_SESSION['users_input']); unset($_SESSION['users_status']); unset($_SESSION['users_text']); ?>

      <div class="row">
        <div class="col-md-12">
          <div class="card ">

            <div class="card-header card-header-rose card-header-icon">
              <div class="card-icon">
                <i class="material-icons">vpn_key</i>
              </div>
              <h4 class="card-title">Form Reset Password User</h4>
            </div>

            <div class="card-body ">
              <form method="post" class="form-horizontal">
                <div class="row">
                  <div class="col-md-6">
                    <div class="row">
                      <label class="col-md-3 col-form-label">SYSTEM TYPE</label>
                      <div class="col-md-9">
                        <div class="form-group has-default">
                          <select class="selectpicker" name="system" onchange="chained('administrator/admin', this.value, '#username');" data-style="select-with-transition" title="Choose System" data-size="7" required="required">
                          <?php foreach ($system as $key => $val){ ?>
                            <option value="<?php echo $key ; ?>"><?php echo $val ; ?></option>
                          <?php } ?>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <label class="col-md-3 col-form-label">USERNAME</label>
                      <div class="col-md-9">
                        <div class="form-group has-default">
                          <select class="select2 form-control" name="username" id="username" required="required"></select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="row">
                      <label class="col-md-3 col-form-label">PASSWORD</label>
                      <div class="col-md-9">
                        <div class="form-group has-password">
                          <input type="password" name="password" onkeyup="valid_pass();" class="password form-control" required="required">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <label class="col-md-3 col-form-label">RE-TYPE PASSWORD</label>
                      <div class="col-md-9">
                        <div class="form-group has-password">
                          <input type="password" name="re_password" onkeyup="valid_pass();" class="re_password form-control" required="required">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row pull-right">
                  <div class="col-md-9">
                    <button type="submit" class="btn btn-fill btn-primary submit" disabled="disabled">Submit</button>
                  </div>
                </div>
              </form>
            </div>

          </div>
        </div>
      </div>

    </div>
  </div>
</div>