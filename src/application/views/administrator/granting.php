<div class="content">
  <div class="content">
    <div class="container-fluid">

      <?php if (isset($_SESSION['users_input']) && $_SESSION['users_input'] == true) { ?>
      <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <i class="material-icons">close</i>
        </button>
        <span><b> Success - </b> <?php echo $_SESSION['users_text']; ?></span>
      </div>
      <?php } unset($_SESSION['users_input']); unset($_SESSION['users_text']); ?>

      <div class="row">
        <div class="col-md-12">
          <div class="card ">

            <div class="card-header card-header-primary card-header-icon">
              <div class="card-icon">
                <i class="material-icons">perm_data_setting</i>
              </div>
              <h4 class="card-title">Granting User Module</h4>
            </div>

            <div class="card-body ">
              <div class="row">
                <div class="col-md-12">
                  <form method="post" class="form-horizontal">
                    <div class="row">
                      <label class="col-md-3 col-form-label">SYSTEM TYPE</label>
                      <div class="col-md-9">
                        <div class="form-group has-default">
                          <select class="selectpicker" name="system" id="system" onchange="chained('administrator/admin', this.value, '#username');" data-style="select-with-transition" title="Choose System" data-size="7" required="required">
                          <?php foreach ($system as $key => $val){ ?>
                            <option value="<?php echo $key ; ?>"><?php echo $val ; ?></option>
                          <?php } ?>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <label class="col-md-3 col-form-label">USERNAME</label>
                      <div class="col-md-9">
                        <div class="form-group has-default">
                          <select class="select2 form-control" name="username" id="username" required="required" onchange="granting('administrator/system', this.value, '#granting', system.value);"></select>
                        </div>
                      </div>
                    </div>
                    <div>
                      <div class="table-full-width table-responsive">
                        <form method="post" class="form" action="">
                          <table class="table">
                            <tbody id="granting">
                              <tr>
                                <td><label class="col-md-3 col-form-label label-checkbox">Checkboxes and radios</label></td>
                                <td>
                                  <div class="form-check">
                                    <label class="form-check-label">
                                      <input class="form-check-input" type="checkbox" value="">
                                      <span class="form-check-sign">
                                        <span class="check"></span>
                                      </span>
                                    </label>
                                  </div>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                          <div class="row pull-right">
                            <div class="col-md-9">
                              <button type="submit" class="btn btn-fill btn-primary submit" disabled="disabled">Submit</button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>

    </div>
  </div>
</div>