<div class="content">
  <div class="content">
    <div class="container-fluid">

      <?php if (isset($_SESSION['users_input']) && $_SESSION['users_input'] == true) { ?>
      <div class="alert alert-<?php echo $_SESSION['users_status']; ?>">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <i class="material-icons">close</i>
        </button>
        <span><b class="text-capitalize"> <?php echo $_SESSION['users_status']; ?> - </b> <?php echo $_SESSION['users_text']; ?></span>
      </div>
      <?php } unset($_SESSION['users_input']); unset($_SESSION['users_status']); unset($_SESSION['users_text']); ?>

      <div class="row">
        <div class="col-md-12">
          <div class="card ">

            <div class="card-header card-header-rose card-header-icon">
              <div class="card-icon">
                <i class="material-icons">vpn_key</i>
              </div>
              <h4 class="card-title">Form Change Password User</h4>
            </div>

            <div class="card-body ">
              <div class="row">
                <div class="col-md-12 ml-auto mr-auto">
                  <form method="post" class="form-horizontal">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="row">
                          <label class="col-md-3 col-form-label">FULL NAME</label>
                          <div class="col-md-9">
                            <div class="form-group has-default">
                              <input type="text" value="<?php echo $_SESSION['name'] . '(' . $_SESSION['username'] . ')'; ?>" class="form-control" readonly="readonly">
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <label class="col-md-3 col-form-label">CURRENT PASSWORD</label>
                          <div class="col-md-9">
                            <div class="form-group has-default">
                              <input type="password" name="old_password" class="form-control" required="required">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="row">
                          <label class="col-md-3 col-form-label">PASSWORD</label>
                          <div class="col-md-9">
                            <div class="form-group has-password">
                              <input type="password" name="password" onkeyup="valid_pass();" class="password form-control" required="required">
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <label class="col-md-3 col-form-label">RE-TYPE PASSWORD</label>
                          <div class="col-md-9">
                            <div class="form-group has-password">
                              <input type="password" name="re_password" onkeyup="valid_pass();" class="re_password form-control" required="required">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row pull-right">
                      <div class="col-md-9">
                        <button type="submit" class="btn btn-fill btn-primary submit" disabled="disabled">Submit</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>

    </div>
  </div>
</div>