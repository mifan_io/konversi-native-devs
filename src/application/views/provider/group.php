<div class="content">
  <div class="content">
    <div class="container-fluid">

      <?php if (isset($_SESSION['users_input']) && $_SESSION['users_input'] == true) { ?>
      <div class="alert alert-<?php echo $_SESSION['users_status']; ?>">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <i class="material-icons">close</i>
        </button>
        <span><b class="text-capitalize"> <?php echo $_SESSION['users_status']; ?> - </b> <?php echo $_SESSION['users_text']; ?></span>
      </div>
      <?php } unset($_SESSION['users_input']); unset($_SESSION['users_status']); unset($_SESSION['users_text']); ?>

      <div class="row">
        <div class="col-md-12">
          <div class="card ">

            <div class="card-header card-header-info card-header-icon">
              <div class="card-icon">
                <i class="material-icons">chrome_reader_mode</i>
              </div>
              <h4 class="card-title">Data Group Provider</h4>
            </div>

            <div class="card-body ">
              <div class="row">
                <div class="col-md-12">
                  <div class="material-datatables">
                    <table class="table table-striped table-no-bordered table-hover datatables" cellspacing="0" width="100%" style="width:100%">
                      <thead>
                        <tr>
                          <th><i class="fa fa-hashtag"></i></th>
                          <th>Group Name</th>
                          <th>Provider Pusat</th>
                          <th>PKS Date</th>
                          <th>Expired Date</th>
                          <th>SSA PKS No</th>
                          <th>Provider PKS No</th>
                          <th>Status</th>
                          <th>PKS File</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php foreach ($data as $key => $val) { ?>
                      <?php $file = ($val['mpg_attach_pks']) ? '<a href="' . $val['mpg_attach_pks'] . '" target="_blank"><b>PKS File</b></a>' : '' ; ?>
                        <tr>
                          <td><small><?php echo ($key + 1); ?></small></td>
                          <td><small><?php echo $val['mpg_group_name']; ?></small></td>
                          <td><small><?php echo $val['mprov_name']; ?></small></td>
                          <td><small class="inDate"><?php echo $val['mpg_pks_date']; ?></small></td>
                          <td><small class="inDate"><?php echo $val['mpg_expired_pks']; ?></small></td>
                          <td><small><?php echo $val['mpg_pks_no']; ?></small></td>
                          <td><small><?php echo $val['mpg_pks_no_provider']; ?></small></td>
                          <td><small><?php echo $val['mpg_status']; ?></small></td>
                          <td><small><?php echo $file; ?></small></td>
                        </tr>
                      <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>

    </div>
  </div>
</div>