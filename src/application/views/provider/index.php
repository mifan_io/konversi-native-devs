<div class="content">
  <div class="content">
    <div class="container-fluid">

      <?php if (isset($_SESSION['users_input']) && $_SESSION['users_input'] == true) { ?>
      <div class="alert alert-<?php echo $_SESSION['users_status']; ?>">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <i class="material-icons">close</i>
        </button>
        <span><b class="text-capitalize"> <?php echo $_SESSION['users_status']; ?> - </b> <?php echo $_SESSION['users_text']; ?></span>
      </div>
      <?php } unset($_SESSION['users_input']); unset($_SESSION['users_status']); unset($_SESSION['users_text']); ?>

      <div class="row">
        <div class="col-md-12">
          <div class="card ">

            <div class="card-header card-header-info card-header-icon">
              <div class="card-icon">
                <i class="material-icons">featured_play_list</i>
              </div>
              <h4 class="card-title">Data Provider Relation</h4>
            </div>

            <div class="card-body ">
              <div class="row">
                <div class="col-md-12">
                  <div class="material-datatables">
                    <a href="#" onclick="tableToExcel('Provider', 'Data Provider Relation');"><i class="text-success fa fa-file-excel-o"></i></a>
                    <table class="table table-striped table-no-bordered table-hover datatables" cellspacing="0" width="100%" style="width:100%">
                      <thead>
                        <tr>
                          <th><i class="fa fa-hashtag"></i></th>
                          <th>Kode</th>
                          <th>Provider Name</th>
                          <th>Type</th>
                          <th>City</th>
                          <th>Phone</th>
                          <th>Group Name</th>
                          <th>Status System</th>
                        </tr>
                      </thead>
                      <tbody><?php echo $html; ?></tbody>
                    </table>

                    <table style="display: none;" id="Provider">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Kode</th>
                          <th>Provider Name</th>
                          <th>Type</th>
                          <th>City</th>
                          <th>Phone</th>
                          <th>Group Name</th>
                          <th>Status System</th>
                        </tr>
                      </thead>
                      <tbody><?php echo $html; ?></tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>

    </div>
  </div>
</div>