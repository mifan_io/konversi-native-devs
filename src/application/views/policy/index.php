<div class="content">
  <div class="content">
    <div class="container-fluid">

      <?php if (isset($_SESSION['users_input']) && $_SESSION['users_input'] == true) { ?>
      <div class="alert alert-<?php echo $_SESSION['users_status']; ?>">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <i class="material-icons">close</i>
        </button>
        <span><b class="text-capitalize"> <?php echo $_SESSION['users_status']; ?> - </b> <?php echo $_SESSION['users_text']; ?></span>
      </div>
      <?php } unset($_SESSION['users_input']); unset($_SESSION['users_status']); unset($_SESSION['users_text']); ?>

      <div class="row">
        <div class="col-md-12">
          <div class="card ">

            <div class="card-header card-header-rose card-header-icon">
              <div class="card-icon">
                <i class="material-icons">class</i>
              </div>
              <h4 class="card-title">Inquery Data Policy</h4>
            </div>

            <div class="card-body ">
              <div class="row">
                <div class="col-md-12">
                  <div class="toolbar text-right">
                    <button class="btn btn-primary btn-round btn-fab" title="Tambah Policy" onclick="redirect('policy/form');">
                      <i class="material-icons">note_add</i>
                    </button>
                  </div>
                  <div class="material-datatables">
                    <table class="table table-striped table-no-bordered table-hover datatables" cellspacing="0"
                      width="100%" style="width:100%">
                      <thead>
                        <tr>
                          <th><i class="fa fa-hashtag"></i></th>
                          <th>No. Polis</th>
                          <th>Policy Name</th>
                          <th>Start Date</th>
                          <th>End Date</th>
                          <th>Member(s)</th>
                          <th>Status</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php foreach ($data as $key => $val) { ?>
                        <tr>
                          <td><?php echo ($key + 1); ?></td>
                          <td><a href="<?php echo base_url() . 'policy/form/' . $val['pol_pol_no']; ?>"><?php echo $val['pol_pol_no']; ?></a></td>
                          <td><?php echo $val['pol_company_name']; ?></td>
                          <td><?php echo $val['pol_start_date']; ?></td>
                          <td><?php echo $val['pol_end_date']; ?></td>
                          <td class="text-right"><?php echo $val['pol_member']; ?></td>
                          <td><?php echo $val['pol_status']; ?></td>
                        </tr>
                      <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>

    </div>
  </div>
</div>