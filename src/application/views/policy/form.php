<div class="content">
  <div class="container-fluid">

    <?php if (isset($_SESSION['users_input']) && $_SESSION['users_input'] == true) { ?>
    <div class="alert alert-<?php echo $_SESSION['users_status']; ?>">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <i class="material-icons">close</i>
      </button>
      <span><b class="text-capitalize"> <?php echo $_SESSION['users_status']; ?> - </b> <?php echo $_SESSION['users_text']; ?></span>
    </div>
    <?php } unset($_SESSION['users_input']); unset($_SESSION['users_status']); unset($_SESSION['users_text']); ?>

    <div class="col-md-12 mr-auto ml-auto">
      <div class="wizard-container">
        <div class="card card-wizard active" data-color="purple" id="wizardProfile">
          <form action="" method="post">
            <!--        You can switch " data-color="primary" "  with one of the next bright colors: "green", "orange", "red", "blue"       -->
            <div class="card-header text-center">
              <h3 class="card-title">Form Policy</h3>
            </div>

            <div class="wizard-navigation">
              <ul class="nav nav-pills">
                <li class="nav-item" style="width: 33.3333%;">
                  <a class="nav-link active" href="#about" data-toggle="tab" role="tab">Profile</a>
                </li>
                <li class="nav-item" style="width: 33.3333%;">
                  <a class="nav-link" href="#account" data-toggle="tab" role="tab">Benefit</a>
                </li>
                <li class="nav-item" style="width: 33.3333%;">
                  <a class="nav-link" href="#address" data-toggle="tab" role="tab">Finance</a>
                </li>
              </ul>
              <div class="moving-tab" style="width: 219.109px; transform: translate3d(-8px, 0px, 0px); transition: transform 0s ease 0s;">About</div>
            </div>

            <div class="card-body">
              <div class="tab-content">

                <div class="tab-pane active" id="about">
                  <h5 class="info-text">Profile</h5>
                  <div class="row justify-content-center">
                    <div class="col-md-12">

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput0" class="bmd-label-floating">Nomor Polis</label>
                          <input type="text" class="form-control" id="exampleInput0" value="<?php echo $id; ?>" readonly="readonly">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput1" class="bmd-label-floating">Perusahaan</label>
                          <input type="text" class="form-control" id="exampleInput1" name="pol_company_name" value="<?php echo $data['pol_company_name']; ?>">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput2" class="bmd-label-floating">Alamat</label>
                          <input type="text" class="form-control" id="exampleInput2" name="pol_address" value="<?php echo $data['pol_address']; ?>">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput3" class="bmd-label-floating">Kota</label>
                          <input type="text" class="form-control" id="exampleInput3" name="pol_city" value="<?php echo $data['pol_city']; ?>">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput4" class="bmd-label-floating">Province</label>
                          <input type="text" class="form-control" id="exampleInput4"  name="pol_province" value="<?php echo $data['pol_province']; ?>">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput5" class="bmd-label-floating">Zipcode</label>
                          <input type="number" class="form-control num" id="exampleInput5"  name="pol_zipcode">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput6" class="bmd-label-floating">Phone</label>
                          <input type="number" class="form-control num" id="exampleInput6" name="pol_phone">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput7" class="bmd-label-floating">Fax</label>
                          <input type="number" class="form-control num" id="exampleInput7"  name="pol_fax">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput8" class="bmd-label-floating">Contact</label>
                          <input type="number" class="form-control num" id="exampleInput8" name="pol_cp">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput9" class="bmd-label-floating">Email CP</label>
                          <input type="email" class="form-control" id="exampleInput9" name="pol_cp_email">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput10" class="bmd-label-floating">Start Date</label>
                          <input type="text" class="form-control datepicker" id="exampleInput10" name="pol_start_date" value="<?php echo $data['pol_start_date']; ?>">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput11" class="bmd-label-floating">End Date</label>
                          <input type="text" class="form-control datepicker" id="exampleInput11" name="pol_end_date" value="<?php echo $data['pol_end_date']; ?>">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <!-- <label for="exampleInput12" class="bmd-label-floating">Holding</label> -->
                          <select class="selectpicker" name="pol_holding_mode" id="exampleInput12" data-style="select-with-transition" title="Choose Holding Mode" data-size="7">
                          <?php foreach ($holding as $key => $val) { ?>
                            <option value="<?php echo $key; ?>" <?php echo ($data['pol_holding_mode'] == $key) ? 'selected="selected"' : '' ; ?>><?php echo $val; ?></option>
                          <?php } ?>
                          </select>
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput13" class="bmd-label-floating">Branch</label>
                          <input type="text" class="form-control" id="exampleInput13" name="pol_branch_code" value="<?php echo $data['pol_branch_code']; ?>">
                        </div>
                      </div>

                    </div>
                  </div>
                </div>

                <div class="tab-pane" id="account">
                  <h5 class="info-text">Benefit</h5>
                  <div class="row justify-content-center">
                    <div class="col-md-12">

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput14" class="bmd-label-floating">Insurance</label>
                          <input type="text" class="form-control num" id="exampleInput14" name="pol_insurance_code" value="<?php echo $data['pol_insurance_code']; ?>">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label class="col-sm-2 col-form-label label-checkbox">Benefit Type</label>
                          <div class="col-sm-10 checkbox-radios"><?php echo $polbt_benefit_type; ?></div>
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <select class="selectpicker" name="pol_product_code" data-style="select-with-transition" title="Choose Product" data-size="7"><?php echo $pol_product_code; ?></select>
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <select class="selectpicker" name="pol_services_type" data-style="select-with-transition" title="Choose Service" data-size="7"><?php echo $pol_services_type; ?></select>
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput15" class="bmd-label-floating">Cost Share</label>
                          <input type="text" class="form-control num" id="exampleInput15" name="pol_cost_share" value="<?php echo $data['pol_cost_share']; ?>">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput16" class="bmd-label-floating">Recovery Claim</label>
                          <input type="text" class="form-control num" id="exampleInput16" name="pol_recovery_claim" value="<?php echo $data['pol_recovery_claim']; ?>">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput17" class="bmd-label-floating">Expired Claim</label>
                          <input type="text" class="form-control num" id="exampleInput17" name="pol_kadaluarsa_claim" value="<?php echo $data['pol_kadaluarsa_claim']; ?>">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label class="col-sm-2 col-form-label label-checkbox">Family Benefit</label>
                          <div class="col-sm-10 checkbox-radios">
                          <?php foreach ($family as $key => $val) { ?>
                            <div class="form-check form-check-inline">
                              <label class="form-check-label">
                                <input class="form-check-input" name="pol_family_benefit" type="radio" value="<?php echo $key; ?>" <?php echo ($data['pol_family_benefit'] == $key) ? 'checked="checked"' : '' ;?>> <?php echo $val; ?>
                                <span class="circle">
                                  <span class="check"></span>
                                </span>
                              </label>
                            </div>
                          <?php } ?>
                          </div>
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput18" class="bmd-label-floating">Refund Endorst</label>
                          <input type="text" class="form-control num" id="exampleInput18" name="pol_refund_endorsement" value="<?php echo $data['pol_refund_endorsement']; ?>">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput19" class="bmd-label-floating">SLA</label>
                          <input type="text" class="form-control num" id="exampleInput19" name="pol_service_level_day" value="<?php echo $data['pol_service_level_day']; ?>">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput20" class="bmd-label-floating">Case Monitoring Amount</label>
                          <input type="text" class="form-control num" id="exampleInput20" name="pol_casemonitoring_platform" value="<?php echo $data['pol_casemonitoring_platform']; ?>">
                        </div>
                      </div>

                    </div>
                  </div>
                </div>

                <div class="tab-pane" id="address">
                  <h5 class="info-text">Finance</h5>
                  <div class="row justify-content-center">
                    <div class="col-md-12">

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <select class="selectpicker" name="pol_currency" data-style="select-with-transition" title="Choose Currency" data-size="7">
                          <?php foreach ($currency as $key => $val) { ?>
                            <option value="<?php echo $val; ?>" <?php echo ($data['pol_currency'] == $val) ? 'selected="selected"' : '' ; ?>><?php echo $val; ?></option>
                          <?php } ?>
                          </select>
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput14" class="bmd-label-floating">Fee TPA</label>
                          <input type="text" class="form-control num" id="exampleInput21" name="pol_fee_tpa" value="<?php echo $data['pol_fee_tpa']; ?>">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput14" class="bmd-label-floating">Fee Card Member</label>
                          <input type="text" class="form-control num" id="exampleInput21" name="pol_fee_card" value="<?php echo $data['pol_fee_card']; ?>">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput14" class="bmd-label-floating">Dedicated Account</label>
                          <input type="text" class="form-control num" id="exampleInput21" name="pol_dedicated_account" value="<?php echo $data['pol_dedicated_account']; ?>">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <select class="selectpicker" name="pol_payment_mode" data-style="select-with-transition" title="Choose Payment Mode" data-size="7">
                            <?php echo $pol_payment_mode; ?>
                          </select>
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <select class="selectpicker" name="pol_payment_to" data-style="select-with-transition" title="Choose Payment To" data-size="7">
                          <?php foreach ($payment as $key => $val) { ?>
                            <option value="<?php echo $key; ?>" <?php echo ($data['pol_payment_to'] == $key) ? 'selected="selected"' : '' ; ?>><?php echo $val; ?></option>
                          <?php } ?>
                          </select>
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <select class="selectpicker" name="pol_agent" data-style="select-with-transition" title="Choose Agent" data-size="7">
                          <?php foreach ($agent as $key => $val) { ?>
                            <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                          <?php } ?>
                          </select>
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput14" class="bmd-label-floating">Commision</label>
                          <input type="text" class="form-control num" id="exampleInput21" name="pol_commision" value="<?php echo $data['pol_commision']; ?>">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput14" class="bmd-label-floating">ACC Number</label>
                          <input type="text" class="form-control num" id="exampleInput21" name="pol_acc_number" value="<?php echo $data['pol_acc_number']; ?>">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput14" class="bmd-label-floating">ACC Name</label>
                          <input type="text" class="form-control num" id="exampleInput21" name="pol_acc_name" value="<?php echo $data['pol_acc_name']; ?>">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <select class="selectpicker" name="pol_acc_bank" data-style="select-with-transition" title="Choose Bank" data-size="7">
                          <?php foreach ($bank as $key => $val) { ?>
                            <option value="<?php echo $val; ?>"><?php echo $val; ?></option>
                          <?php } ?>
                          </select>
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput14" class="bmd-label-floating">ACC Branch</label>
                          <input type="text" class="form-control num" id="exampleInput21" name="pol_acc_branch" value="<?php echo $data['pol_acc_branch']; ?>">
                        </div>
                      </div>

                    </div>
                  </div>
                </div>

              </div>
            </div>
            <div class="card-footer">
              <div class="mr-auto">
                <button type="button" class="btn btn-previous btn-fill btn-rose btn-wd disabled">Previous</button>
                <!-- <input type="button" class="btn btn-previous btn-fill btn-rose btn-wd disabled" name="previous" value="Previous"> -->
              </div>
              <div class="ml-auto">
                <button type="button" class="btn btn-next btn-fill btn-primary btn-wd">Next</button>

                <?php if(empty($id)){ ?>
                <button type="submit" class="btn btn-finish btn-fill btn-primary btn-wd" style="display: none;">Finish</button>
                <?php } ?>
                <!-- <input type="button" class="btn btn-next btn-fill btn-primary btn-wd" name="next" value="Next"> -->
                <!-- <input type="submit" class="btn btn-finish btn-fill btn-primary btn-wd" name="finish" value="Finish" style="display: none;"> -->
              </div>
              <div class="clearfix"></div>
            </div>
          </form>
        </div>
      </div>
    </div>

  </div>
</div>