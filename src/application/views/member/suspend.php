<div class="content">
  <div class="content">
    <div class="container-fluid">

      <?php if (isset($_SESSION['users_input']) && $_SESSION['users_input'] == true) { ?>
      <div class="alert alert-<?php echo $_SESSION['users_status']; ?>">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <i class="material-icons">close</i>
        </button>
        <span><b class="text-capitalize"> <?php echo $_SESSION['users_status']; ?> - </b> <?php echo $_SESSION['users_text']; ?></span>
      </div>
      <?php } unset($_SESSION['users_input']); unset($_SESSION['users_status']); unset($_SESSION['users_text']); ?>

      <div class="row">
        <div class="col-md-12">
          <div class="card ">

            <div class="card-header card-header-danger card-header-icon">
              <div class="card-icon">
                <i class="material-icons">voice_over_off</i>
              </div>
              <h4 class="card-title">Suspend Member</h4>
            </div>

            <div class="card-body ">
              <form method="post" class="form-horizontal">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">Periode Polis (<span id="periode"></span>)</div>
                    <div class="form-group">
                      <div class="input-group no-border">
                        <input type="text" id="no" class="form-control" placeholder="Nomor Polis" readonly="readonly">
                        <button type="button" data-toggle="modal" class="btn btn-white btn-round btn-just-icon" data-target="#noticeModal" onclick="large('noticeModal');">
                          <i class="material-icons">search</i>
                          <div class="ripple-container"></div>
                        </button>
                      </div>
                    </div>

                    <div class="form-group benefit-option" style="display: none;">
                      <div class="input-group no-border">
                        <input type="text" id="nik" class="form-control" placeholder="Karyawan" readonly="readonly">
                        <button type="button" data-toggle="modal" class="btn btn-white btn-round btn-just-icon" data-target="#Mkaryawan" onclick="large('Mkaryawan');">
                          <i class="material-icons">search</i>
                          <div class="ripple-container"></div>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row benefit-option" style="display: none;">
                  <div class="col-md-4">
                    <div class="input-group form-control-md">
                      <div class="form-group bmd-form-group">
                        <label for="classplancode" class="bmd-label-floating">Effective Suspend</label>
                        <input type="text" class="form-control datepicker" id="classplancode" name="mbs_suspend_date" value="<?php echo date('d-m-Y'); ?>" required="required">
                      </div>
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="input-group form-control-md">
                      <div class="form-group">
                        <div class="input-group no-border">
                          <select class="selectpicker" name="mmbr_status" id="provider" data-style="select-with-transition" title="Choose Type Suspend" data-size="7" required="required">
                          <?php foreach ($type as $key => $val) { ?>
                            <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                          <?php } ?>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="input-group form-control-md">
                      <div class="form-group bmd-form-group">
                        <label for="classplancode" class="bmd-label-floating">Reason</label>
                        <input type="text" class="form-control" id="classplancode" name="mbs_suspend_reason" required="required">
                      </div>
                    </div>
                  </div>
                </div>
              
                <div class="row benefit-option" style="display: none;">
                  <div class="col-md-12">
                    <div class="material-datatables">
                      <table class="table table-striped table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                          <tr>
                            <th><i class="fa fa-hashtag"></i></th>
                            <th>Member No</th>
                            <th>Name</th>
                            <th>Card No</th>
                            <th>Enroll Code</th>
                            <th>DOB</th>
                            <th>
                              <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                  <input class="form-check-input" type="checkbox" id="check" onclick="checkall(this, '.checker');">
                                  <span class="form-check-sign">
                                    <span class="check"></span>
                                  </span>
                                </label>
                              </div>
                            </th>
                          </tr>
                        </thead>
                        <tbody id="family_list"></tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="row text-right">
                  <div class="col-md-9">&nbsp;</div>
                  <div class="col-md-3">
                    <input type="hidden" id="val" class="form-control" name="mbs_policy_no">
                    <input type="hidden" id="val2" class="form-control">
                    <input type="hidden" id="val3" class="form-control">
                    <input type="hidden" id="code" class="form-control">
                    <input type="hidden" id="_card" class="form-control">
                    <input type="hidden" id="_member" class="form-control">
                    <button type="submit" class="btn btn-fill btn-primary submit" disabled="disabled">Submit</button>
                  </div>
                </div>
              </form>
            </div>

          </div>
        </div>
      </div>

    </div>
  </div>
</div>

<!-- notice modal -->
<div class="modal fade bs-example-modal-lg" id="noticeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="padding-right: 50%">
  <div class="modal-dialog modal-notice modal-lg" style="width: 80%;">
    <div class="modal-content" style="width: 150%;">
      <div class="modal-header">
        <h5 class="modal-title" id="myModalLabel">Inquery Data Policy</h5>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
          <i class="material-icons">close</i>
        </button>
      </div>
      <div class="modal-body">
        <div class="instruction">
          <div class="row">
            <div class="col-md-12">
              <div class="material-datatables">
                <table class="table table-striped table-no-bordered table-hover datatables" cellspacing="0" width="100%" style="width:100%">
                  <thead>
                    <tr>
                      <th><i class="fa fa-hashtag"></i></th>
                      <th>No. Polis</th>
                      <th>Policy Name</th>
                      <th>Start Date</th>
                      <th>End Date</th>
                      <th>Member(s)</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php foreach ($data as $key => $val) { ?>
                    <tr>
                      <td><?php echo ($key + 1); ?></td>
                      <td>
                        <a data-dismiss="modal" href="#" onclick=" display('.benefit-option'); code('policy/code', '<?php echo $val['pol_pol_no']; ?>', '#code'); binding('#benefit<?php echo $key; ?>'); karyawan('member/karyawan', '<?php echo $val['pol_pol_no']; ?>', '#list-karyawan');">
                          <?php echo $val['pol_pol_no']; ?>
                          </a>
                      </td>
                      <td><?php echo $val['pol_company_name']; ?></td>
                      <td><?php echo $val['pol_start_date']; ?></td>
                      <td><?php echo $val['pol_end_date']; ?></td>
                      <td class="text-right"><?php echo $val['pol_member']; ?></td>
                      <td>
                        <?php echo $val['pol_status']; ?>
                        <input type="hidden" id="benefit<?php echo $key; ?>" value='<?php echo json_encode($val); ?>'>
                      </td>
                    </tr>
                  <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end notice modal -->

<!-- notice modal -->
<div class="modal fade bs-example-modal-lg" id="Mkaryawan" tabindex="-1" role="dialog" aria-labelledby="karyawan" aria-hidden="true" style="padding-right: 50%">
  <div class="modal-dialog modal-notice modal-lg" style="width: 80%;">
    <div class="modal-content" style="width: 150%;">
      <div class="modal-header">
        <h5 class="modal-title" id="karyawan">Inquery Data Karyawan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
          <i class="material-icons">close</i>
        </button>
      </div>
      <div class="modal-body">
        <div class="instruction">
          <div class="row">
            <div class="col-md-12">
              <div class="material-datatables">
                <table class="table table-striped table-no-bordered table-hover datatables-new" cellspacing="0" width="100%" style="width:100%">
                  <thead>
                    <tr>
                      <th><i class="fa fa-hashtag"></i></th>
                      <th>No. Member</th>
                      <th>Member Name</th>
                      <th>Card No</th>
                      <th>Enroll</th>
                      <th>DOB</th>
                    </tr>
                  </thead>
                  <tbody id="list-karyawan"></tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end notice modal -->