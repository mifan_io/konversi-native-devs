<div class="content">
  <div class="content">
    <div class="container-fluid">

      <div class="row">
        <div class="col-md-12">
          <div class="card">

            <div class="card-header card-header-tabs card-header-primary">
              <div class="nav-tabs-navigation">
                <div class="nav-tabs-wrapper">
                  <ul class="nav nav-tabs" data-tabs="tabs">

                  <?php $i = 0 ; foreach ($tab as $key => $val) {
                    if (($data['mmbr_bpjs_status'] == 'Y' && $val == 'BPJS') || ($val != 'BPJS')) { ?>
                    <li class="nav-item">
                      <a class="nav-link <?php echo ($i == 0) ? 'active' : '' ; ?>" href="#link<?php echo $i; ?>" data-toggle="tab">
                        <i class="material-icons"><?php echo $logo[$i]; ?></i> <?php echo $val; ?>
                        <div class="ripple-container"></div>
                      </a>
                    </li>
                    <?php } $i++; } ?>
                  </ul>
                </div>
              </div>
            </div>

            <div class="card-body ">
              <div class="tab-content tab-space">

                <div class="tab-pane active" id="link0">
                  <div class="row">
                    <div class="col-md-6">
                      <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                        <tbody>
                          <tr>
                            <th>Name</th>
                            <td><?php echo $data['mmbr_name']; ?></td>
                          </tr>
                          <tr>
                            <th>Card No</th>
                            <td><?php echo $data['mmbr_card_nomor']; ?></td>
                          </tr>
                          <tr>
                            <th>NIK</th>
                            <td><?php echo $data['mmbr_employee_id']; ?></td>
                          </tr>
                          <tr>
                            <th>Code</th>
                            <td><?php echo $enroll[$data['mmbr_enrol_code']]; ?></td>
                          </tr>
                          <tr>
                            <th>Sex</th>
                            <td><?php echo $sex[$data['mmbr_sex']]; ?></td>
                          </tr>
                          <tr>
                            <th>Status</th>
                            <td><?php echo $marital[$data['mmbr_marital_status']]; ?></td>
                          </tr>
                          <tr>
                            <th>DOB</th>
                            <td><?php echo get_indonesian_simple_date($data['mmbr_dob']); ?></td>
                          </tr>
                          <tr>
                            <th>Age</th>
                            <td><?php echo $data['mmbr_age']; ?></td>
                          </tr>
                          <tr>
                            <th>Benefit</th>
                            <td><?php echo $data['prdclass_plan_name']; ?></td>
                          </tr>
                          <tr>
                            <th>Cabang</th>
                            <td><?php echo $data['mmbr_cabang']; ?></td>
                          </tr>
                          <tr>
                            <th>VIP</th>
                            <td><?php echo $data['mmbr_vip']; ?></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="col-md-6">
                      <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                        <tbody>
                          <tr>
                            <th>Class Benefit</th>
                            <td>Class <?php echo $data['mmbr_class_code']; ?></td>
                          </tr>
                          <tr>
                            <th>Start Service</th>
                            <td><?php echo get_indonesian_simple_date($data['mmbr_start_date']); ?></td>
                          </tr>
                          <tr>
                            <th>End Service</th>
                            <td><?php echo get_indonesian_simple_date($data['mmbr_end_date']); ?></td>
                          </tr>
                          <tr>
                            <th>Terminate Service</th>
                            <td><?php echo get_indonesian_simple_date($data['mmbr_terminate_date']); ?></td>
                          </tr>
                          <tr>
                            <th>Terminate Reason</th>
                            <td><?php echo $data['mmbr_terminate_reason']; ?></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>

                <div class="tab-pane" id="link1">
                  <div class="material-datatables">
                    <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                      <thead>
                        <tr>
                          <th class="text-center"><a href="#" onclick="tableToExcel('groupExcel', 'Member Group');"><i class="text-success fa fa-file-excel-o"></i></a></th>
                          <th>Name</th>
                          <th>Card No</th>
                          <th>DOB</th>
                          <th>Age</th>
                          <th>Sex</th>
                          <th>Enroll</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php foreach ($family as $key => $val){ ?>
                        <tr>
                          <td><?php echo ($key + 1); ?></td>
                          <td><?php echo $val['mmbr_name']; ?></td>
                          <td><?php echo $val['mmbr_card_nomor']; ?></td>
                          <td><?php echo get_indonesian_simple_date($val['mmbr_dob']); ?></td>
                          <td><?php echo $val['mmbr_age']; ?></td>
                          <td><?php echo $sex[$val['mmbr_sex']]; ?></td>
                          <td><?php echo $enroll[$val['mmbr_enrol_code']]; ?></td>
                        </tr>
                      <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>

                <div class="tab-pane" id="link2">
                  <div class="material-datatables">
                    <table class="table table-striped table-no-bordered table-hover" id="highestExcel" cellspacing="0" width="100%" style="width:100%">
                      <thead>
                        <tr>
                          <th class="text-center"><a href="#" onclick="tableToExcel('highestExcel', '20 Highest Claim');"><i class="text-success fa fa-file-excel-o"></i></a></th>
                          <th>Benefit Code</th>
                          <th>Benefit Description</th>
                          <th>Category</th>
                          <th>Amount Limit</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php $ben = ''; $i = 1; foreach ($data['benefit_data'] as $key => $val){ ?>
                      <?php if($ben == '' || $ben != $val['description']){ ?>
                        <tr><td colspan="5" class="table-info"><?php echo $val['description']; ?></td></tr>
                      <?php $i = 1; $ben = $val['description']; } ?>
                        <tr>
                          <td><?php echo $i++; ?></td>
                          <td><?php echo $val['prdplan_ben_code']; ?></td>
                          <td><?php echo $val['mbi_ben_description']; ?></td>
                          <td><?php echo $category[$val['prdplan_category']]; ?></td>
                          <td class="rupiah text-right"><?php echo $val['prdplan_benefit_amount']; ?></td>
                        </tr>
                      <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>

                <div class="tab-pane" id="link3">
                  <div class="material-datatables">
                    <table class="table table-striped table-no-bordered table-hover" id="deseasesExcel" cellspacing="0" width="100%" style="width:100%">
                      <thead>
                        <tr>
                          <th class="text-center"><a href="#" onclick="tableToExcel('deseasesExcel', 'Common Diseases');"><i class="text-success fa fa-file-excel-o"></i></a></th>
                          <th>Benefit Code</th>
                          <th>Benefit Description</th>
                          <th>Limit</th>
                          <th>Sisa Limit</th>
                          <th>Last Claim</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php $ben = ''; $i = 1; foreach ($data['plafond'] as $key => $val){ ?>
                      <?php if($ben == '' || $ben != $val['description']){ ?>
                        <tr><td colspan="6" class="table-info"><?php echo $val['description']; ?></td></tr>
                      <?php $i = 1; $ben = $val['description']; } ?>
                        <tr>
                          <td><?php echo $i++; ?></td>
                          <td><?php echo $val['mbl_benefit_code']; ?></td>
                          <td><?php echo $val['mbi_ben_description']; ?></td>
                          <td class="rupiah text-right"><?php echo $val['mbl_benefit_limit']; ?></td>
                          <td class="rupiah text-right"><?php echo $val['mbl_benefit_balance']; ?></td>
                          <td><?php echo get_indonesian_simple_date($val['mbl_claim_last_date']); ?></td>
                        </tr>
                      <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>

                <div class="tab-pane" id="link4">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="material-datatables">
                        <table class="table table-striped table-no-bordered table-hover" id="visitExcel" cellspacing="0" width="100%" style="width:100%">
                          <tbody>
                            <tr>
                              <th>Card Number</th>
                              <td><?php echo $data['mmbr_bpjs_nomor']; ?></td>
                            </tr>
                            <tr>
                              <th>Faskes 1 Code</th>
                              <td><?php echo $data['mmbr_bpjs_ppk1_code']; ?></td>
                            </tr>
                            <tr>
                              <th>Faskes 1 Description</th>
                              <td><?php echo $data['mmbr_bpjs_ppk1_desc']; ?></td>
                            </tr>
                            <tr>
                              <th>Class</th>
                              <td><?php echo $data['mmbr_bpjs_class']; ?></td>
                            </tr>
                            <tr>
                              <th>Premi BPJS</th>
                              <td><?php echo $data['mmbr_bpjs_premi']; ?></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="material-datatables">
                        <table class="table table-striped table-no-bordered table-hover" id="visitExcel" cellspacing="0" width="100%" style="width:100%">
                          <thead>
                            <tr>
                              <th class="text-center"><a href="#" onclick="tableToExcel('visitExcel', '20 Highest Visit');"><i class="text-success fa fa-file-excel-o"></i></a></th>
                              <th>Month</th>
                              <th>Payment Date</th>
                              <th>Class</th>
                              <th>Payment By</th>
                              <th>Amount</th>
                            </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($data['bpjs'] as $key => $val) { ?>
                            <tr>
                              <td><?php echo $val['pbpjs_periode_month']; ?></td>
                              <td><?php echo get_indonesian_simple_date($val['pbbjs_payment_date']); ?></td>
                              <td><?php echo $val['pbbjs_class']; ?></td>
                              <td><?php echo $val['pbbjs_payment_by']; ?></td>
                              <td><?php echo $val['pbbjs_payment_amount']; ?></td>
                            </tr>
                          <?php } ?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>

          </div>
        </div>
      </div>

    </div>
  </div>
</div>


<!-- notice modal -->
<div class="modal fade bs-example-modal-lg" id="noticeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="padding-right: 50%">
  <div class="modal-dialog modal-notice modal-lg" style="width: 80%;">
    <div class="modal-content" style="width: 150%;">
      <div class="modal-header">
        <h5 class="modal-title" id="myModalLabel">Inquery Data Policy</h5>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
          <i class="material-icons">close</i>
        </button>
      </div>
      <div class="modal-body">
        <div class="instruction">
          <div class="row">
            <div class="col-md-12">
              <div class="material-datatables">
                <table class="table table-striped table-no-bordered table-hover datatables" cellspacing="0" width="100%" style="width:100%">
                  <thead>
                    <tr>
                      <th class="text-center"><i class="fa fa-hashtag"></i></th>
                      <th class="text-center">No. Polis</th>
                      <th class="text-center">Policy Name</th>
                      <th class="text-center">Start Date</th>
                      <th class="text-center">End Date</th>
                      <th class="text-center">Member(s)</th>
                      <th class="text-center">Status</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php foreach ($data as $key => $val) { ?>
                    <tr>
                      <td><?php echo ($key + 1); ?></td>
                      <td>
                        <a data-dismiss="modal" href="#" onclick="display('.benefit-option'); binding('#benefit<?php echo $key; ?>'); _reportAll('report/data', '<?php echo $val['pol_pol_no']; ?>', Start.value, End.value, benefit.value, provider.value); chainedNull('report/provider', '<?php echo $val['pol_pol_no']; ?>', '#provider'); chainedNull('report/benefit', '<?php echo $val['pol_pol_no']; ?>', '#benefit');">
                          <?php echo $val['pol_pol_no']; ?>
                          </a>
                      </td>
                      <td><?php echo $val['pol_company_name']; ?></td>
                      <td><?php echo get_indonesian_simple_date($val['pol_start_date']); ?></td>
                      <td><?php echo get_indonesian_simple_date($val['pol_end_date']); ?></td>
                      <td class="text-right"><?php echo $val['pol_member']; ?></td>
                      <td>
                        <?php echo $val['pol_status']; ?>
                        <input type="hidden" id="benefit<?php echo $key; ?>" value='<?php echo json_encode($val); ?>'>
                      </td>
                    </tr>
                  <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end notice modal -->