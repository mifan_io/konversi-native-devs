<div class="content">
  <div class="container-fluid">

    <?php if (isset($_SESSION['users_input']) && $_SESSION['users_input'] == true) { ?>
    <div class="alert alert-<?php echo $_SESSION['users_status']; ?>">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <i class="material-icons">close</i>
      </button>
      <span><b class="text-capitalize"> <?php echo $_SESSION['users_status']; ?> - </b> <?php echo $_SESSION['users_text']; ?></span>
    </div>
    <?php } unset($_SESSION['users_input']); unset($_SESSION['users_status']); unset($_SESSION['users_text']); ?>

    <div class="col-md-12 mr-auto ml-auto">
      <div class="wizard-container">
        <div class="card card-wizard active" data-color="purple" id="wizardProfile">
          <form action="" method="post">
            <!--        You can switch " data-color="primary" "  with one of the next bright colors: "green", "orange", "red", "blue"       -->
            <div class="card-header text-center">
              <h3 class="card-title">Member Policy</h3>
            </div>

            <div class="wizard-navigation">
              <ul class="nav nav-pills">
                <li class="nav-item" style="width: 25%;">
                  <a class="nav-link active" href="#about" data-toggle="tab" role="tab">Profile</a>
                </li>
                <li class="nav-item" style="width: 25%;">
                  <a class="nav-link" href="#member" data-toggle="tab" role="tab">Membership</a>
                </li>
                <li class="nav-item" style="width: 25%;">
                  <a class="nav-link" href="#account" data-toggle="tab" role="tab">Benefit</a>
                </li>
                <li class="nav-item" style="width: 25%;">
                  <a class="nav-link" href="#address" data-toggle="tab" role="tab">Finance</a>
                </li>
              </ul>
              <!-- <div class="moving-tab" style="width: 219.109px; transform: translate3d(-8px, 0px, 0px); transition: transform 0s ease 0s;">About</div> -->
            </div>

            <div class="card-body">
              <div class="tab-content">

                <div class="tab-pane active" id="about">
                  <h5 class="info-text">Profile</h5>
                  <div class="row justify-content-center">
                    <div class="col-md-12">

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <div class="input-group no-border">
                            <input type="text" id="no" class="form-control" placeholder="Nomor Polis" readonly="readonly" required="required">
                            <button onclick="large('noticeModal');" type="button" data-toggle="modal" class="btn btn-white btn-round btn-just-icon" data-target="#noticeModal">
                              <i class="material-icons">search</i>
                              <div class="ripple-container"></div>
                            </button>
                          </div>
                        </div>
                        <div class="form-group">Periode Polis (<span id="periode"></span>)</div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <div class="input-group no-border">
                            <input type="text" id="nik" class="form-control" placeholder="Karyawan [Jika Penambahan Anggota Keluarga]" readonly="readonly">
                            <button type="button" data-toggle="modal" class="btn btn-white btn-round btn-just-icon" data-target="#Mkaryawan" onclick="large('Mkaryawan');">
                              <i class="material-icons">search</i>
                              <div class="ripple-container"></div>
                            </button>
                          </div>
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="row">
                          <div class="col-md-4 form-group bmd-form-group">
                            <select class="selectpicker" name="mmbr_enrol_type" id="mmbr_enrol_type" data-style="select-with-transition" title="Choose Enroll Type" data-size="7" required="required">
                            <?php foreach ($type as $key => $val) { ?>
                              <option value="<?php echo $val['vals']; ?>"><?php echo $val['label']; ?></option>
                            <?php } ?>
                            </select>
                          </div>

                          <div class="col-md-4 form-group bmd-form-group">
                            <label for="exampleInputz" class="bmd-label-floating">Endors Date</label>
                            <input type="text" class="form-control datepicker" value="<?php echo date('d-m-Y'); ?>" id="exampleInputz" name="mmbr_endorsment_date" required="required">
                          </div>

                          <div class="col-md-4 form-group bmd-form-group">
                            <select class="selectpicker" name="mmbr_enrol_code" id="mmbr_enrol_code" data-style="select-with-transition" title="Choose Enroll Code" data-size="7" required="required">
                            <?php foreach ($code as $key => $val) { ?>
                              <option value="<?php echo $val['vals']; ?>"><?php echo $val['label']; ?></option>
                            <?php } ?>
                            </select>
                          </div>
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput3" class="bmd-label-floating">Name</label>
                          <input type="text" class="form-control" id="exampleInput3" name="mmbr_name" required="required">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput4" class="bmd-label-floating">Employee Id</label>
                          <input type="text" class="form-control" id="exampleInput4"  name="mmbr_employee_id" required="required">
                        </div>
                      </div>

                    </div>
                  </div>
                </div>

                <div class="tab-pane" id="member">
                  <h5 class="info-text">Membership</h5>
                  <div class="row justify-content-center">
                    <div class="col-md-12">

                      <div class="input-group form-control-md">
                        <div class="row">
                          <div class="col-md-4 form-group bmd-form-group">
                            <select class="selectpicker" name="mmbr_marital_status" data-style="select-with-transition" title="Choose Enroll Type" data-size="7" required="required">
                            <?php foreach ($mari as $key => $val) { ?>
                              <option value="<?php echo $val['vals']; ?>"><?php echo $val['label']; ?></option>
                            <?php } ?>
                            </select>
                          </div>

                          <div class="col-md-4 form-group bmd-form-group">
                            <label for="exampleInputq" class="bmd-label-floating">Date Of Birth</label>
                            <input type="text" class="form-control datepicker" value="<?php echo date('d-m-Y'); ?>" id="exampleInputq" name="mmbr_dob" required="required">
                          </div>

                          <div class="col-md-4 form-group bmd-form-group">
                            <select class="selectpicker" name="mmbr_sex" data-style="select-with-transition" title="Choose Enroll Code" data-size="7" required="required">
                            <?php foreach ($gender as $key => $val) { ?>
                              <option value="<?php echo $val['vals']; ?>"><?php echo $val['label']; ?></option>
                            <?php } ?>
                            </select>
                          </div>
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="mmbr_phone" class="bmd-label-floating">Phone</label>
                          <input type="text" class="form-control num" name="mmbr_phone" id="mmbr_phone" required="required">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="mmbr_handphone" class="bmd-label-floating">Handphone</label>
                          <input type="text" class="form-control num" name="mmbr_handphone" id="mmbr_handphone" required="required">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="mmbr_email" class="bmd-label-floating">Email</label>
                          <input type="email" class="form-control" name="mmbr_email" id="mmbr_email" required="required">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="mmbr_departement" class="bmd-label-floating">Department</label>
                          <input type="text" class="form-control" name="mmbr_departement" id="mmbr_departement" required="required">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="mmbr_jabatan" class="bmd-label-floating">Jabatan</label>
                          <input type="text" class="form-control" name="mmbr_jabatan" id="mmbr_jabatan" required="required">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="mmbr_cabang" class="bmd-label-floating">Cabang</label>
                          <input type="text" class="form-control" name="mmbr_cabang" id="mmbr_cabang">
                        </div>
                      </div>

                    </div>
                  </div>
                </div>

                <div class="tab-pane" id="account">
                  <h5 class="info-text">Benefit</h5>
                  <div class="row justify-content-center">
                    <div class="col-md-12">

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="exampleInput14" class="bmd-label-floating">Start Date</label>
                          <input type="text" class="form-control datepicker" id="exampleInput14" name="mmbr_start_date" required="required">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="ending" class="bmd-label-floating">End Date</label>
                          <input type="text" class="form-control" id="ending" name="mmbr_end_date" readonly="readonly">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <select class="selectpicker" name="mmbr_class_code" data-style="select-with-transition" id="benefit" title="Choose Class Benefit" data-size="7" required="required"></select>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>

                <div class="tab-pane" id="address">
                  <h5 class="info-text">Finance</h5>
                  <div class="row justify-content-center">
                    <div class="col-md-12">

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="mmbr_acc_no" class="bmd-label-floating">Acc No</label>
                          <input type="text" class="form-control num" id="mmbr_acc_no" name="mmbr_acc_no" required="required">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="mmbr_acc_name" class="bmd-label-floating">Acc Name</label>
                          <input type="text" class="form-control" id="mmbr_acc_name" name="mmbr_acc_name" required="required">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="mmbr_bank" class="bmd-label-floating">Bank Name</label>
                          <input type="text" class="form-control" id="mmbr_bank" name="mmbr_bank" required="required">
                        </div>
                      </div>

                      <div class="input-group form-control-md">
                        <div class="form-group bmd-form-group">
                          <label for="mmbr_bank_branch" class="bmd-label-floating">Bank Branch</label>
                          <input type="text" class="form-control" id="mmbr_bank_branch" name="mmbr_bank_branch">
                        </div>
                      </div>

                    </div>
                  </div>
                </div>

              </div>
            </div>
            <div class="card-footer">
              <div class="mr-auto">
                <button type="button" class="btn btn-previous btn-fill btn-rose btn-wd disabled">Previous</button>
                <!-- <input type="button" class="btn btn-previous btn-fill btn-rose btn-wd disabled" name="previous" value="Previous"> -->
              </div>
              <div class="ml-auto">
                <input type="hidden" id="check">
                <input type="hidden" name="mmbr_pol_no" id="val">
                <input type="hidden" name="card" id="_card">
                <input type="hidden" name="member" id="_member">
                <button type="button" class="btn btn-next btn-fill btn-primary btn-wd">Next</button>
                <button type="submit" class="btn btn-finish btn-fill btn-primary btn-wd" style="display: none;">Finish</button>
                <!-- <input type="button" class="btn btn-next btn-fill btn-primary btn-wd" name="next" value="Next"> -->
                <!-- <input type="submit" class="btn btn-finish btn-fill btn-primary btn-wd" name="finish" value="Finish" style="display: none;"> -->
              </div>
              <div class="clearfix"></div>
            </div>
          </form>
        </div>
      </div>
    </div>

    <tbody id="family_list" style="display: none;"></tbody>

  </div>
</div>


<!-- notice modal -->
<div class="modal fade bs-example-modal-lg" id="noticeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="padding-right: 50%">
  <div class="modal-dialog modal-notice modal-lg" style="width: 80%;">
    <div class="modal-content" style="width: 150%;">
      <div class="modal-header">
        <h5 class="modal-title" id="myModalLabel">Inquery Data Policy</h5>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
          <i class="material-icons">close</i>
        </button>
      </div>
      <div class="modal-body">
        <div class="instruction">
          <div class="row">
            <div class="col-md-12">
              <div class="material-datatables">
                <table class="table table-striped table-no-bordered table-hover datatables" cellspacing="0" width="100%" style="width:100%">
                  <thead>
                    <tr>
                      <th><i class="fa fa-hashtag"></i></th>
                      <th>No. Polis</th>
                      <th>Policy Name</th>
                      <th>Start Date</th>
                      <th>End Date</th>
                      <th>Member(s)</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php foreach ($data as $key => $val) { ?>
                    <tr>
                      <td><?php echo ($key + 1); ?></td>
                      <td>
                        <a data-dismiss="modal" href="#" onclick="binding('#benefit<?php echo $key; ?>'); code('policy/code', '<?php echo $val['pol_pol_no']; ?>', '#code'); karyawan('member/karyawan', '<?php echo $val['pol_pol_no']; ?>', '#list-karyawan'); chained('member/benefit_data', '<?php echo $val['pol_pol_no']; ?>', '#benefit'); display('.benefit-option');">
                          <?php echo $val['pol_pol_no']; ?>
                          </a>
                      </td>
                      <td><?php echo $val['pol_company_name']; ?></td>
                      <td><?php echo $val['pol_start_date']; ?></td>
                      <td><?php echo $val['pol_end_date']; ?></td>
                      <td class="text-right"><?php echo $val['pol_member']; ?></td>
                      <td>
                        <?php echo $val['pol_status']; ?>
                        <input type="hidden" id="benefit<?php echo $key; ?>" value='<?php echo json_encode($val); ?>'>
                      </td>
                    </tr>
                  <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end notice modal -->

<!-- notice modal -->
<div class="modal fade bs-example-modal-lg" id="Mkaryawan" tabindex="-1" role="dialog" aria-labelledby="karyawan" aria-hidden="true" style="padding-right: 50%">
  <div class="modal-dialog modal-notice modal-lg" style="width: 80%;">
    <div class="modal-content" style="width: 150%;">
      <div class="modal-header">
        <h5 class="modal-title" id="karyawan">Inquery Data Karyawan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
          <i class="material-icons">close</i>
        </button>
      </div>
      <div class="modal-body">
        <div class="instruction">
          <div class="row">
            <div class="col-md-12">
              <div class="material-datatables">
                <table class="table table-striped table-no-bordered table-hover datatables-new" cellspacing="0" width="100%" style="width:100%">
                  <thead>
                    <tr>
                      <th><i class="fa fa-hashtag"></i></th>
                      <th>No. Member</th>
                      <th>Member Name</th>
                      <th>Card No</th>
                      <th>Enroll</th>
                      <th>DOB</th>
                    </tr>
                  </thead>
                  <tbody id="list-karyawan"></tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end notice modal -->