<div class="content">
  <div class="content">
    <div class="container-fluid">

      <div class="row">
      <?php foreach ($data as $key => $val) { ?>

        <div class="col-lg-3 col-md-6 col-sm-6">
          <div class="card card-stats">
            <div class="card-header card-header-<?php echo $head[$key % 4]; ?> card-header-icon">
              <div class="card-icon">
                <i class="material-icons">local_hospital</i>
              </div>
              <p class="card-category"><?php echo $val['mprov_name']; ?></p>
              <h3 class="card-title"><?php echo $val['mprov_prov_code']; ?></h3>
            </div>
            <div class="card-footer">
              <div class="stats">
                <a href="#pablo"><?php echo $val['mprov_last_login_date']; ?></a>
              </div>
            </div>
          </div>
        </div>

      <?php } ?>
      </div>

    </div>
  </div>
</div>