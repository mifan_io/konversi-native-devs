<?php
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Provider extends REST_Controller {

  function __construct(){
    parent::__construct();

    $this->load->model('Menu_model');
    $this->load->model('Provider_model');
    $this->load->library('session');
    $this->load->helper('url_helper');
    $this->load->helper('url');

    if (empty($_SESSION['username'])) {
      redirect(base_url());
    }
  }

  function fetch_class(){
    return $this->router->fetch_class();
  }

  function fetch_method(){
    return $this->router->fetch_method();
  }

  function index_get(){

    $header['supmenu'] = 'Provider';
    $header['submenu'] = 'Provider Relation';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu($_SESSION['username']);

    $tr = '';
    $data = $this->Provider_model->index();
    $text = ['N' => 'rose', 'I' => 'primary', 'T' => 'info', 'S' => 'danger', 'W' => 'warning'];
    $status = ['N' => 'BELUM INSTALL', 'I' => 'INSTALL GUIDE', 'T' => 'INSTALL TRAINING', 'S' => 'STOP LAYANAN', 'W' => 'WARNING SECURITY'];

    foreach ($data as $key => $val) {
      $tr .= '<tr><td><small>' . ($key + 1) . '</small></td>';
      $tr .= '<td><small>' . $val['mprov_prov_code'] . '</small></td>';
      $tr .= '<td><small>' . $val['mprov_name'] . '</small></td>';
      $tr .= '<td><small>' . $val['mco_description'] . '</small></td>';
      $tr .= '<td><small>' . $val['mprov_city'] . '</small></td>';
      $tr .= '<td><small>' . $val['mprov_telephone'] . '</small></td>';
      $tr .= '<td><small>' . $val['mpg_group_name'] . '</small></td>';
      $tr .= '<td><small class="text-' . $text[$val['mprov_layanan_system']] . '">' . $status[$val['mprov_layanan_system']] . '</small></td></tr>';
    }

    $data['html'] = $tr;

    $this->load->view('header', $header);
    $this->load->view('provider/index', $data);
    $this->load->view('footer');
  }

  function group_get(){

    $header['supmenu'] = 'Provider';
    $header['submenu'] = 'Provider Group';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu($_SESSION['username']);

    $data['data'] = $this->Provider_model->group();

    $this->load->view('header', $header);
    $this->load->view('provider/group', $data);
    $this->load->view('footer');
  }

}