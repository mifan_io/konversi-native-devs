<?php
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Finance extends REST_Controller {

  function __construct(){
    parent::__construct();

    $this->load->model('Menu_model');
    $this->load->model('Finance_model');
    $this->load->library('session');
    $this->load->helper('url_helper');
    $this->load->helper('url');

    if (empty($_SESSION['username'])) {
      redirect(base_url());
    }
  }

  function fetch_class(){
    return $this->router->fetch_class();
  }

  function fetch_method(){
    return $this->router->fetch_method();
  }

  function index_get(){

    $header['supmenu'] = 'Finance';
    $header['submenu'] = 'Invoice Membership';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu($_SESSION['username']);

    $this->load->view('header', $header);
    $this->load->view('content');
    $this->load->view('footer');
  }

  function panel_get(){
    $header['supmenu'] = 'Finance';
    $header['submenu'] = 'Pay Bills - Claim';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu($_SESSION['username']);

    $this->load->view('header', $header);
    $this->load->view('content');
    $this->load->view('footer');
  }

  function membership_get(){
    $header['supmenu'] = 'Finance';
    $header['submenu'] = 'Receive Pay - Membership';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu($_SESSION['username']);

    $this->load->view('header', $header);
    $this->load->view('content');
    $this->load->view('footer');
  }

  function eoc_get(){
    $header['supmenu'] = 'Finance';
    $header['submenu'] = 'Receive Pay - EOC';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu($_SESSION['username']);

    $this->load->view('header', $header);
    $this->load->view('content');
    $this->load->view('footer');
  }

  function paid_get(){
    $header['supmenu'] = 'Finance';
    $header['submenu'] = 'Update Paid - Manual Upload';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu($_SESSION['username']);

    $this->load->view('header', $header);
    $this->load->view('content');
    $this->load->view('footer');
  }


}