<?php
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Claim extends REST_Controller {

  function __construct(){
    parent::__construct();

    $this->load->model('Menu_model');
    $this->load->model('Claim_model');
    $this->load->library('session');
    $this->load->helper('url_helper');
    $this->load->helper('url');

    if (empty($_SESSION['username'])) {
      redirect(base_url());
    }
  }

  function fetch_class(){
    return $this->router->fetch_class();
  }

  function fetch_method(){
    return $this->router->fetch_method();
  }

  function dates($date){
    $date = date("Y-m-d",strtotime($date));
    return $date;
  }

  function status(){
    return ['IN' => '<span class="text-success">Active</span>', 'SU' => '<span class="text-rose">Not Active</span>', 'MA' => '<span class="text-primary">Mature</span>', 'PA' => '<span class="text-warning">Prop. Approve</span>', 'TR' => '<span class="text-danger">Terminate</span>'];
  }

  function provider(){
    return $this->Claim_model->provider();
  }

  function registered(){
    return $this->Claim_model->read('claim_register', 'WHERE clmreg_register_status NOT IN ("F","Y","V") ORDER BY clmreg_stamp_claim DESC', 'clmreg_register_no, clmreg_claim_type, clmreg_claimer_code, clmreg_claimer_name, clmreg_invoice_no, clmreg_invoice_date, clmreg_claim_amount, clmreg_receive_date, clmreg_process_date, clmreg_claim_approve, clmreg_claim_excess, clmreg_claim_exgratia, clmreg_finish_date, clmreg_sla_days, clmreg_sla_finish_date, clmreg_register_status, clmreg_claim_proposed');
  }

  function claimer($no, $reg){
    return $this->Claim_model->read($no . '_claim', 'JOIN ' . $no . '_mst_member ON clm_policy_no = mmbr_pol_no AND clm_member_no = mmbr_member_no WHERE clm_register_no = "' . $reg . '"AND clm_status = "1" AND clm_paid_status = "N"');
  }

  function external(){
    return $this->Claim_model->read('callcenter_check_in', 'WHERE ci_source = "PV" AND ci_approve_claim = "" AND ci_status_reff = "OUT" ORDER BY callcenter_check_in.ci_stamp DESC', 'ci_reff_no, ci_date, ci_service_out, ci_provider_code, ci_provider_name, ci_policy_no, ci_policy_name, ci_member_no, ci_card_no, ci_member_name, ci_benefit_type, ci_icd, ci_icd_desc, ci_proposed_amount, ci_approved_amount, ci_excess_member, ci_excess_ssa, ci_guarantee_letter, ci_source');
  }

  function detail_external($ref, $no){
    $external = $this->Claim_model->external($ref, $no);
    $external['detail'] = $this->Claim_model->detail($ref, $external['ci_benefit_type']);
    return $external;
  }

  function policy($where = "WHERE pol_status = 'IN'"){
    $data = $this->Claim_model->policy($where);
    $status = $this->status();

    foreach ($data as $key => &$val) {
      $val['end'] = $val['pol_end_date'];
      $val['start'] = $val['pol_start_date'];
      $val['pol_status'] = $status[$val['pol_status']];
      $val['pol_member'] = number_format($val['pol_member'], 0, ',', '.');
      $val['pol_end_date'] = get_indonesian_simple_date($val['pol_end_date']);
      $val['pol_start_date'] = get_indonesian_simple_date($val['pol_start_date']);
    }

    return $data;
  }

  function index_get(){

    $header['supmenu'] = 'Claim';
    $header['submenu'] = 'Claim';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu($_SESSION['username']);

    $this->load->view('header', $header);
    $this->load->view('content');
    $this->load->view('footer');
  }

  function register_post(){
    extract($_POST);

    $_POST['clmreg_invoice_date'] = $this->dates($clmreg_invoice_date);
    $_POST['clmreg_receive_date'] = $this->dates($clmreg_receive_date);

    $finish = strtotime ($clmreg_sla_days . ' day', strtotime ($clmreg_receive_date) ) ;
    $finish = date ("Y-m-j", $finish);

    $_POST['clmreg_register_no'] = $this->Claim_model->register_batch();

    $_POST['clmreg_claim_proposed'] = 0;
    $_POST['clmreg_claim_approve'] = 0;
    $_POST['clmreg_claim_excess'] = 0;
    $_POST['clmreg_claim_exgratia'] = 0;
    $_POST['clmreg_sla_finish_date'] = $finish;
    $_POST['clmreg_register_status'] = 'I';
    $_POST['clmreg_paid_status'] = 2;
    $_POST['clmreg_paid_amount'] = 0;
    $_POST['clmreg_stamp_claim'] = cdatetime;
    $_POST['clmreg_stamp'] = cdatetime;
    $_POST['clmreg_user'] = $_SESSION['username'];

    $this->Claim_model->create('claim_register', $_POST);

    $_SESSION['users_input'] = true;
    $_SESSION['users_text'] = 'Transaction Register Claim Berhasil Disimpan';
    $_SESSION['users_status'] = 'success';

    redirect(base_url() . 'claim/register');
  }

  function register_get(){
    $header['supmenu'] = 'Claim';
    $header['submenu'] = 'Register | Batch';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu($_SESSION['username']);

    $data['data'] = $this->policy();
    $data['prov'] = $this->provider();
    $data['type'] = ['P' => 'Provider', 'R' => 'Reimbursement'];

    $this->load->view('header', $header);
    $this->load->view('claim/register', $data);
    $this->load->view('footer');
  }

  function external_post(){
    extract($_POST);

    $upclaim['ci_approve_claim'] = 'Y';
    $upclaim['ci_approve_date'] = cdatetime;
    $this->Claim_model->update('callcenter_check_in', 'ci_reff_no = "' . $ci_reff_no . '"', $upclaim);

    $_SESSION['users_input'] = true;
    $_SESSION['users_text'] = 'Claim Analyst No.Reff ' . $ci_reff_no . ' Berhasil';
    $_SESSION['users_status'] = 'success';

    redirect(base_url() . 'claim/approval');
  }

  function external_data_post(){
    $data = $this->detail_external($_POST['param'], $_POST['param2']);
    $response = (empty($data)) ? 204 : 200 ;
    json_output($response, $data);
  }

  function approval_data_post(){
    // claimer($no, $reg)
    $data = $this->claimer($_POST['param'], $_POST['param2']);
    $response = (empty($data)) ? 204 : 200 ;
    json_output($response, $data);
  }

  function approval_get(){
    $header['supmenu'] = 'Claim';
    $header['submenu'] = 'Approval Claim';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu($_SESSION['username']);

    $data['exte'] = $this->external();
    $data['claim'] = $this->registered();
    $data['type'] = ['P' => 'Provider', 'R' => 'Reimbursement'];

    $this->load->view('header', $header);
    $this->load->view('claim/approval', $data);
    $this->load->view('footer');
  }

  function voucher_get(){
    $header['supmenu'] = 'Claim';
    $header['submenu'] = 'Voucher Claim';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu($_SESSION['username']);

    $this->load->view('header', $header);
    $this->load->view('content');
    $this->load->view('footer');
  }

  function preview_data_post(){
    $data = $this->Claim_model->read($_POST['param'] . '_claim', 'WHERE clm_claim_code <> "" ORDER BY clm_stamp DESC LIMIT 200', 'clm_claim_code, clm_register_no, clm_provider_code, clm_provider_name, clm_member_no, clm_member_name, clm_input_source, clm_claim_type, clm_benefit_type, clm_service_in, clm_service_out, clm_proposed_amount, clm_approved_amount, clm_excess_amount, clm_cob_amount, clm_icd1, clm_icd_desc1, clm_status, clm_paid_status, clm_claim_date, clm_note_claim');
    $response = (empty($data)) ? 204 : 200 ;
    json_output($response, $data);
  }

  function preview_get(){
    $header['supmenu'] = 'Claim';
    $header['submenu'] = 'Preview Claim';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu($_SESSION['username']);

    $data['data'] = $this->policy();

    $this->load->view('header', $header);
    $this->load->view('claim/preview', $data);
    $this->load->view('footer');
  }

  function productivity_get(){
    $header['supmenu'] = 'Claim';
    $header['submenu'] = 'Productivity Analyst';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu($_SESSION['username']);

    $this->load->view('header', $header);
    $this->load->view('content');
    $this->load->view('footer');
  }


}