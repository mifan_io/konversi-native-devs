<?php
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Bank extends REST_Controller {

  function __construct(){
    parent::__construct();

    $this->load->model('Bank_model');
    $this->load->library('session');
    $this->load->helper('url_helper');
    $this->load->helper('url');

    if (empty($_SESSION['username'])) {
      redirect(base_url());
    }
  }

  function index_post(){
    $plan = $this->Bank_model->bank();
    $response = (empty($plan)) ? 204 : 200 ;
    json_output($response, $plan);
  }

}