<?php
use Restserver\Libraries\REST_Controller;

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Member extends REST_Controller {

  function __construct(){
    parent::__construct();

    $this->load->model('Menu_model');
    $this->load->model('Member_model');
    $this->load->library('session');
    $this->load->helper('url_helper');
    $this->load->helper('url');

    if (empty($_SESSION['username'])) {
      redirect(base_url());
    }
  }

  function fetch_class(){
    return $this->router->fetch_class();
  }

  function policy($where = null, $limit = null){
    $cols = 'pol_pol_no, pol_member, pol_status,
             pol_service_level_day, pol_company_name,
             pol_product_code, pol_start_date,
             pol_fee_tpa, pol_fee_card, pol_end_date';
    $data = $this->Member_model->read('policy', $where . 'ORDER BY pol_company_name ASC' . $limit, $cols);
    $status = $this->status();

    foreach ($data as $key => &$val) {
      $val['end'] = $val['pol_end_date'];
      $val['start'] = $val['pol_start_date'];
      $val['pol_status'] = $status[$val['pol_status']];
      $val['pol_member'] = number_format($val['pol_member'], 0, ',', '.');
      $val['pol_end_date'] = get_indonesian_simple_date($val['pol_end_date']);
      $val['pol_start_date'] = get_indonesian_simple_date($val['pol_start_date']);
    }

    return $data;
  }

  function fetch_method(){
    return $this->router->fetch_method();
  }

  function status(){
    return ['IN' => '<span class="text-success">Active</span>', 'SU' => '<span class="text-rose">Not Active</span>', 'MA' => '<span class="text-primary">Mature</span>', 'PA' => '<span class="text-warning">Prop. Approve</span>', 'TR' => '<span class="text-danger">Terminate</span>'];
  }

  function dates($date){
    $date = date("Y-m-d",strtotime($date));
    return $date;
  }

  function karyawan_post(){
    $data = $this->Member_model->karyawan($_POST['param']);
    $response = (empty($data)) ? 204 : 200 ;
    json_output($response, $data);
  }

  function family_post(){
    $data = $this->Member_model->family($_POST['param'], $_POST['param2']);
    $response = (empty($data)) ? 204 : 200 ;
    json_output($response, $data);
  }

  function member_post(){
    $users = $this->Member_model->member($_POST['param'], $_POST['param2']);
    $response = (empty($users)) ? 204 : 200 ;
    json_output($response, $users);
  }

  function index_post(){
    extract($_POST);

    $child = ['SPO' => 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
    $pass = 'adzhwa' . $mmbr_employee_id . '9113ssa' . $mmbr_employee_id . '086666!@#$%^&*()ClUmSyArt';
    $password = md5(md5($pass));
    $limit = [];

    $_POST['mmbr_age'] = date_diff(date_create($mmbr_dob), date_create('today'))->y;
    $_POST['mmbr_personal_last_date'] = '0000-00-00';
    $_POST['mmbr_personal_password'] = $password;
    $_POST['mmbr_user'] = $_SESSION['username'];
    $_POST['mmbr_payment_date'] = '0000-00-00';
    $_POST['mmbr_personal_access'] = '1';
    $_POST['mmbr_personal_login'] = '0';
    $_POST['mmbr_stamp'] = cdatetime;
    $_POST['mmbr_status'] = '1';
    $_POST['mmbr_vip'] = 'N';

    $_POST['mmbr_dob'] = $this->dates($mmbr_dob);
    $_POST['mmbr_start_date'] = $this->dates($mmbr_start_date);
    $_POST['mmbr_endorsment_date'] = $this->dates($mmbr_endorsment_date);

    $member = (empty($member)) ? '' : substr($member, 0, 7);
    $card = (empty($card)) ? '' : substr($card, 0, 15);

    if ($mmbr_enrol_code == 'EMP') {
      $_POST['mmbr_member_no'] = $this->Member_model->policy($mmbr_pol_no) . 'A';
      $_POST['mmbr_card_nomor'] = $this->Member_model->gen_card($mmbr_pol_no) . 'A';

      $valid = $this->Member_model->one($mmbr_pol_no . '_mst_member', 'WHERE mmbr_member_no = "' . $_POST['mmbr_member_no'] . '"', 'mmbr_member_no');

      if (!empty($valid)) {
        $_SESSION['users_input'] = true;
        $_SESSION['users_text'] = 'Data Member Sudah Ada';
        $_SESSION['users_status'] = 'danger';

        redirect(base_url() . 'member');
      }
    } else {
      $code = ($mmbr_enrol_code == 'SPO') ? 'SPO' : $this->Member_model->child($mmbr_pol_no, $member);
      $_POST['mmbr_member_no'] = $member . $child[$code];
      $_POST['mmbr_card_nomor'] = $card . $child[$code];
    }

    $data = $this->Member_model->limit($mmbr_pol_no, $mmbr_class_code, $_POST['mmbr_member_no']);

    foreach ($data as $key => $plan) {
      $limit[$key]['mbl_pol_no'] = $mmbr_pol_no;
      $limit[$key]['mbl_member_no'] = $_POST['mmbr_member_no'];
      $limit[$key]['mbl_benefit_type'] = $plan['prdplan_ben_type'];
      $limit[$key]['mbl_benefit_code'] = $plan['prdplan_ben_code'];
      $limit[$key]['mbl_benefit_limit_category'] = $plan['prdplan_category'];
      $limit[$key]['mbl_benefit_limit'] = $plan['prdplan_benefit_amount'];
      $limit[$key]['mbl_max_seq'] = $plan['prdplan_frequently'];
      $limit[$key]['mbl_max_day'] = $plan['prdplan_case_disability'];
      $limit[$key]['mbl_benefit_covered'] = $plan['prdplan_covered'];
      $limit[$key]['mbl_benefit_proposed'] = 0;
      $limit[$key]['mbl_benefit_approved'] = 0;
      $limit[$key]['mbl_benefit_excess'] = 0;
      $limit[$key]['mbl_benefit_balance'] = ($plan['family'] > 1) ? $plan['prdplan_benefit_amount'] * $plan['family'] : $plan['prdplan_benefit_amount'];
      $limit[$key]['mbl_stamp'] = cdatetime;
      $limit[$key]['mbl_user'] = $_SESSION['username'];

      $item = substr($plan['prdplan_ben_code'], 1, 4);

      if(($plan['pol_family_benefit'] == "Y") AND ($item == "9999") AND ($plan['family'] > "1") AND ($plan['benfac_benefit_amount'] == "1")) {

        $this->Member_model->custom('UPDATE ' . $mmbr_pol_no . '_mst_member_limit SET mbl_benefit_balance = mbl_benefit_balance + ' . $plan['prdplan_benefit_amount'] . ' WHERE mbl_member_no LIKE "%' . substr($member, 0, 7) . '%" AND mbl_benefit_type = "' . $ftplan['prdplan_ben_type'] . '" AND mbl_benefit_code LIKE "%9999%"');

      }
    }

    unset($_POST['card']);
    unset($_POST['member']);

    $this->Member_model->create($mmbr_pol_no . '_mst_member', $_POST);
    $this->Member_model->create_batch($mmbr_pol_no . '_mst_member_limit', $limit);
    $this->Member_model->custom('UPDATE policy SET pol_stock_card = pol_stock_card - 1, pol_member = pol_member + 1 WHERE pol_pol_no = "' . $_POST['mmbr_pol_no'] . '"');

    /*echo "<pre>";
    print_r($data);
    print_r($limit);
    print_r($_POST);*/

    $_SESSION['users_input'] = true;
    $_SESSION['users_text'] = 'Member Baru Berhasil Disimpan';
    $_SESSION['users_status'] = 'success';

    redirect(base_url() . 'member');
  }

  function index_get(){
    $header['supmenu'] = 'Member';
    $header['submenu'] = 'New Member';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu($_SESSION['username']);

    $data['data'] = $this->policy("WHERE pol_status = 'IN'");
    $data['type'] = $this->Member_model->code('WHERE mco_cat_id = "10" AND mco_status = "1"');
    $data['code'] = $this->Member_model->code('WHERE mco_cat_id = "9" AND mco_status = "1"');
    $data['mari'] = $this->Member_model->code('WHERE mco_cat_id = "19" AND mco_status = "1"');
    $data['gender'] = $this->Member_model->code('WHERE mco_cat_id = "1" AND mco_status = "1"');

    $footer['js'] = 'demo.initMaterialWizard();
                 setTimeout(function () {
                   $(".card .card-wizard").addClass("active");
                 }, 600);';

    $this->load->view('header', $header);
    $this->load->view('member/form', $data);
    $this->load->view('footer', $footer);
  }

  function suspend_post(){
    $no = null;
    extract($_POST);

    foreach ($member as $key => &$val) {
      $val['mbs_suspend_date'] = $this->dates($mbs_suspend_date);
      $val['mbs_suspend_reason'] = $mbs_suspend_reason;
      $val['mbs_suspend_user'] = $_SESSION['username'];
      $val['mbs_policy_no'] = $mbs_policy_no;
      $val['mbs_stamp'] = cdatetime;

      $no[] = '"'. $val['mbs_member_no'] . '"';
    }

    if ($mbs_suspend_date <= date('d-m-Y')){
      $edit['mmbr_status'] = $mmbr_status;
    }

    if ($mmbr_status == 0){
      $edit['mmbr_end_date'] = $this->dates($mbs_suspend_date);
    }

    $edit['mmbr_terminate_reason'] = $mbs_suspend_reason;
    $edit['mmbr_terminate_user'] = $_SESSION['username'];
    $edit['mmbr_terminate_date'] = $this->dates($mbs_suspend_date);

    $this->Member_model->create_batch('member_suspend_history', $member);
    $this->Member_model->update($mbs_policy_no . '_mst_member', 'mmbr_pol_no = "' . $mbs_policy_no . '" AND mmbr_member_no IN (' . implode(', ', $no) . ')', $edit);

    $_SESSION['users_input'] = true;
    $_SESSION['users_text'] = 'Data Member Berhasil Suspend';
    $_SESSION['users_status'] = 'success';

    redirect(base_url() . 'member/suspend');
  }

  function suspend_get(){
    $header['supmenu'] = 'Member';
    $header['submenu'] = 'Member Suspend';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu($_SESSION['username']);

    $data['type'] = [0 => 'Terminate', 2 => 'Suspend'];
    $data['data'] = $this->policy("WHERE pol_status = 'IN'");

    $this->load->view('header', $header);
    $this->load->view('member/suspend', $data);
    $this->load->view('footer');
  }

  function suspend_data_post(){
    $data = $this->Member_model->suspend($_POST['param']);
    $response = (empty($data)) ? 204 : 200 ;
    json_output($response, $data);
  }

  function reactive_post(){
    $no = null;

    $member['mmbr_status'] = 1;
    $member['mmbr_terminate_user'] = null;
    $member['mmbr_terminate_reason'] = null;
    $member['mmbr_end_date'] = $_POST['end'];
    $member['mmbr_terminate_date'] = '0000-00-00 00:00:00';

    $active['mbs_reactive_user'] = $_SESSION['username'];
    $active['mbs_reactive_date'] = $this->dates($_POST['mbs_reactive_date']);


    foreach ($_POST['member'] as $key => $val) {
      $no[] = '"' . $val['mbs_member_no'] . '"' ;
    }

    $no = implode(', ', $no);

    $this->Member_model->update($_POST['mbs_policy_no'] . '_mst_member', 'mmbr_pol_no = ' . $_POST['mbs_policy_no'] . ' AND mmbr_member_no IN (' . $no . ')', $member);
    $this->Member_model->update('member_suspend_history', 'mbs_policy_no = ' . $_POST['mbs_policy_no'] . ' AND mbs_member_no IN (' . $no . ')', $active);

    $_SESSION['users_input'] = true;
    $_SESSION['users_text'] = 'Data Member Berhasil Active';
    $_SESSION['users_status'] = 'success';

    redirect(base_url() . 'member/reactive');
  }

  function reactive_get(){
    $header['supmenu'] = 'Member';
    $header['submenu'] = 'Member Re Active';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu($_SESSION['username']);

    $data['type'] = [0 => 'Terminate', 2 => 'Suspend'];
    $data['data'] = $this->policy("WHERE pol_status = 'IN'");

    $this->load->view('header', $header);
    $this->load->view('member/reactive', $data);
    $this->load->view('footer');
  }

  function upload_get(){
    $header['supmenu'] = 'Member';
    $header['submenu'] = 'Member Upload';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu($_SESSION['username']);

    $this->load->view('header', $header);
    $this->load->view('content');
    $this->load->view('footer');
  }

  function voucher_get(){
    $header['supmenu'] = 'Member';
    $header['submenu'] = 'Voucher Membership';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu($_SESSION['username']);

    $this->load->view('header', $header);
    $this->load->view('content');
    $this->load->view('footer');
  }

  function benefit_data_post(){
    $data = $this->Member_model->benefit($_POST['param']);
    $response = (empty($data)) ? 204 : 200 ;
    json_output($response, $data);
  }

  function report_data_post(){
    // $data = $this->Member_model->profile($no, $member, $card);
    $data = $this->Member_model->card($_POST['param']);
    $response = (empty($data)) ? 204 : 200 ;
    json_output($response, $data);
  }

  function report_get(){
    $header['supmenu'] = 'Member';
    $header['submenu'] = 'Member Preview';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu($_SESSION['username']);

    $data['data'] = $this->policy("WHERE pol_status = 'IN'");

    $this->load->view('header', $header);
    $this->load->view('member/report', $data);
    $this->load->view('footer');
  }

  function detail_get($no, $member, $card){
    $header['supmenu'] = 'Member';
    $header['submenu'] = 'Member Preview';
    $header['class'] = $this->fetch_class();
    $header['method'] = $this->fetch_method();
    $header['menu'] = $this->Menu_model->menu($_SESSION['username']);

    $data['family'] = $this->Member_model->family($no, $member);
    $data['data'] = $this->Member_model->profile($no, $member, $card);

    $data['sex'] = ['M' => 'Male','F' => 'Female'];
    $data['marital'] = ['M' => 'Married', 'S' => 'Single'];
    $data['tab'] = ['Profile', 'Family', 'Benefit', 'Plafond', 'BPJS'];
    $data['enroll'] = ['EMP' => 'Employe','SPO' => 'Spouse','CHL' => 'Child'];
    $data['category'] = ['H' => 'Per-Hari', 'T' => 'Per-Tahun', 'K' => 'Per-Kunjungan'];
    $data['payment'] = ['', 'ATM Transfer', 'Kantor BPJS', 'Kantor POS', 'Indomart', 'Alfamart'];
    $data['logo'] = ['assignment_ind', 'people_outline', 'assignment', 'assignment_turned_in', 'healing'];

    $this->load->view('header', $header);
    $this->load->view('member/detail', $data);
    $this->load->view('footer');
  }

  function coba_get(){
    $no = '2018010000022';
    $member = 'M000002C';
    $card = '180100002200002C';
    $data = $this->Member_model->profile($no, $member, $card);
    echo "<pre>";
    print_r($data);
  }

}